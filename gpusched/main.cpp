#include <argp.h>
#include <stdlib.h>

#include <chrono>
#include <iostream>
#include <libgpusched/gpusched.hpp>

const char *argp_program_version = "gpusched v0.0.1";

const char *argp_program_bug_address = "";
static char doc[] =
    "GPUSCHED: schedule abstract graph generator for GPU workload";

static char args_doc[] = "[Task set file] [Job set file]";

static struct argp_option options[] = {
    {"work_conserving", 'w', 0, 0, "Use work_conserving scheduling"},
    {"generate all node in graph", 'a', 0, 0, "generate all possible node"},
    {"Generate more details in graph", 'd', 0, 0, "generate all possible node"},
    {"Use EDF to construct single successor with max utilization each time", 'f', 0, 0, "EDF generation"},
    {"Use EDF to construct single successor with job in isolation each time", 'e', 0, 0, "EDF generation"},
    {0}};

struct arguments {
	char *args[2];
	int work_conserving;
	bool all_nodes_graph;
	bool details_graph;
	bool edfmax;
	bool edfisolation;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	struct arguments *arguments =
	    static_cast<struct arguments *>(state->input);
	switch (key) {
		case 'w':
			arguments->work_conserving = 1;
			break;
		case 'a':
			arguments->all_nodes_graph = true;
			break;
		case 'd':
			arguments->details_graph = true;
			break;
		case 'f':
			arguments->edfmax= true;
			break;
		case 'e':
			arguments->edfisolation= true;
			break;
		case ARGP_KEY_ARG:
			if (state->arg_num >= 2) argp_usage(state);
			arguments->args[state->arg_num] = arg;
			break;
		case ARGP_KEY_END:
			if (state->arg_num < 2) argp_usage(state);
			break;
		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};

using namespace std;
int main(int argc, char **argv) {
	struct arguments arguments;
	arguments.work_conserving = 0;
	arguments.all_nodes_graph = false;
	arguments.details_graph = false;
	arguments.edfmax = false;
	arguments.edfisolation= false;
	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	std::string task_set_str = arguments.args[0];
	std::string job_set_str = arguments.args[1];
	std::string fullnode;
	if (arguments.all_nodes_graph)
	       	fullnode = "_full_nodes";
	std::string graph_filename =
	    std::string("./experiments/") + task_set_str.substr(11) + "_" +
	    job_set_str.substr(11) + fullnode + std::string(".dot");
	std::cout << graph_filename << std::endl;

	gpusched::graph_viz::GraphGen _graph(graph_filename,
					   arguments.details_graph);

	// gpusched::utils::fetchTasks("/home/korede/phd/prototypes/gpuschedcode/task_set/task_set_1");
	// gpusched::utils::fetchJobs("/home/korede/phd/prototypes/gpuschedcode/task_set/jobs_task_set_1");

	gpusched::utils::fetchTasks(arguments.args[0]);
	gpusched::utils::fetchJobs(arguments.args[1]);
	// std::shared_ptr<gpusched::GpuTask> x =
	// static_pointer_cast<gpusched::GpuTask>(gpusched::GLOBALTASKS[1]); cout <<
	// x->to_string(); std::shared_ptr<gpusched::Job> j =
	// gpusched::GLOBALJOBS[0]; cout << j->to_string() << endl;

	// gpusched::Occupancy o;
	// o.allocate_resource_kernel(j->get_job_id());
	// std::cout << o.to_string() << std::endl;
	// o.free_resources(j->get_job_id());
	// std::cout << o.to_string() << std::endl;

	// Chrono using
	using std::chrono::duration;
	using std::chrono::duration_cast;
	using std::chrono::high_resolution_clock;
	using std::chrono::milliseconds;

	std::string result_filename(task_set_str.substr(11,12));
	std::ofstream result_file(result_filename + std::string("result"), std::ios::app);
	if (arguments.work_conserving) {
		auto t1 = high_resolution_clock::now();
		// gpusched::parallel_explore<gpusched::VertexBatch>(arguments.all_nodes_graph,
		// arguments.details_graph);
		bool find_path = gpusched::explore<gpusched::VertexBatch2>(arguments.all_nodes_graph,
						     arguments.details_graph);
		auto t2 = high_resolution_clock::now();
		std::ofstream execution_time_file("./execution_time",
						  std::ios::app);
		auto ms_int = duration_cast<milliseconds>(t2 - t1);
		duration<double, std::milli> ms_double = t2 - t1;

		execution_time_file << graph_filename
				    << " ms_int: " << ms_int.count()
				    << " ms_double: " << ms_double.count()
				    << " nb_jobs: " << gpusched::GLOBALJOBS.size();
		if (find_path){
			result_file << " 1 ";
			execution_time_file << " schedulable: 1 ";
		}
		else{
			result_file << " 0 ";
			execution_time_file << " schedulable: 0 ";
		}
		execution_time_file.close();
		
		result_file.close();
	}else if (arguments.edfmax) {
		std::ofstream execution_time_file("./execution_time",
						  std::ios::app);
		auto t1 = high_resolution_clock::now();
		// gpusched::parallel_explore<gpusched::VertexBatch>(arguments.all_nodes_graph,
		// arguments.details_graph);
		bool find_path = gpusched::exploreEDFMAX<gpusched::VertexBatch2>(arguments.all_nodes_graph,
						     arguments.details_graph);
		auto t2 = high_resolution_clock::now();
		auto ms_int = duration_cast<milliseconds>(t2 - t1);
		duration<double, std::milli> ms_double = t2 - t1;

		execution_time_file << graph_filename
				    << " ms_int: " << ms_int.count()
				    << " ms_double: " << ms_double.count()
				    << " nb_jobs: " << gpusched::GLOBALJOBS.size();
		if (find_path){
			result_file << " 1 ";
			execution_time_file << " schedulable: 1 ";
		}
		else{
			result_file << " 0 ";
			execution_time_file << " schedulable: 0 ";
		}
		execution_time_file.close();
		result_file.close();
	}
	else if (arguments.edfisolation) {
		std::ofstream execution_time_file("./execution_time",
						  std::ios::app);
		auto t1 = high_resolution_clock::now();
		// gpusched::parallel_explore<gpusched::VertexBatch>(arguments.all_nodes_graph,
		// arguments.details_graph);
		bool find_path = gpusched::exploreEDFIsolation<gpusched::VertexBatch2>(arguments.all_nodes_graph,
						     arguments.details_graph);
		auto t2 = high_resolution_clock::now();
		auto ms_int = duration_cast<milliseconds>(t2 - t1);
		duration<double, std::milli> ms_double = t2 - t1;

		execution_time_file << graph_filename
				    << " ms_int: " << ms_int.count()
				    << " ms_double: " << ms_double.count()
				    << " nb_jobs: " << gpusched::GLOBALJOBS.size();
		if (find_path){
			result_file << " 1 ";
			execution_time_file << " schedulable: 1 ";
		}
		else{
			result_file << " 0 ";
			execution_time_file << " schedulable: 0 ";
		}
		execution_time_file.close();
		result_file.close();
	}

	// gpusched::free_GLOBALTASKS();
	// gpusched::free_GLOBALJOBS();

	// write time
	gpusched::free_TASKSSF();
	// gpusched::graph_viz::GraphGen::DestroyInstance(); // destroy the
	// instance so the destructor is called
}
