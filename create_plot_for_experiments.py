#!/bin/python3 
import collections
import matplotlib as matplt
import matplotlib.pyplot as plt
import matplotlib.axes as axes
import glob
import sys 
import numpy as np
name = list()


utilizations = ["0.2", "0.4", "0.6", "0.8", "1.0", "1.2", "1.4", "1.6", "1.8", "2.0"]
### Script to generate figure
class Point:
    
    def __init__(self, x, y):
        self.x = x 
        self.y = y

def parse_time_file(filename: str) -> list :
    times = list()
    with open(filename) as file:
        #nb_jobs = [int(x) for x in next(file).split()]
        nb_jobs = [int(x) for x in next(file).split()];
        for line in file:
            #times.append([int(x) for x in line.split(' ')[2]])
            times.append(int(line.split(' ')[2])) 
    print(times)
    return (nb_jobs, times); 

def generate_time_plot(files: list):

    matplt.use("pgf")
    plt.rcParams.update({
        "pgf.texsystem" : "pdflatex",
        })
    
    points = list() 
    for i in range(0,len(files)):
        nb_job, job_set_time = parse_time_file(files[i]);
        for j in range(0, len(job_set_time)):
            points.append(Point(nb_job, job_set_time[j]))
    print("Points length:", len(points))
    plt.figure("Time execution")
    plt.ylabel("CPU time execution ms")
    plt.xlabel("Job set size")
    x = np.array([point.x for point in points])
    y = np.array([point.y for point in points])
    plt.scatter(x, y)
    plt.savefig("time_test_plot.pgf")
    plt.savefig("time_test_plot.png")
    plt.close()



def generate_plot(filename: str, result: list, lines_names: list, markers: list,outputname: str):
    matplt.use("pgf")
    plt.rcParams.update({
            "pgf.texsystem" : "pdflatex", 
        })
    plt.figure("Result")
    print("result: ", result)
    print("name: ", name)
    #plt.xticks([0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0])
    plt.axhline(y = 0.2, color = 'lightgrey', linestyle = '-')
    plt.axhline(y = 0.4, color = 'lightgrey', linestyle = '-')
    plt.axhline(y = 0.6, color = 'lightgrey', linestyle = '-')
    plt.axhline(y = 0.8, color = 'lightgrey', linestyle = '-')
    plt.axhline(y = 1.0, color = 'lightgrey', linestyle = '-')

    ax = plt.gca()
    ax.set_ylim(bottom = 0)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    for i in range(0, len(lines_names)):
        plt.plot(utilizations, result[i], label= lines_names[i], ls="--", marker = markers[i])
        plt.legend();
        plt.ylabel("Acceptance ratio")
        plt.xlabel("Utilization")
    plt.savefig(outputname[0:-2] + ".pgf")
    plt.savefig(outputname)
    plt.close()
    #plt.show()

def parse_file(filename: str) -> float: 

    with open(filename) as file:
        result = [int(x) for x in next(file).split()]
        result = sum(result) / len(result)
    return result 


def parse(result: dict) -> list:
    list_x = list()
    for i in range(0, len(utilizations)):
        x = sum(result[utilizations[i]]) / len(result[utilizations[i]])
        list_x.append(x)
    return list_x

def parse_acceptance_ratio_gpusched_and_edfmax(filename: str) -> float:
   
    result_mix   = dict()
    result_large = dict()
    result_small = dict() 
    for i in range(0, len(utilizations)):
        result_mix[utilizations[i]] = list()
        result_large[utilizations[i]] = list()
        result_small[utilizations[i]] = list()

    with open(filename) as file:
        for line in file:
            util    = line.split("umax")[1].split("_")[0]
            sched   = line.split("schedulable:")[1].split(" ")[1]
            if "small" in line:
                result_small[util].append(int(sched))
            if "large" in line:
                result_large[util].append(int(sched))
            if "mix" in line:
                result_mix[util].append(int(sched))

    return (result_small, result_large, result_mix)

def parse_edfisolation(filename: str) -> float:
   
    result_edf_isolation= dict()
    for i in range(0, len(utilizations)):
        result_edf_isolation[utilizations[i]] = list()

    with open(filename) as file:
        for line in file:
            util    = line.split("umax")[1].split("_")[0]
            sched   = line.split("schedulable:")[1].split(" ")[1]
            result_edf_isolation[util].append(int(sched))

    return result_edf_isolation 

                 

def parse_time_gpusched_edfmax(filename: str) -> dict:

    job_time = dict()
    with open(filename) as file:
        for line in file:
            sched = line.split("schedulable:")[1].split(" ")[1]
            if(int(sched) == 1):
                nb_job = line.split("nb_jobs:")[1].split(" ")[1]
                job_time[nb_job] = line.split("ms_int:")[1].split(" ")[1]

    return job_time




def generate_time_plot(nb_jobs_time: dict):

    matplt.use("pgf")
    plt.rcParams.update({
        "pgf.texsystem" : "pdflatex",
        })
    
    points = list() 
    
    for key in nb_jobs_time:
        if(int(nb_jobs_time[key]) < 3000):
                points.append(Point(int(key), int(nb_jobs_time[key])))
    
    print("Points length:", len(points))
    plt.figure("Time execution")
    plt.ylabel("CPU time execution ms")
    plt.xlabel("Job set size")
    x = np.array([point.x for point in points])
    y = np.array([point.y for point in points])
    plt.scatter(x, y)
    plt.savefig("time_test_plot.pgf")
    plt.savefig("time_test_plot.png")
    plt.close()

def generate_merge_plot(with_and_without: list):
    matplt.use("pgf")
    plt.rcParams.update({
        "pgf.texsystem" : "pdflatex",
        })
    
    matplt.rcParams['mathtext.fontset']='custom'

    points_merge = list() 
    points_without_merge = list()
    for key in with_and_without[0]:
        if(int(with_and_without[0][key])):
            points_without_merge.append(Point(int(key), int(with_and_without[0][key])-1))
    for key in with_and_without[1]:
        if(int(with_and_without[1][key])):
            points_merge.append(Point(int(key), int(with_and_without[1][key])))
   
    
    plt.figure("With and without merge")
    plt.ylabel("Vertices")
    plt.xlabel("Job set size")
    plt.yscale('log', base=10)

    plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14])
    ax = plt.gca()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    x_merge = np.array([point.x for point in points_merge])
    y_merge = np.array([point.y for point in points_merge])
    x_without = np.array([point.x for point in points_without_merge])
    y_without = np.array([point.y for point in points_without_merge])
    print(x_merge)
    #plt.plot(result[i], label= lines_names[i], ls="--", marker = markers[i])
    plt.scatter(x_merge, y_merge)
    plt.scatter(x_without, y_without)
    x_m_sort = np.sort(x_merge)
    y_m_sort = np.sort(y_merge)
    x_w_sort = np.sort(x_without)
    y_w_sort = np.sort(y_without)
    plt.plot(x_m_sort, y_m_sort)
    plt.plot(x_w_sort, y_w_sort)
    plt.savefig("with_and_without_merge.pgf")
    plt.savefig("with_and_without_merge.png")
    plt.close()

def parse_merge(filename: str) -> dict:

    node_jobs = dict()
    with open(filename) as file:
        for line in file:
           nb_jobs = line.split("nb_jobs:")[1].split(" ")[1] 
           node_generated =  line.split("generated:")[1].split(" ")[1].strip()
           node_jobs[nb_jobs] = node_generated
    return node_jobs

def parse_gpusched_time_utilization(filename: str) -> tuple:
    result_mix_utilization_time   = dict()
    result_large_utilization_time = dict()
    result_small_utilization_time = dict() 

    for i in range(0, len(utilizations)):
      #  result_mix_utilization_time[utilizations[i]]   = 0 
      #  result_large_utilization_time[utilizations[i]] = 0 
      #  result_small_utilization_time[utilizations[i]] = 0 
        result_mix_utilization_time[utilizations[i]]   = list() 
        result_large_utilization_time[utilizations[i]] = list()
        result_small_utilization_time[utilizations[i]] = list()

    with open(filename) as file:
        for line in file:
            util    = line.split("umax")[1].split("_")[0]
            time    = line.split("ms_int:")[1].split(" ")[1]
            if "small" in line:
                #result_small_utilization_time[util]+=int(time)
                result_small_utilization_time[util].append(time)
            if "large" in line:
                #result_large_utilization_time[util]+=int(time)
                result_large_utilization_time[util].append(time)
            if "mix" in line:
                #result_mix_utilization_time[util]+=int(time) 
                result_mix_utilization_time[util].append(time)

    #for i in range(0, len(utilizations)):
    #    result_small_utilization_time[utilizations[i]]/= 50 # put global variable for task set number
    #    result_large_utilization_time[utilizations[i]]/= 50 # put global variable for task set number
    #    result_mix_utilization_time[utilizations[i]]/=50 # put global variable for task set number
    return (result_small_utilization_time, result_large_utilization_time, result_mix_utilization_time)

def generate_gpusched_time_utilization_boxplot(results: list):

    matplt.use("pgf")
    plt.rcParams.update({
        "pgf.texsystem" : "pdflatex",
        })
    
    points_small = list() 
    points_large = list()
    points_mix   = list()

    data_small   = dict()
    for i in range(0, len(utilizations)):
        data_small[utilizations[i]] = list()  

    for key in results[0].keys():
        for i in range(0, len(results[0][key])):
            points_small.append(Point(float(key), int(results[0][key][i])))
            data_small[key].append(int(results[0][key][i]))

    #for key in results[1].keys():
    #    if(int(results[1][key])):
    #        points_small.append(Point(float(key), int(results[1][key])))
    #        data_small[key].append(int(results[1][key]))
    #for key in results[2].keys():
    #    if(int(results[2][key])):
    #        points_small.append(Point(float(key), int(results[2][key])))
    #        data_small[key].append(int(results[2][key]))
    print(data_small)
    plt.figure("GPUSCHED time/utilizations")
    plt.ylabel("CPU time (ms)")
    plt.xlabel("U")
  #  plt.yscale('log', base=10)

    ax = plt.gca()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.boxplot(data_small.values(), 0, '', labels=data_small.keys())
    print(data_small.keys()) 
    plt.savefig( "gpusched_utilization.pgf")
    plt.savefig("gpusched_utilization.png")
    plt.close()

def parse_file_tasks(task4_filename: str, task5_filename: str, task6_filename: str):

    task4_execution_time = list()
    task5_execution_time = list()
    task6_execution_time = list()
    
    with open(task4_filename) as file:
        for line in file:
            #umax = line.split("umax")[1].split["_"][0]
            if  "1.0" in line:
                time    = line.split("ms_int:")[1].split(" ")[1]
                task4_execution_time.append(int(time))
    
    with open(task5_filename) as file:
        for line in file:
            #umax = line.split("umax")[1].split["_"][0]
            if  "1.0" in line:
                time    = line.split("ms_int:")[1].split(" ")[1]
                task5_execution_time.append(int(time))
    
    with open(task6_filename) as file:
        for line in file:
            #umax = line.split("umax")[1].split["_"][0]
            if  "1.0" in line:
                time    = line.split("ms_int:")[1].split(" ")[1]
                task6_execution_time.append(int(time))
    x = (4, 5, 6)
    plt.figure("GPUSCHED compare tasks")
    plt.ylabel("CPU time (ms)")
    plt.xlabel("Number of tasks")
    plt.xticks([4,5,6])
    plt.yscale('log', base=10)
    
    ax = plt.gca()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    mean_4_tasks = int(sum(task4_execution_time)/(len(task4_execution_time)))
    mean_5_tasks = int(sum(task5_execution_time)/(len(task5_execution_time)))
    mean_6_tasks = int(sum(task6_execution_time)/(len(task6_execution_time)))
    y = (mean_4_tasks, mean_5_tasks, mean_6_tasks)
    plt.bar(x,y, color="grey", ec="black" )
    plt.savefig("test.pgf")
    plt.savefig("test.png")

if __name__ == "__main__":  

       # ACCEPTANCE RATIO FIGURE GENERATION
       # file that will receive acceptance ratio 
       #output_acceptance_name       = sys.argv[1]
       # file for execution time
    #output_execution_time_name   = sys.argv[2]

       ALL_EXP_RESULTS = list()



       list_mix_result              = list()
       list_large_result            = list()
       list_small_result            = list()

       lines_name_small             = list()
       lines_name_mix               = list()
       lines_name_large             = list()

       result_gpusched_small, result_gpusched_large, result_gpusched_mix = parse_acceptance_ratio_gpusched_and_edfmax("./gpusched/execution_time")

       result_edfmax_small, result_edfmax_large, result_edfmax_mix = parse_acceptance_ratio_gpusched_and_edfmax("./edfmax/execution_time")

       result_edfisolation= parse_edfisolation("./edfisolation/execution_time")

       list_small_result = parse(result_gpusched_small)
       list_large_result = parse(result_gpusched_large)
       list_mix_result   = parse(result_gpusched_mix)

       list_edf_max_small                   = parse(result_edfmax_small)
       list_edf_max_large                   = parse(result_edfmax_large)
       list_edf_max_mix                     = parse(result_edfmax_mix)
       print(list_small_result)
       print(list_large_result)
       print(list_mix_result)
       print(list_edf_max_small)
       print(list_edf_max_large)
       print(list_edf_max_mix)

       ## PARSE EDF ISOLATION AND MAX
       list_edf_isolation           = list()
       list_edf_isolation           = parse(result_edfisolation)

       print(list_edf_isolation)
       markers = list()
       markers.append("1")
       markers.append("o")
       markers.append("2")
       markers.append("3")
       markers.append("2")
       lines_name_small.append("GPUsched small sf")
       lines_name_small.append("EDF isolation")
       lines_name_small.append("EDF max small parallel")
       EXP_ONLY_SMALL_RESULTS = list()
       EXP_ONLY_SMALL_RESULTS.append(list_small_result)
       EXP_ONLY_SMALL_RESULTS.append(list_edf_isolation)
       EXP_ONLY_SMALL_RESULTS.append(list_edf_max_small)
       generate_plot("Result", EXP_ONLY_SMALL_RESULTS, lines_name_small, markers, "exp_small")

       lines_name_large.append("GPUsched large sf")
       lines_name_large.append("EDF isolation")
       lines_name_large.append("EDF max large parallel")
       EXP_ONLY_LARGE_RESULTS = list()
       EXP_ONLY_LARGE_RESULTS.append(list_large_result)
       EXP_ONLY_LARGE_RESULTS.append(list_edf_isolation)
       EXP_ONLY_LARGE_RESULTS.append(list_edf_max_large)
       generate_plot("Result", EXP_ONLY_LARGE_RESULTS, lines_name_large, markers, "exp_large")

       lines_name_mix.append("GPUsched mix sf")
       lines_name_mix.append("EDF isolation")
       lines_name_mix.append("EDF max mix parallel")

       EXP_ONLY_MIX_RESULTS = list()
       EXP_ONLY_MIX_RESULTS.append(list_mix_result)
       EXP_ONLY_MIX_RESULTS.append(list_edf_isolation)
       EXP_ONLY_MIX_RESULTS.append(list_edf_max_mix)
       generate_plot("Result", EXP_ONLY_MIX_RESULTS, lines_name_mix, markers, "exp_mix")

       nb_jobs_times = parse_time_gpusched_edfmax("./gpusched/execution_time") 
       generate_time_plot(nb_jobs_times)        

       EXP_MERGE     = list() 
       without_merge = parse_merge("./test_set/without_merge_execution_time")
       with_merge    = parse_merge("./test_set/with_merge_execution_time")
       EXP_MERGE.append(without_merge)
       EXP_MERGE.append(with_merge)
       generate_merge_plot(EXP_MERGE)



       EXP_GPUSCHED_UTILIZATION_TIME = list()
       gpusched_time_utilization_small, gpusched_time_utilization_large, gpusched_time_utilization_mix = parse_gpusched_time_utilization("./gpusched/execution_time")
       print(gpusched_time_utilization_small)
       print(gpusched_time_utilization_large)
       print(gpusched_time_utilization_mix)

       EXP_GPUSCHED_UTILIZATION_TIME.append(gpusched_time_utilization_large)
       #EXP_GPUSCHED_UTILIZATION_TIME.append(gpusched_time_utilization_large)
       #EXP_GPUSCHED_UTILIZATION_TIME.append(gpusched_time_utilization_mix)
       print(gpusched_time_utilization_small)
       generate_gpusched_time_utilization_boxplot(EXP_GPUSCHED_UTILIZATION_TIME)

       parse_file_tasks("./gpusched/tasks4_execution_time", "./gpusched/tasks5_execution_time", "./gpusched/task6_execution_time")

       

