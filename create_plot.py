#!/bin/python3 
import matplotlib as matplt
import matplotlib.pyplot as plt
import matplotlib.axes as axes
import glob
import sys 
import numpy as np
name = list()

class Point:
    
    def __init__(self, x, y):
        self.x = x 
        self.y = y

def parse_time_file(filename: str) -> list :
    times = list()
    with open(filename) as file:
        #nb_jobs = [int(x) for x in next(file).split()]
        nb_jobs = [int(x) for x in next(file).split()];
        for line in file:
            #times.append([int(x) for x in line.split(' ')[2]])
            times.append(int(line.split(' ')[2])) 
    print(times)
    return (nb_jobs, times); 

def generate_time_plot(files: list):

    matplt.use("pgf")
    plt.rcParams.update({
        "pgf.texsystem" : "pdflatex",
        })
    
    points = list() 
    for i in range(0,len(files)):
        nb_job, job_set_time = parse_time_file(files[i]);
        for j in range(0, len(job_set_time)):
            points.append(Point(nb_job, job_set_time[j]))
    print("Points length:", len(points))
    plt.figure("Time execution")
    plt.ylabel("CPU time execution ms")
    plt.xlabel("Job set size")
    x = np.array([point.x for point in points])
    y = np.array([point.y for point in points])
    plt.scatter(x, y)
    plt.savefig("time_test_plot.pgf")
    plt.savefig("time_test_plot.png")



def generate_plot(filename: str, result: list, outputname: str):
    matplt.use("pgf")
    plt.rcParams.update({
            "pgf.texsystem" : "pdflatex", 
        })
    plt.figure("Results")
    print("result: ", result)
    print("name: ", name)
    plt.plot(name, result, label="line one")
    plt.legend();
    plt.ylabel("Acceptance ratio")
    plt.xlabel("Utilization")
    ax = plt.gca()
    ax.set_ylim(bottom = 0)
    plt.savefig(outputname[0:-4] + ".pgf")
    plt.savefig(outputname)
    #plt.show()

def parse_file(filename: str) -> float: 

    with open(filename) as file:
        result = [int(x) for x in next(file).split()]
        result = sum(result) / len(result)
    return result 


if __name__ == "__main__":  
       outputname =  sys.argv[1]
       list_file = glob.glob("./umax*result")
       list_time_file = glob.glob("./execution*")
       #list_time_file.sort()
       list_file.sort()
       list_result = list()
       print(len(list_file))
       for i in range(0, len(list_file)):
           print(list_file)
           list_result.append(parse_file(list_file[i]))
           name.append(float(list_file[i][6:9])) 
       print(name)
       print("list result", list_result)
       generate_plot("Result", list_result, outputname)
       #generate_time_plot(list_time_file)
