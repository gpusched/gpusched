# GPUSCHED  

A Real-Time Scheduler for GPU workload.

# Quick Guide

# Test

GPUSched require Catch2 v3 library to be installed. To install Catch2 see [Catch2 installation](https://github.com/catchorg/Catch2/blob/devel/docs/cmake-integration.md#installing-catch2-from-git-repository)

# Documentation generation 

GPUSched project documentation can be generated with doxygen. 

