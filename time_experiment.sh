#!/bin/bash 
# This script will run experiments with all combinaisons of job_set and task_set  file in taskset/ directory

#TASK_SET="./task_set/task*"
#JOB_SET="./task_set/job*"
TASK_SET="./task_set/umax*"
JOB_SET="./task_set/job_*"

MEASURE_TIME="./execution_time"

EXPDIR="./experiments"
BINDIR="./build"
BIN="./build/gpusched/gpusched_exe"

FLAG=$1
FULLFLAG="-wda"

rm umax*result 

if [ -d "$BINDIR" ]; then
       	rm -rf $BINDIR 
	mkdir $BINDIR 
else 
	mkdir $BINDIR
fi

if [ -d "$EXPDIR" ]; then
	rm -rf $EXPDIR
	mkdir $EXPDIR
else
	mkdir $EXPDIR
fi

if [ -f "$MEASURE_TIME" ]; then
       	rm $MEASURE_TIME 
else 
	touch $MEASURE_TIME
fi

EDF_MAX_SMALL_DIR="./edf_max_small/"
EDF_MAX_LARGE_DIR="./edf_max_large/"
EDF_MAX_MIX_DIR="./edf_max_mix/"
SMALL_DIR="./small/"
LARGE_DIR="./large/"
MIX_DIR="./mix/"
EDF_ISOLATION_DIR="./edf_isolation/"

ARRAY_EXP_DIR=($EDF_MAX_SMALL_DIR $EDF_MAX_LARGE_DIR $EDF_MAX_MIX_DIR $SMALL_DIR $LARGE_DIR $MIX_DIR $EDF_ISOLATION_DIR)



TASK_SET_GENERATOR_DIR="./task_set_generator/"
TASK_SET_DIR="./task_set/"

for dir in "${ARRAY_EXP_DIR[@]}";do
	if [ -d "$dir" ]; then
		rm $dir/*
	else
		mkdir $dir
	fi
done

ALL_EXP_TIME_EXECUTION_DIR="./all_exp_time_execution_dir"

GPUSCHED_LARGE_EXEC_TIME_FILE="execution_time_gpusched_large"
GPUSCHED_SMALL_EXEC_TIME_FILE="execution_time_gpusched_small"
GPUSCHED_MIX_EXEC_TIME_FILE="execution_time_gpusched_mix"

EDFMAX_LARGE_EXEC_TIME_FILE="execution_time_edf_max_large"
EDFMAX_SMALL_EXEC_TIME_FILE="execution_time_edf_max_small"
EDFMAX_MIX_EXEC_TIME_FILE="execution_time_edf_max_mix"

EDFISOLATION_EXEC_TIME_FILE="execution_time_edf_isolation"

if [ -d "$ALL_EXP_TIME_EXECUTION_DIR" ]; then 
	rm $ALL_EXP_TIME_EXECUTION_DIR/*
else
	mkdir $ALL_EXP_TIME_EXECUTION_DIR
fi

# DIFFERENT COMPILATION MODE
#cd "./generator_task_set"; ./script.py;  cd -
cd "./build"; cmake -DDEBUG=OFF -DBUILD_DOC=OFF -DTEST=OFF .. ; make; cd -
#cd "./build"; cmake -DBUILD_DOC=OFF -DTEST=OFF .. ; make; cd -
#cd "./build"; cmake -DDEBUG=OFF -DBUILD_DOC=OFF  .. ; make; cd -
#cd "./build"; cmake ..; make; cd - 

shopt -s nullglob




for i in $(seq 0 10); 
do

# delete umax in TASK_SET_GENERATOR prev initialization
#rm $TASK_SET_GENERATOR_DIR/umax*

touch $ALL_EXP_TIME_EXECUTION_DIR/"exp_${i}_nb_jobs"

# Generate task set 
cd ${TASK_SET_GENERATOR_DIR}; timeout 60s ./initialize_taskset_for_experiment.sh 5 job_set_exp JETSON_AGX_XAVIER 30 task_sets/task_set_size7/exp_00 SF_VARIATION_SMALL; cd ..


wc -l < $TASK_SET_DIR/job_set_exp > $ALL_EXP_TIME_EXECUTION_DIR/"exp_${i}_nb_jobs"

FLAG="-w"

cp ./task_set_generator/umax*small* ./task_set/

declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
	done
done


./create_plot  exp
#./create_plot.py exp_sf_small
mv $TASK_SET_DIR/umax* $MEASURE_TIME umax* ./small
cp ${SMALL_DIR}/${MEASURE_TIME} "${ALL_EXP_TIME_EXECUTION_DIR}/${GPUSCHED_SMALL_EXEC_TIME_FILE}_${i}"

cp ./task_set_generator/umax*large* ./task_set/
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
	done
done

./create_plot  exp
mv ./task_set/umax* $MEASURE_TIME umax* ./large
cp $LARGE_DIR/$MEASURE_TIME "${ALL_EXP_TIME_EXECUTION_DIR}/${GPUSCHED_LARGE_EXEC_TIME_FILE}_${i}"

# Start mix task set 
cp $TASK_SET_GENERATOR_DIR/umax*mix* $TASK_SET_DIR
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
	done
done

./create_plot  exp
mv umax* $TASK_SET_DIR/umax* $MEASURE_TIME ./mix
cp "${MIX_DIR}/${MEASURE_TIME}" "${ALL_EXP_TIME_EXECUTION_DIR}/${GPUSCHED_MIX_EXEC_TIME_FILE}_${i}"

FLAG="-f"
cp $TASK_SET_GENERATOR_DIR/umax*small* $TASK_SET_DIR 
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
	done
done


./create_plot  exp
mv umax* $TASK_SET_DIR/umax* $MEASURE_TIME ./edf_max_small
cp "${EDF_MAX_SMALL_DIR}/${MEASURE_TIME}" "${ALL_EXP_TIME_EXECUTION_DIR}/${EDFMAX_SMALL_EXEC_TIME_FILE}_${i}" 

cp $TASK_SET_GENERATOR_DIR/umax*large* $TASK_SET_DIR 
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
	done
done


./create_plot  exp
mv  umax* ./task_set/umax* $MEASURE_TIME ./edf_max_large
cp "${EDF_MAX_LARGE_DIR}/${MEASURE_TIME}" "${ALL_EXP_TIME_EXECUTION_DIR}/${EDFMAX_LARGE_EXEC_TIME_FILE}_${i}" 

cp $TASK_SET_GENERATOR_DIR/umax*mix* $TASK_SET_DIR
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
	done
done


./create_plot  exp
mv  umax* $TASK_SET_DIR/umax* $MEASURE_TIME ./edf_max_mix
cp "${EDF_MAX_MIX_DIR}/${MEASURE_TIME}" "${ALL_EXP_TIME_EXECUTION_DIR}/${EDFMAX_MIX_EXEC_TIME_FILE}_${i}" 

FLAG="-e"
cp $TASK_SET_GENERATOR_DIR/umax*small* $TASK_SET_DIR 
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
		#if [[ "${taskfile: -3:1}" == "${jobfile: -3:1}" ]] ; then  

			#DOT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.dot"
			#OUTPUT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.svg"
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			#DOT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: 11}_full_nodes.dot"
			#OUTPUT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: -1}_full_nodes.svg"
			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE -o $OUTPUT_FILE; cd -

			# then without to generate all nodes
			#echo "Execute gpusched -wa on ${taskfile} ${jobfile}"
			#$BIN $FULLFLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE_FULLNODE -o $OUTPUT_FILE_FULLNODE; cd - 
		#fi
	done
done
mv umax* $TASK_SET_DIR/umax* $MEASURE_TIME $EDF_ISOLATION_DIR 
cp "${EDF_ISOLATION_DIR}/${MEASURE_TIME}" "${ALL_EXP_TIME_EXECUTION_DIR}/${EDFISOLATION_EXEC_TIME_FILE}_${i}" 
done
