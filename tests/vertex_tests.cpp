#include <catch2/catch_test_macros.hpp>
#include "../libgpusched/include/libgpusched/vertex.hpp"
#include "../libgpusched/include/libgpusched/task.hpp"
#include "../libgpusched/include/libgpusched/gputask.hpp"
#include "../libgpusched/include/libgpusched/job.hpp"
#include "../libgpusched/include/libgpusched/occupancy.hpp"
#include "../libgpusched/include/libgpusched/error_t.hpp"
#include "../libgpusched/include/libgpusched/utils.hpp"


#include <memory>
#include <vector>

TEST_CASE( "Vertex constructor", "[vertex]" ) {


	std::unique_ptr<gpusched::real_time_parameters> rp=
		std::make_unique<gpusched::real_time_parameters>();
		rp->bcet = 3;
		rp->wcet = 3;
		rp->period = 10;
		rp->deadline =  10;
		rp->priority = -1;

	std::unique_ptr<gpusched::cuda_kernel_parameters> kp =
		std::make_unique<gpusched::cuda_kernel_parameters>();

		kp->block = 5;
		kp->thread_per_block = 1024;
		kp->regs = 42;
		kp->smem = 0;

	gpusched::GLOBALTASKS[0] = std::make_shared<gpusched::GpuTask>(0, std::move(kp), std::move(rp));

	rp=std::make_unique<gpusched::real_time_parameters>();
	rp->bcet = 3;
	rp->wcet = 3;
	rp->period = 10;
	rp->deadline =  10;
	rp->priority = -1;

	kp = std::make_unique<gpusched::cuda_kernel_parameters>();
	kp->block = 5;
	kp->thread_per_block = 1024;
	kp->regs = 42;
	kp->smem = 0;

	gpusched::GLOBALTASKS[1] = std::make_shared<gpusched::GpuTask>(1, std::move(kp), std::move(rp));

	gpusched::GLOBALJOBS[0] = std::make_shared<gpusched::Job>(0,1,0,10);
	gpusched::GLOBALJOBS[1] = std::make_shared<gpusched::Job>(1,0,0,10);

	SECTION("defaultconstructor") {
		gpusched::Vertex default_vertex;

		// must have only one stream
		// time must be equal to 0
		// it is not schedulable

		REQUIRE(default_vertex.get_vertex_id() == 1);
		REQUIRE(default_vertex.get_origin_vertex_id() == 0);
		REQUIRE(default_vertex.get_count_stream() == 0);
		REQUIRE_FALSE(default_vertex.is_schedulable());
		REQUIRE_FALSE(default_vertex.get_invalid_vertex_flag());
		REQUIRE(default_vertex.get_depth() == 0);

	}


	SECTION("Copy constructor") {
		std::vector<int> workload = {0};
		gpusched::Vertex default_vertex;
		gpusched::Vertex copy_vertex(default_vertex, workload, gpusched::utils::schedule_type::WORK_CONSERVING_LOWER_STREAM);

		REQUIRE(copy_vertex.get_origin_vertex_id() == default_vertex.get_vertex_id());
		REQUIRE_FALSE(copy_vertex.is_schedulable());
	}
}

TEST_CASE("Vertex handle jobs", "[vertex]")
{
	std::unique_ptr<gpusched::real_time_parameters> rp=
		std::make_unique<gpusched::real_time_parameters>();
		rp->bcet = 3;
		rp->wcet = 3;
		rp->period = 10;
		rp->deadline =  10;
		rp->priority = -1;

	std::unique_ptr<gpusched::cuda_kernel_parameters> kp =
		std::make_unique<gpusched::cuda_kernel_parameters>();

		kp->block = 5;
		kp->thread_per_block = 1024;
		kp->regs = 42;
		kp->smem = 0;


	gpusched::GLOBALTASKS[0] = std::make_shared<gpusched::GpuTask>(0, std::move(kp), std::move(rp));

	rp=std::make_unique<gpusched::real_time_parameters>();
	rp->bcet = 5;
	rp->wcet = 5;
	rp->period = 10;
	rp->deadline =  10;
	rp->priority = -1;

	kp = std::make_unique<gpusched::cuda_kernel_parameters>();
	kp->block = 5;
	kp->thread_per_block = 1024;
	kp->regs = 42;
	kp->smem = 0;

	gpusched::GLOBALTASKS[1] = std::make_shared<gpusched::GpuTask>(1, std::move(kp), std::move(rp));

	gpusched::GLOBALJOBS[0] = std::make_shared<gpusched::Job>(0,1,0,10);
	gpusched::GLOBALJOBS[1] = std::make_shared<gpusched::Job>(1,0,0,10);

	gpusched::TASKSSF = new double*[2];
	for (std::size_t i = 0; i < 2; i++)
		gpusched::TASKSSF[i] = new double[2];

	gpusched::TASKSSF[0][0] = 1.0;
	gpusched::TASKSSF[0][1] = 1.1;
	gpusched::TASKSSF[1][0] = 1.1;
	gpusched::TASKSSF[1][1] = 1.0;

	gpusched::Vertex default_vertex;
	gpusched::Vertex copy_vertex(default_vertex,{0,1}, gpusched::utils::schedule_type::WORK_CONSERVING_LOWER_STREAM);
	copy_vertex.fetch_ready_jobs(gpusched::utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
	copy_vertex.handle_scheduled_jobs();
	REQUIRE(copy_vertex.get_remaining_jobs().count() == 0b1111100);// 7 jobs
}
