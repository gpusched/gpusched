#include <catch2/catch_test_macros.hpp>
#include "../libgpusched/include/libgpusched/utils.hpp"
#include "../libgpusched/include/libgpusched/error_t.hpp"


#include <vector>
#include <set>

TEST_CASE( "Permutation generation", "[utils]" ) {


    std::vector<int> v{1,2,3};
    std::set<std::vector<int> > all_permutation;
    std::set<std::vector<int> > two_k_combinaison_only;
    std::set<std::vector<int> > zero_k_combinaison;
    std::set<std::vector<int> > one_k_combinaison;
    SECTION("Limit case: 0")
    {
	    gpusched::utils::get_k_combinaisons(0, zero_k_combinaison,v);

 	    REQUIRE(zero_k_combinaison.size() == 0);
    }

    SECTION("Limit case: 1")
    {
	gpusched::utils::get_k_combinaisons(1, one_k_combinaison, v);

	REQUIRE(one_k_combinaison.find({1}) != one_k_combinaison.end());
	REQUIRE(one_k_combinaison.find({2}) != one_k_combinaison.end());
	REQUIRE(one_k_combinaison.find({3}) != one_k_combinaison.end());
    }

    SECTION("Permutation example (1,2,3)")
    {
	// must return all combinaisons
	// 1
	// 1 2
	// 1 2 3
	// 1 3
	// 1 3 2
	// 2
	// 2 1
	// 2 1 3
	// 2 3
	// 2 3 1
	// 3
	// 3 1
	// 3 1 2
	// 3 2
	// 3 2 1
    	gpusched::utils::get_all_combinaisons(all_permutation, v);

	REQUIRE(all_permutation.find({1}) != all_permutation.end());
	REQUIRE(all_permutation.find({1,2}) != all_permutation.end());
	REQUIRE(all_permutation.find({1,2,3}) != all_permutation.end());
	REQUIRE(all_permutation.find({1,3}) != all_permutation.end());
	REQUIRE(all_permutation.find({1,3,2}) != all_permutation.end());
	REQUIRE(all_permutation.find({2}) != all_permutation.end());
	REQUIRE(all_permutation.find({2,1}) != all_permutation.end());
	REQUIRE(all_permutation.find({2,1,3}) != all_permutation.end());
	REQUIRE(all_permutation.find({2,3}) != all_permutation.end());
	REQUIRE(all_permutation.find({2,3,1}) != all_permutation.end());
	REQUIRE(all_permutation.find({3}) != all_permutation.end());
	REQUIRE(all_permutation.find({3,1}) != all_permutation.end());
	REQUIRE(all_permutation.find({3,1,2}) != all_permutation.end());
	REQUIRE(all_permutation.find({3,2}) != all_permutation.end());
	REQUIRE(all_permutation.find({3,2,1}) != all_permutation.end());

    }

    SECTION("two k combinaison only (1,2,3)")
    {
	gpusched::utils::get_k_combinaisons(2,two_k_combinaison_only, v);
	// must contains all 2-combinaisons
	// 1 2
	// 1 3
	// 2 1
	// 2 3
	// 3 1
	// 3 2

	REQUIRE(all_permutation.find({1,2}) != two_k_combinaison_only.end());
	REQUIRE(all_permutation.find({1,3}) != two_k_combinaison_only.end());
	REQUIRE(all_permutation.find({2,1}) != two_k_combinaison_only.end());
	REQUIRE(all_permutation.find({2,3}) != two_k_combinaison_only.end());
	REQUIRE(all_permutation.find({3,1}) != two_k_combinaison_only.end());
	REQUIRE(all_permutation.find({3,2}) != two_k_combinaison_only.end());
    }
 }


TEST_CASE("is_subset", "[utils]"){

	std::vector<int> initial_vector{1,2,3};
	// valid subsets
	std::vector<int> is_subset_vector{1};
	std::vector<int> is_subset_vector2{1,3};
	// invalid subsets
	std::vector<int> is_not_subset_vector{4};
	std::vector<int> is_not_subset_vector2{1,2,3,4};

	SECTION("Valid subsets")
	{
		REQUIRE(gpusched::utils::is_subset(initial_vector, is_subset_vector));
		REQUIRE(gpusched::utils::is_subset(initial_vector, is_subset_vector2));
	}

	SECTION("Invalid subsets")
	{
		REQUIRE_FALSE(gpusched::utils::is_subset(initial_vector, is_not_subset_vector));
		REQUIRE_FALSE(gpusched::utils::is_subset(initial_vector, is_not_subset_vector2));
	}
}
