#include <catch2/catch_test_macros.hpp>
#include "../libgpusched/include/libgpusched/occupancy.hpp"
#include "../libgpusched/include/libgpusched/error_t.hpp"
#include "../libgpusched/include/libgpusched/gpu_conf.hpp"

constexpr int ARBITRARY_REGS = 10;
constexpr int ARBITRARY_SMEM = 256;
TEST_CASE("Occupancy constructors", "[occupancy]")
{

	SECTION("default constructor")
	{
		gpusched::Occupancy default_occupancy;
		REQUIRE(default_occupancy.get_current_sm() == 0);
		std::array<int, NSM> default_thread_occ = default_occupancy.get_thread_occupancy();
		std::array<int, NSM> default_smem_occ = default_occupancy.get_smem_occupancy();
		std::array<int, NSM> default_reg_occ = default_occupancy.get_reg_occupancy();

		for(std::size_t i = 0; i < NSM; i++)
		{
			REQUIRE(default_thread_occ[i] == THREADS_PER_SM);
			REQUIRE(default_reg_occ[i] == REGS_PER_BLOCK);
			REQUIRE(default_smem_occ[i] == SMEM_PER_BLOCK);
		}
	}
}


TEST_CASE("Allocate a kernel from a GPU task", "[occupancy]")
{

	std::unique_ptr<gpusched::real_time_parameters> rp=
		std::make_unique<gpusched::real_time_parameters>();
		rp->bcet = 3;
		rp->wcet = 3;
		rp->period = 10;
		rp->deadline =  10;
		rp->priority = -1;

	std::unique_ptr<gpusched::cuda_kernel_parameters> kp =
		std::make_unique<gpusched::cuda_kernel_parameters>();

		kp->block = NSM/2;
		kp->thread_per_block = MAX_THREAD_PER_BLOCK;
		kp->regs = ARBITRARY_REGS;//arbitrary
		kp->smem = ARBITRARY_SMEM;//arbitrary

	gpusched::GLOBALTASKS[0] = std::make_shared<gpusched::GpuTask>(0, std::move(kp), std::move(rp));

	rp = std::make_unique<gpusched::real_time_parameters>();
		rp->bcet = 3;
		rp->wcet = 3;
		rp->period = 10;
	 	rp->deadline =  10;
		rp->priority = -1;

	kp = std::make_unique<gpusched::cuda_kernel_parameters>();

		kp->block = NSM;
		kp->thread_per_block = MAX_THREAD_PER_BLOCK;
		kp->regs = ARBITRARY_REGS;//arbitrary
		kp->smem = ARBITRARY_SMEM;//arbitrary

	gpusched::GLOBALTASKS[1] = std::make_shared<gpusched::GpuTask>(1, std::move(kp), std::move(rp));

	rp= std::make_unique<gpusched::real_time_parameters>();
		rp->bcet = 3;
		rp->wcet = 3;
		rp->period = 10;
		rp->deadline =  10;
		rp->priority = -1;

	kp = std::make_unique<gpusched::cuda_kernel_parameters>();

		kp->block = NSM * 2;
		kp->thread_per_block = MAX_THREAD_PER_BLOCK;
		kp->regs = ARBITRARY_REGS;//arbitrary
		kp->smem = ARBITRARY_SMEM;//arbitrary
					  //
	gpusched::GLOBALTASKS[2] = std::make_shared<gpusched::GpuTask>(2, std::move(kp), std::move(rp));

	gpusched::GLOBALJOBS[0] = std::make_shared<gpusched::Job>(0,0,0,10);
	gpusched::GLOBALJOBS[1] = std::make_shared<gpusched::Job>(1,1,0,10);
	gpusched::GLOBALJOBS[2] = std::make_shared<gpusched::Job>(2,2,0,10);

	SECTION("allocation (NSM/2) blocks check if only even sm are affected")
	{
		gpusched::Occupancy base_occupancy;
		base_occupancy.allocate_resource_kernel(0);
		std::array<int, NSM> _thread_occ = base_occupancy.get_thread_occupancy();
		std::array<int, NSM> _smem_occ = base_occupancy.get_smem_occupancy();
		std::array<int, NSM> _reg_occ = base_occupancy.get_reg_occupancy();
		// kernel has 8 blocks, 1 block per sm will be allocated
		for (std::size_t i = 0; i< NSM; i+=2) {
			REQUIRE(_thread_occ[i] == (THREADS_PER_SM-MAX_THREAD_PER_BLOCK));
			REQUIRE(_reg_occ[i] == (REGS_PER_BLOCK - (MAX_THREAD_PER_BLOCK
						* ARBITRARY_REGS)));
			REQUIRE(_smem_occ[i] == (SMEM_PER_BLOCK - ARBITRARY_SMEM));
		}

		for (std::size_t i = 1; i< NSM; i+=2) {
			REQUIRE(_thread_occ[i] == THREADS_PER_SM);
			REQUIRE(_reg_occ[i] == REGS_PER_BLOCK);
			REQUIRE(_smem_occ[i] == SMEM_PER_BLOCK);
		}

	}

	SECTION("Allocation (NSM) block check if all SM has one block assigned")
	{
		gpusched::Occupancy one_block_per_sm_occupancy;
		one_block_per_sm_occupancy.allocate_resource_kernel(1);
		std::array<int, NSM> _thread_occ = one_block_per_sm_occupancy.get_thread_occupancy();
		std::array<int, NSM> _smem_occ = one_block_per_sm_occupancy.get_smem_occupancy();
		std::array<int, NSM> _reg_occ = one_block_per_sm_occupancy.get_reg_occupancy();

		for (std::size_t i = 0; i < NSM; i++) {
			REQUIRE(_thread_occ[i] == (THREADS_PER_SM-MAX_THREAD_PER_BLOCK));
			REQUIRE(_reg_occ[i] == (REGS_PER_BLOCK - (MAX_THREAD_PER_BLOCK
						       	* ARBITRARY_REGS)));
			REQUIRE(_smem_occ[i] == (SMEM_PER_BLOCK - ARBITRARY_SMEM));
		}

	}

	SECTION("Allocate with (2*NSM) with MAX_THREAD_PER_BLOCK to test max allocation")
	{

		std::cout << "2*NSM: " << 2*NSM << std::endl;
		gpusched::Occupancy full_allocation_occ;
		full_allocation_occ.allocate_resource_kernel(2);
		std::array<int, NSM> _thread_occ = full_allocation_occ.get_thread_occupancy();
		std::array<int, NSM> _smem_occ = full_allocation_occ.get_smem_occupancy();
		std::array<int, NSM> _reg_occ = full_allocation_occ.get_reg_occupancy();

		for (std::size_t i = 0; i < NSM; i++) {
			REQUIRE(_thread_occ[i] == 0);
		}

	}

}

TEST_CASE("Free allocated resources of kernel from a GPU task", "[occupancy]")
{

	std::unique_ptr<gpusched::real_time_parameters> rp=
		std::make_unique<gpusched::real_time_parameters>();
		rp->bcet = 3;
		rp->wcet = 3;
		rp->period = 10;
		rp->deadline =  10;
		rp->priority = -1;

	std::unique_ptr<gpusched::cuda_kernel_parameters> kp =
		std::make_unique<gpusched::cuda_kernel_parameters>();

		kp->block = NSM;
		kp->thread_per_block = MAX_THREAD_PER_BLOCK;
		kp->regs = ARBITRARY_REGS;//arbitrary
		kp->smem = ARBITRARY_SMEM;//arbitrary

	gpusched::GLOBALTASKS[0] = std::make_shared<gpusched::GpuTask>(0, std::move(kp), std::move(rp));

	gpusched::GLOBALJOBS[0] = std::make_shared<gpusched::Job>(0,0,0,10);

	SECTION("Free allocation (NSM) blocks")
	{
		gpusched::Occupancy base_occupancy;
		base_occupancy.allocate_resource_kernel(0);
		std::array<int, NSM> _thread_occ = base_occupancy.get_thread_occupancy();
		std::array<int, NSM> _smem_occ = base_occupancy.get_smem_occupancy();
		std::array<int, NSM> _reg_occ = base_occupancy.get_reg_occupancy();
		// kernel has NSM blocks, 1 block per sm will be allocated
		base_occupancy.free_resources(0);

		std::array<int, NSM> _thread_occ_after_free = base_occupancy.get_thread_occupancy();
		std::array<int, NSM> _smem_occ_after_free = base_occupancy.get_smem_occupancy();
		std::array<int, NSM> _reg_occ_after_free = base_occupancy.get_reg_occupancy();
		std::cout << base_occupancy.to_string();
		for (std::size_t i = 0; i < NSM; i++) {
			REQUIRE(_thread_occ[i] != _thread_occ_after_free[i]);
                        REQUIRE(_reg_occ[i] != _reg_occ_after_free[i]);
		        REQUIRE(_smem_occ[i] != _smem_occ_after_free[i]);

			REQUIRE(_thread_occ_after_free[i] == THREADS_PER_SM);
			REQUIRE(_reg_occ_after_free[i] == REGS_PER_BLOCK);
			REQUIRE(_smem_occ_after_free[i] == SMEM_PER_BLOCK);
		}
	}
}

TEST_CASE("Allocate weird  kernel from a GPU task", "[occupancy]")
{

	std::unique_ptr<gpusched::real_time_parameters> rp=
		std::make_unique<gpusched::real_time_parameters>();
		rp->bcet = 3;
		rp->wcet = 3;
		rp->period = 1600;
		rp->deadline =  1600;
		rp->priority = -1;

	std::unique_ptr<gpusched::cuda_kernel_parameters> kp =
		std::make_unique<gpusched::cuda_kernel_parameters>();

		kp->block = 50;
		kp->thread_per_block = 64;
		kp->regs = 3;//arbitrary
		kp->smem = 9551;//arbitrary

	gpusched::GLOBALTASKS[0] = std::make_shared<gpusched::GpuTask>(0, std::move(kp), std::move(rp));

	gpusched::GLOBALJOBS[0] = std::make_shared<gpusched::Job>(0,0,0,10);

	SECTION("allocation (NSM/2) blocks check if only even sm are affected")
	{
		gpusched::Occupancy base_occupancy;
		base_occupancy.allocate_resource_kernel(0);
		std::array<int, NSM> _thread_occ = base_occupancy.get_thread_occupancy();
		std::array<int, NSM> _smem_occ = base_occupancy.get_smem_occupancy();
		std::array<int, NSM> _reg_occ = base_occupancy.get_reg_occupancy();
		// kernel has 8 blocks, 1 block per sm will be allocated
		
		for (auto i = 0; i < NSM ; i++)
			std::cout << "SM[" << i << "] threads: " << _thread_occ[i] << std::endl;
		for (auto i = 0; i < NSM ; i++)
			std::cout << "SM[" << i << "] smems: " << _smem_occ[i] << std::endl;
		for (auto i = 0; i < NSM ; i++)
			std::cout << "SM[" << i << "] regs: " << _reg_occ[i] << std::endl;
	}
}

