#include <catch2/catch_test_macros.hpp>
#include "../libgpusched/include/libgpusched/vertex.hpp"

TEST_CASE( "Streams constructor", "[stream]") {

	const int _OFFSET = 5;

	SECTION("Default constructor") {
		gpusched::stream default_stream;
		REQUIRE(default_stream.stream_id == 0);
		REQUIRE(default_stream.scheduled_job_id == -1);
		REQUIRE(default_stream._free);

		for (auto it = default_stream.sm_interval.begin();
				it != default_stream.sm_interval.end();
				++it) {
			REQUIRE((*it).get_left_bound() == 0);
			REQUIRE((*it).get_right_bound() == 0);
		}
	}


	SECTION("Copy constructor") {
		gpusched::stream default_stream;
		default_stream._free = false;
		default_stream.scheduled_job_id = 1;
		for (auto it = default_stream.sm_interval.begin();
				it != default_stream.sm_interval.end();
				it++) {
			(*it).update_with_offset(_OFFSET);
		}


		gpusched::stream copy_stream(default_stream);

		REQUIRE(copy_stream.stream_id == 0);
		REQUIRE_FALSE(copy_stream._free);
		REQUIRE(copy_stream.scheduled_job_id == 1);

		for (auto it = copy_stream.sm_interval.begin();
				it != copy_stream.sm_interval.end();
				it++) {
			REQUIRE((*it).get_left_bound() == _OFFSET);
			REQUIRE((*it).get_right_bound() == _OFFSET);

		}

	}

	SECTION("Constructor with parameters") {
		gpusched::stream initialized_stream(10, 2, 5);

		REQUIRE(initialized_stream.stream_id == 10);
		REQUIRE_FALSE(initialized_stream._free);
		REQUIRE(initialized_stream.scheduled_job_id == 2);


		for (auto it = initialized_stream.sm_interval.begin();
				it != initialized_stream.sm_interval.end();
				it++) {
			REQUIRE((*it).get_left_bound() == 5);
			REQUIRE((*it).get_right_bound() == 5);
		}

	}
}

TEST_CASE("modify sm interval", "[stream]")
{
	const int _MODIFIED_SM = 2;
	const int _OFFSET = 5;
	SECTION("Modify one sm interval by adding offset") {
		gpusched::stream base_str;
		base_str.modify_sm_interval(_MODIFIED_SM,_OFFSET);

		for (std::size_t i = 0; i < NSM; i++) {
			if (i == _MODIFIED_SM) {
				REQUIRE(base_str.sm_interval[i].get_right_bound() == _OFFSET);
				REQUIRE(base_str.sm_interval[i].get_left_bound() == _OFFSET);
			} else {
				REQUIRE(base_str.sm_interval[i].get_right_bound() == 0);
				REQUIRE(base_str.sm_interval[i].get_left_bound() == 0);
			}
		}
	}

	SECTION("Modify all SMs by adding offset") {
		gpusched::stream base_str;
		base_str.modify_all(_OFFSET);

		for (auto it = base_str.sm_interval.begin();
				it != base_str.sm_interval.end();
				it++) {
			REQUIRE((*it).get_right_bound() == _OFFSET);
			REQUIRE((*it).get_left_bound() == _OFFSET);
		}

	}
}


TEST_CASE("comparaison between streams", "[stream]")
{

	gpusched::stream base_stream;
	gpusched::stream stream_1(1,1, 15);
	gpusched::stream stream_2(2,0, 10);

	SECTION("Comparaison with base_stream") {
		REQUIRE(base_stream < stream_1);
		REQUIRE(base_stream < stream_2);
	}

	SECTION("Comparaison between two initialized stream") {
		REQUIRE(stream_2 < stream_1);
	}
}



