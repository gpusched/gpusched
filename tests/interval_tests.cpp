#include <catch2/catch_test_macros.hpp>
#include "../libgpusched/include/libgpusched/interval.hpp"
#include "../libgpusched/include/libgpusched/error_t.hpp"


TEST_CASE( "Interval constructor/deconstructor", "[interval]" ) {

    gpusched::Interval default_interval;
    gpusched::Interval interval(1,5);
    // For each section, vector v is anew:
    SECTION(" Default constructor")
    {
    	REQUIRE( default_interval.get_left_bound() == 0);
    	REQUIRE( default_interval.get_right_bound() == 0);
    }

    SECTION("Constructor")
    {
    	REQUIRE( interval.get_left_bound() == 1);
    	REQUIRE( interval.get_right_bound() == 5);
    }
}

TEST_CASE( "Intersection operation", "[interval]")
{

	// interval_a must intersect with interval_b
	// but not with interval_c
	gpusched::Interval interval_a(2,5);
	gpusched::Interval interval_b(1,4);
	gpusched::Interval interval_c(6,7);

	SECTION("Valid intersection")
	{
		REQUIRE(interval_a.intersect(interval_b));
	}

	SECTION("Not valid intersection")
	{
		REQUIRE_FALSE(interval_a.intersect(interval_c));
	}

}


TEST_CASE("In operation", "[interval]")
{

	gpusched::Interval interval_a(2,5);
	gpusched::Interval interval_b(5, 10);
	gpusched::Interval interval_c(3,4);
	int time1 = 3;
	int time2 = 15;

	SECTION("Check if int value is in interval")
	{
		REQUIRE(interval_a.in(time1));
		REQUIRE_FALSE(interval_a.in(time2));
	}

	SECTION("Check in an interval is contained in another one")
	{
		REQUIRE(interval_a.in(interval_c));
		REQUIRE_FALSE(interval_a.in(interval_b));
	}
}

TEST_CASE("Disjoint operation", "[interval]")
{
	gpusched::Interval interval_a(2,4);
	gpusched::Interval interval_b(5,10);
	gpusched::Interval interval_c(4,10);

	SECTION("Check if intervals are disjoints")
	{
		REQUIRE(interval_a.disjoint(interval_b));
		REQUIRE_FALSE(interval_a.disjoint(interval_c));
	}
}


TEST_CASE("Finish later than operation")
{
	gpusched::Interval interval_a(1,4);
	gpusched::Interval interval_b(2,3);
	gpusched::Interval interval_c(2,5);
	int finish_time1 = 5;
	int finish_time2 = 3;

	SECTION("Check if an interval finish later than an int time")
	{
		REQUIRE(interval_a.finish_later_than(finish_time2));
		REQUIRE_FALSE(interval_a.finish_later_than(finish_time1));
	}


	SECTION("Check if an interval finish later than another interval")
	{
		REQUIRE(interval_a.finish_later_than(interval_b));
		REQUIRE_FALSE(interval_a.finish_later_than(interval_c));
	}
}

TEST_CASE("extend_rbound_to", "[interval]")
{
	gpusched::Interval interval_a(1,5);
	gpusched::Interval interval_b(1,5);
	int valid_offset = 10;
	int invalid_offset = 2;

	int interval_b_initial_right_bound = interval_b.get_right_bound();
	SECTION("Check extend interval with a value superior to left bound")

	SECTION("Check extend interval with a value superior to  right bound")
	{
		interval_a.extend_rbound_to(valid_offset);

		REQUIRE(interval_a.get_right_bound() == valid_offset);
	}

	SECTION("Check that interval keep the same right bound when users try to extend it with a value smaller than right bound")
	{
		// interval_b initial right bound is equal to 5
		interval_b.extend_rbound_to(invalid_offset);

		REQUIRE(interval_b.get_right_bound() == interval_b_initial_right_bound);
	}

}

TEST_CASE("extend_lbound_to", "[interval]")
{
	gpusched::Interval interval_a(1,5);
	gpusched::Interval interval_b(1,5);

	int valid_offset = 3;
	int invalid_offset = 10;

	int interval_b_initial_left_bound = interval_b.get_left_bound();
	SECTION("Check extend interval with a value superior to left bound")
	{
		interval_a.extend_lbound_to(valid_offset);

		REQUIRE(interval_a.get_left_bound() == valid_offset);
	}

	SECTION("Check that extended lbound is not greater than the current rbound")
	{
		// interval_b initial left bound is equal to 1
		interval_b.extend_lbound_to(invalid_offset);

		REQUIRE(interval_b.get_left_bound() == interval_b_initial_left_bound);
	}

}


TEST_CASE("update_wih_offset", "[interval]")
{
	gpusched::Interval interval_a(1, 5);
	gpusched::Interval interval_b(2,10);
	int valid_offset = 5;
//	int invalid_offset = -7;


	int interval_b_initial_right_bound = interval_b.get_right_bound();
	int interval_b_initial_left_bound = interval_b.get_left_bound();

	SECTION("Update with a valid offset")
	{
		interval_a.update_with_offset(valid_offset);

		REQUIRE(interval_a.get_left_bound() == 6);
		REQUIRE(interval_a.get_right_bound() == 10);

	}

//	SECTION("Update with invalid offset")
//	{
//		REQUIRE(interval_b.update_with_offset(invalid_offset) == gpusched::error::EIAV);
//
//		REQUIRE(interval_b.get_right_bound() == interval_b_initial_right_bound);
//		REQUIRE(interval_b.get_left_bound() == interval_b_initial_left_bound);
//	}
//
}
