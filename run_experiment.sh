#!/bin/bash 
# This script will run experiments with all combinaisons of job_set and task_set  file in taskset/ directory

#TASK_SET="./task_set/task*"
#JOB_SET="./task_set/job*"
TASK_SET="./task_set/umax*"
JOB_SET="./task_set/job_*"

MEASURE_TIME="./execution_time"

EXPDIR="./experiments"
BINDIR="./build"
BIN="./build/gpusched/gpusched_exe"

FLAG=$1
FULLFLAG="-wda"

rm umax*result 

if [ -d "$BINDIR" ]; then
       	rm -rf $BINDIR 
	mkdir $BINDIR 
else 
	mkdir $BINDIR
fi

if [ -d "$EXPDIR" ]; then
	rm -rf $EXPDIR
	mkdir $EXPDIR
else
	mkdir $EXPDIR
fi

if [ -f "$MEASURE_TIME" ]; then
       	rm $MEASURE_TIME 
else 
	touch $MEASURE_TIME
fi


#cd "./generator_task_set"; ./script.py;  cd -
cd "./build"; cmake -DDEBUG=OFF -DBUILD_DOC=OFF -DTEST=OFF .. ; make; cd -
#cd "./build"; cmake -DBUILD_DOC=OFF -DTEST=OFF .. ; make; cd -
#cd "./build"; cmake -DDEBUG=OFF -DBUILD_DOC=OFF  .. ; make; cd -
#cd "./build"; cmake ..; make; cd - 

shopt -s nullglob


if [ "$FLAG" == "-w" ]; then
cp ./task_set_generator/umax*small* ./task_set/
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
		#if [[ "${taskfile: -3:1}" == "${jobfile: -3:1}" ]] ; then  

			#DOT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.dot"
			#OUTPUT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.svg"
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			#DOT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: 11}_full_nodes.dot"
			#OUTPUT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: -1}_full_nodes.svg"
			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE -o $OUTPUT_FILE; cd -

			# then without to generate all nodes
			#echo "Execute gpusched -wa on ${taskfile} ${jobfile}"
			#$BIN $FULLFLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE_FULLNODE -o $OUTPUT_FILE_FULLNODE; cd - 
		#fi
	done
done

./create_plot.py exp_sf_small
mv exp_sf* ./task_set/umax* $MEASURE_TIME umax* ./small

cp ./task_set_generator/umax*large* ./task_set/
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
		#if [[ "${taskfile: -3:1}" == "${jobfile: -3:1}" ]] ; then  

			#DOT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.dot"
			#OUTPUT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.svg"
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			#DOT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: 11}_full_nodes.dot"
			#OUTPUT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: -1}_full_nodes.svg"
			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE -o $OUTPUT_FILE; cd -

			# then without to generate all nodes
			#echo "Execute gpusched -wa on ${taskfile} ${jobfile}"
			#$BIN $FULLFLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE_FULLNODE -o $OUTPUT_FILE_FULLNODE; cd - 
		#fi
	done
done

./create_plot.py exp_sf_large
mv exp_sf* ./task_set/umax* $MEASURE_TIME umax* ./large

cp ./task_set_generator/umax*mix* ./task_set/
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
		#if [[ "${taskfile: -3:1}" == "${jobfile: -3:1}" ]] ; then  

			#DOT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.dot"
			#OUTPUT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.svg"
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			#DOT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: 11}_full_nodes.dot"
			#OUTPUT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: -1}_full_nodes.svg"
			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE -o $OUTPUT_FILE; cd -

			# then without to generate all nodes
			#echo "Execute gpusched -wa on ${taskfile} ${jobfile}"
			#$BIN $FULLFLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE_FULLNODE -o $OUTPUT_FILE_FULLNODE; cd - 
		#fi
	done
done

./create_plot.py exp_sf_mix
mv exp_sf* umax* ./task_set/umax* $MEASURE_TIME ./mix

elif [ "$FLAG" == "-f" ]; then
# case -f -> EDF MAX 
cp ./task_set_generator/umax*small* ./task_set/
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
		#if [[ "${taskfile: -3:1}" == "${jobfile: -3:1}" ]] ; then  

			#DOT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.dot"
			#OUTPUT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.svg"
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			#DOT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: 11}_full_nodes.dot"
			#OUTPUT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: -1}_full_nodes.svg"
			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE -o $OUTPUT_FILE; cd -

			# then without to generate all nodes
			#echo "Execute gpusched -wa on ${taskfile} ${jobfile}"
			#$BIN $FULLFLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE_FULLNODE -o $OUTPUT_FILE_FULLNODE; cd - 
		#fi
	done
done

./create_plot.py exp_sf_mix
mv umax* ./task_set/umax* $MEASURE_TIME ./edf_max_small

cp ./task_set_generator/umax*large* ./task_set/
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
		#if [[ "${taskfile: -3:1}" == "${jobfile: -3:1}" ]] ; then  

			#DOT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.dot"
			#OUTPUT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.svg"
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			#DOT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: 11}_full_nodes.dot"
			#OUTPUT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: -1}_full_nodes.svg"
			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE -o $OUTPUT_FILE; cd -

			# then without to generate all nodes
			#echo "Execute gpusched -wa on ${taskfile} ${jobfile}"
			#$BIN $FULLFLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE_FULLNODE -o $OUTPUT_FILE_FULLNODE; cd - 
		#fi
	done
done

./create_plot.py exp_sf_mix
mv  umax* ./task_set/umax* $MEASURE_TIME ./edf_max_large

cp ./task_set_generator/umax*mix* ./task_set/
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
		#if [[ "${taskfile: -3:1}" == "${jobfile: -3:1}" ]] ; then  

			#DOT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.dot"
			#OUTPUT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.svg"
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			#DOT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: 11}_full_nodes.dot"
			#OUTPUT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: -1}_full_nodes.svg"
			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE -o $OUTPUT_FILE; cd -

			# then without to generate all nodes
			#echo "Execute gpusched -wa on ${taskfile} ${jobfile}"
			#$BIN $FULLFLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE_FULLNODE -o $OUTPUT_FILE_FULLNODE; cd - 
		#fi
	done
done

./create_plot.py exp_sf_mix
mv  umax* ./task_set/umax* $MEASURE_TIME ./edf_max_mix
else
cp ./task_set_generator/umax*small* ./task_set/
declare TASKS_FILE_ARRAY=($TASK_SET)
declare JOBS_FILE_ARRAY=($JOB_SET)
echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
for taskfile in "${TASKS_FILE_ARRAY[@]}"
do
	for jobfile in "${JOBS_FILE_ARRAY[@]}"
	do
		#if [[ "${taskfile: -3:1}" == "${jobfile: -3:1}" ]] ; then  

			#DOT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.dot"
			#OUTPUT_FILE="taskset${taskfile: -1}_jobset${jobfile: -1}.svg"
			DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
			OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"

			#DOT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: 11}_full_nodes.dot"
			#OUTPUT_FILE_FULLNODE="taskset${taskfile:11}_jobset${jobfile: -1}_full_nodes.svg"
			DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
			OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
			# execute first with domination
			echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
			$BIN $FLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE -o $OUTPUT_FILE; cd -

			# then without to generate all nodes
			#echo "Execute gpusched -wa on ${taskfile} ${jobfile}"
			#$BIN $FULLFLAG $taskfile $jobfile
			#cd $EXPDIR; dot -T svg $DOT_FILE_FULLNODE -o $OUTPUT_FILE_FULLNODE; cd - 
		#fi
	done
done
./create_plot.py exp_edf_isolation
mv umax* exp_edf_* ./task_set/umax* $MEASURE_TIME ./edf_isolation
fi
