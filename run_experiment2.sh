#!/bin/bash 
# This script will run experiments with all combinaisons of job_set and task_set  file in taskset/ directory

#TASK_SET="./task_set/task*"
#JOB_SET="./task_set/job*"
TASK_SET="./task_set/umax*"
JOB_SET="./task_set/job_*"

MEASURE_TIME="./execution_time"

EXPDIR="./experiments"
BINDIR="./build"
BIN="./build/gpusched/gpusched_exe"

FLAG=$1
FULLFLAG="-wda"

rm umax*result 

#if [ -d "$BINDIR" ]; then
#       	rm -rf $BINDIR 
#	mkdir $BINDIR 
#else 
#	mkdir $BINDIR
#fi

if [ -d "$EXPDIR" ]; then
	rm -rf $EXPDIR
	mkdir $EXPDIR
else
	mkdir $EXPDIR
fi

if [ -f "$MEASURE_TIME" ]; then
       	rm $MEASURE_TIME 
else 
	touch $MEASURE_TIME
fi


#cd "./generator_task_set"; ./script.py;  cd -
cd "./build"; cmake -DDEBUG=OFF -DBUILD_DOC=OFF -DTEST=OFF .. ; make; cd -
#cd "./build"; cmake -DBUILD_DOC=OFF -DTEST=OFF .. ; make; cd -
#cd "./build"; cmake -DDEBUG=OFF -DBUILD_DOC=OFF  .. ; make; cd -
#cd "./build"; cmake ..; make; cd - 

shopt -s nullglob


UTILIZATION_ARRAY=("0.2" "0.4" "0.6" "0.8" "1.0" "1.2" "1.4" "1.6" "1.8" "2.0")
NUMBER=("0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "23" "24" "25" "26" "27" "28" "29"
	 "30" "31" "32" "33" "34" "35" "36" "37" "38" "39" "40" "41" "42" "43" "44" "45" "46" "47" "48" "49")


echo $UTILIZATON_ARRAY
if [ "$FLAG" == "-w" ]; then
for x in "${UTILIZATION_ARRAY[@]}";
do
	
	for number in "${NUMBER[@]}";
	do
		cp ./task_set_generator/umax*${x}*_"${number}"  ./task_set_generator/job_set_exp*${x}_"${number}" ./task_set
		declare TASKS_FILE_ARRAY=($TASK_SET)
		declare JOBS_FILE_ARRAY=($JOB_SET)
		echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
		echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
		for taskfile in "${TASKS_FILE_ARRAY[@]}"
		do
			for jobfile in "${JOBS_FILE_ARRAY[@]}"
			do
		
					DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
					OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"
		
					DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
					OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
					# execute first with domination
					echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
					$BIN $FLAG $taskfile $jobfile
			done
		done
		mv ./task_set/umax*${x}*small_${number} ./small 
		mv ./task_set/umax*${x}*large_${number} ./large 
		mv ./task_set/umax*${x}*mix_${number} ./mix
		mv ./task_set/job_set_exp*${x}_${number} ./small
	done
done 
mv $MEASURE_TIME ./gpusched/ 
elif [ "$FLAG" == "-f" ]; then
# case -f -> EDF MAX 
for x in "${UTILIZATION_ARRAY[@]}";
do
	for number in "${NUMBER[@]}";
	do
		cp ./task_set_generator/umax*${x}*_${number} ./task_set_generator/job_set_exp*${x}_${number} ./task_set
		declare TASKS_FILE_ARRAY=($TASK_SET)
		declare JOBS_FILE_ARRAY=($JOB_SET)
		echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
		echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
		for taskfile in "${TASKS_FILE_ARRAY[@]}"
		do
			for jobfile in "${JOBS_FILE_ARRAY[@]}"
			do
		
					DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
					OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"
		
					DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
					OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
					# execute first with domination
					echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
					$BIN $FLAG $taskfile $jobfile
			done
		done
		mv ./task_set/umax*${x}*small_${number} ./small 
		mv ./task_set/umax*${x}*large_${number} ./large 
		mv ./task_set/umax*${x}*mix_${number} ./mix
		mv ./task_set/job_set_exp*${x}_${number} ./small
	done
done 
mv $MEASURE_TIME ./edfmax/ 
else
FLAG="-e"
for x in "${UTILIZATION_ARRAY[@]}";
do
	for number in "${NUMBER[@]}";
	do
		cp ./task_set_generator/umax*${x}*small_${number} ./task_set_generator/job_set_exp*${x}_${number} ./task_set
		declare TASKS_FILE_ARRAY=($TASK_SET)
		declare JOBS_FILE_ARRAY=($JOB_SET)
		echo "ARRAY LENGTH : ${#TASKS_FILE_ARRAY[@]}"
		echo "ARRAY LENGTH : ${#JOBS_FILE_ARRAY[@]}"
		for taskfile in "${TASKS_FILE_ARRAY[@]}"
		do
			for jobfile in "${JOBS_FILE_ARRAY[@]}"
			do
		
					DOT_FILE="${taskfile:11}_${jobfile:11}.dot"
					OUTPUT_FILE="${taskfile:11}_${jobfile:11}.svg"
		
					DOT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.dot"
					OUTPUT_FILE_FULLNODE="${taskfile:11}_${jobfile:11}_full_nodes.svg"
					# execute first with domination
					echo "Execute gpusched -w on ${taskfile} ${jobfile}"	
					$BIN $FLAG $taskfile $jobfile
			done
		done
		mv ./task_set/umax*${x}*small_${number} ./small 
		mv ./task_set/umax*${x}*large_${number} ./large 
		mv ./task_set/umax*${x}*mix_${number} ./mix
		mv ./task_set/job_set_exp*${x}_${number} ./small
	done
done 
mv $MEASURE_TIME ./edfisolation/ 
fi
