#include <libgpusched/logger.hpp>

namespace gpusched {

    Logger::~Logger(){}

    void Logger::enable_debug_level(_debug_level level) {
        if (level == _debug_level::ALL)
            debugAll = true;
        else {
            debugAll = false;
            std::vector<_debug_level> _debug_levels;
        }
    }

    void Logger::disable_debug_level(_debug_level level) {
        if (level == _debug_level::ALL)
            debugAll = false;
        else {
            std::vector<_debug_level>::iterator it =
                std::find(_debug_levels.begin(), _debug_levels.end(), level);
            if (it != _debug_levels.end())
                _debug_levels.erase(it);
        }
    }

} // namespace gpusched
