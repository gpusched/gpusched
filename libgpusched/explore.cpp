#include <libgpusched/explore.hpp>


namespace gpusched
{
//:	bool explore_space_conserving_work()
//:	{
//:
//:          std::vector<Vertex> schedulable_vertex; // vertex which have the flag
//:                                                  // schedulable set to true.
//:          std::vector<Vertex> dominance_vertex; // vertex that are dominant at a given time t.
//:          std::deque<Vertex> list_vertex;
//:
//:	  std::vector<size_t> invalid_permutation; // if we start iteration with fresh state of gpu
//:						   // we can compute it statically before the execution
//:						   // of gpusched.
//:						   // Otherwise, we can track invalid permutation
//:						   // by saving the state for which the permutation
//:						   // is invalid in the hash stored.
//:
//:          Vertex base;
//:
//:	  list_vertex.push_back(base);
//:          while (not list_vertex.empty()) {
//:		  Vertex curr_vertex = list_vertex.front();
//:		  list_vertex.pop_front();
//:
//:          	  //std::cout << curr_vertex.to_string() << std::endl;
//:
//:		  std::vector<int> rdy_jobs = curr_vertex.fetch_ready_jobs(
//:				  	      utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
//:
//:		  //  have to forward all stream to the next minimal arrival time jobs.
//:		  if (rdy_jobs.size() == 0 && (!curr_vertex.is_schedulable())) {
//:			  rdy_jobs = curr_vertex.fast_forward();
//:		  }
//:	//		  std::cout << "EMPTY RDY_JOBS" << std::endl;
//:	//	  std::cout << "READY JOBS FOR VERTEX["
//:	//		    <<  curr_vertex.get_vertex_id()
//:	//		    << "]"
//:	//		    << rdy_jobs.size() << std::endl;
//:		  std::set<std::vector<int> > permutations;
//:
//:/*==================================== Generate Combinaition ====================================*/
//:		  std::set<std::vector<int> > allocated_permutations;
//: 		  std::set<std::vector<int> > tmp_allocated;
//:		  for (long unsigned int i = 0; i != rdy_jobs.size(); i++)
//:		  {
//:			// start by higher k combinaisons to lowest
//:			// as we use work-conserving this allow us to take the permutation
//:			// that use the most the GPU
//:			// example:
//:			// 	1 2 3 is a valid combinaison
//:			// 	1 2   combinaison will be discard
//:                  	utils::get_k_combinaisons(rdy_jobs.size()-i, permutations, rdy_jobs);
//:		  	// remove all non viable permutations by checking here if they can't be
//:		  	// allocated with the curent state of the current vertex.
//:
//:			//@TODO problem here when there is 0 permutations available due to occupancy
//:			// we have multiple options
//:			//
//:			// 1 - There is no job in rdy_jobs
//:			// 	- forward until next job arrival (free resources allocated on stream
//:			// 	finishing before next job arrival time)
//:			//
//:			// 2 - There is some jobs in rdy_jobs BUT there is not enough resources in
//:			//     Occupancy to allocate.
//:			//     In this case we have to forward stream one by one until there is
//:			//     enough resources in occupancy for at least one combinaison
//:			//
//:			// How to know if the combinaisons already been checked for the current vertex?
//:			// We can use flag seeing if we already tried to allocate in the current state
//:
//:
//:		  	for (std::set<std::vector<int> >::iterator perm = permutations.begin();
//:					  perm != permutations.end();
//:					  ++perm) {
//:
//:				// As we consider work-conserving we first check if the
//:				// current combinaison is not a subset of a previous one
//:				// with more jobs running
//:				bool is_subset_of_previous_permutation = false;
//:				for (auto v_perm : allocated_permutations) {
//:					if (utils::is_subset(v_perm, *perm))
//:					{
//:						//std::cout << "BREAK" << std::endl;
//:						//utils::display_combinaison(*perm);
//:						is_subset_of_previous_permutation = true;
//:					}
//:				}
//:				// if it is a subset of a previous combinaison
//:				// we stop break the loop for this permutation
//:				if (is_subset_of_previous_permutation) {
//:					break;
//:				}
//:				//  we verify if we didn't already check similar permutation
//:				std::size_t curr_perm_hash = utils::PermutationHash{}(*perm);
//:				if (std::find(invalid_permutation.begin(),
//:					      invalid_permutation.end(),
//:					      curr_perm_hash) != invalid_permutation.end())
//:				{
//:					break;
//:				}
//:
//:				Vertex children(curr_vertex, *perm,
//:						utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
//:				// else we check if the permutation fit the GPU state
//:				//if(not children.check_occupancy_actual_state(*perm)) {
//:				  if (not children.check_occupancy_after_free_streams(*perm)) {
//:					children.free_permutation_resources_check(*perm);
//:					// @WARNING below is not a correct statement
//:					// here we can remove all permutations that contains same
//:					// combinaisons of tasks
//:					// for example
//:					// 6 4 if this permutation failed we know that
//:					// 4 6 will fail.
//:					invalid_permutation.push_back(utils::PermutationHash{}(*perm));
//:#ifdef DEBUG_MODE
//:
//:					DEBUGWARN("Start- Not enough space for the given permutation");
//:					utils::display_combinaison(*perm);
//:					DEBUGWARN("End- Not enough space for the given permutation");
//:#endif
//:				} else {
//:					std::cout << "Permutation IS ALLOCATED size: " <<
//:						(*perm).size() << std::endl;
//:					for (std::vector<int>::const_iterator it = (*perm).begin();
//:						it != (*perm).end();
//:						++it)
//:						std::cout << *it << " ";
//:
//:					children.handle_scheduled_jobs();
//:					if (children.check_scheduled_jobs_deadlines()){
//:						bool is_dominated = false;
//:						for (auto vertex: dominance_vertex) {
//:							if (vertex.check_dominance(children)) {
//:								is_dominated = true;
//:								break;
//:							}
//:						}
//:						if (!is_dominated) {
//:							list_vertex.push_back(children);
//:						}
//:					} else {
//:						std::cout << "JOBS DON'T RESPECT DEADLINE" << std::endl;
//:					}
//:					if (children.is_schedulable())
//:						schedulable_vertex.push_back(children);
//:					std::cout << "AFTER COMPUTATION NEW VERTEX CHILDREN" << std::endl;
//:					std::cout << children.to_string() << std::endl;
//:					//restore the curr_vertex to its initial state.
//:					children.free_permutation_resources_check(*perm);
//:					tmp_allocated.insert(*perm);
//:				}
//:			}
//:			for (auto q: tmp_allocated)
//:			{
//:				allocated_permutations.insert(q);
//:			}
//:			tmp_allocated.clear();
//:			permutations.clear();
//:		  }
//:			std::cout << "permutations size:" <<  permutations.size();
//:			std::cout << " allocated_permutations size: " << allocated_permutations.size() << std::endl;
//:			for (auto q: allocated_permutations)
//:				utils::display_combinaison(q);
//:		}
//:#ifdef DEBUG_MODE
//:		if (schedulable_vertex.size() != 0){
//:			DEBUGINFO("FIND PATH");
//:
//:		}
//:#endif
//:		for (auto v: schedulable_vertex){
//:			look_up(v);
//:		}
//:          return true;
//:	}
} // namespace gpusched
