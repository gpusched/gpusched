#include <libgpusched/job.hpp>

namespace gpusched
{
	std::unordered_map<int, std::shared_ptr<Job> > GLOBALJOBS = get_jobs_instance();

//	int min_arrival_time_job(const std::vector<int> remaining_job)
//	{
//		// impossible job_id to start;
//		// to avoid a return 0 (which exists in GLOBALJOBS) if no job is found in loop.
//		int job_id = GLOBALJOBS.size() + 1;
//		int minimal_arrival_time = std::numeric_limits<int>::max();
//
//		for(std::vector<int>::const_iterator it = remaining_jobs.begin();
//				it != remaining_job.end(); ++it)
//		{
//			int curr_arrival_time = GLOBALJOBS[(*it)]->get_arrival_time();
//			if (curr_arrival_time <= minimal_arrival_time){
//				minimal_arrival_time = curr_arrival_time;
//				job_id = *it;
//                        }
//                }
//#ifdef DEBUG_MODE
//		if (job_id == GLOBALJOBS.size() + 1)
//			DEBUGFATAL("No job with minimal arrival time found");
//#endif
//		return job_id;
//	}
//
	//void free_GLOBALJOBS()
	//{
	//	for(auto job: GLOBALJOBS)
	//	{
	//       		delete job.second;
	//	}
	//}

	int Job::get_job_id() const
	{
		return job_id;
	}

	int Job::get_associated_task_id() const
	{
		return associated_task_id;
	}

	int Job::get_arrival_time() const
	{
		return arrival_time;
	}

	int Job::get_relative_deadline() const
	{
		return relative_deadline;
	}

	int Job::get_associated_wcet() const
	{
		return GLOBALTASKS[associated_task_id]->get_task_wcet(); 
	}

	bool Job::check_deadline(const int starting_time) const
	{
		int wcet = GLOBALTASKS[associated_task_id]->get_task_wcet();
		return starting_time + wcet < relative_deadline;
	}

   	const std::string Job::to_string() const {
   	     	std::stringstream ss;
   	     	ss << "\e[1mJob id[" << job_id << "] associated to Task with task id["
   	           << associated_task_id << "]\e[0m\n Earliest arrival time: " << arrival_time
   	           << "\n Relative deadline: " << relative_deadline << "\n";

   	     return ss.str();
   	 }
}
