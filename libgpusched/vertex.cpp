/// @file vertex.cpp
#include <libgpusched/vertex.hpp>

namespace gpusched
{

	std::atomic<int> Vertex::_id;

	const std::string Vertex::to_string() const
	{
        	std::stringstream ss;

        	ss << "=================\e[1mVertex id: " << get_vertex_id()
        	   << "\e[0m=================\n\n"
		   << "\e[0m AT TIME t = " << time
        	   << "\n\e[1m Scheduled Jobs:\e[0m \n";

		//for(int job_id: all_scheduled_jobs)
		for (std::vector<int>::const_iterator it = all_scheduled_jobs.begin();
				it != all_scheduled_jobs.end(); ++it)
			ss << GLOBALJOBS[*it]->to_string();

		// current workload
		ss << "\n\e[1mCurrent workload\e[0m \n";

		for (std::vector<int>::const_iterator it = curr_jobs_to_schedule.begin();
				it != curr_jobs_to_schedule.end();
				it++)
			ss << GLOBALJOBS[*it]->to_string();

		ss << "\e[1m Remaining jobs \e[0m (From left to right, 1 -> not yet scheduled,"
		   <<  " 0 -> scheduled)\n"
		   << "\t" << remaining_jobs << "\n";
		ss << "\e[1m Streams state \e[0m \n";



		for (struct stream st: streams)
			ss << st.to_string();
		ss << vertex_occupancy.to_string();
                return ss.str();
        }

	void Vertex::handle_scheduled_jobs()
	{
#ifdef DEBUG_MODE
          	if (curr_jobs_to_schedule.size() == 0)
		{
          		DEBUGFATAL("Empty curr_jobs vector in Vertex[", vertex_id,"]");

		}
                else
			DEBUGINFO("Add jobs in curr_jobs");
#endif

		double slowdown_factor = -1.0;

		slowdown_factor = utils::get_sf_from_set(curr_jobs_to_schedule);


		std::cout << "SF = " << slowdown_factor << std::endl;


        	for (std::vector<int>::const_iterator it = curr_jobs_to_schedule.begin();
				it != curr_jobs_to_schedule.end();
				++it) {
			//all_scheduled_jobs.push_back(*it);
			// vector remaining_jobs.erase(...); O(n) from end
			// std flipping
			// remaining_jobs &= ~(1UL << *it);
			remaining_jobs[*it] = false;
			// erase-remove
			std::vector<int>::iterator position = std::find(ready_jobs.begin(), ready_jobs.end(), *it);
			ready_jobs.erase(position);

			int applied_sf_wcet = utils::round_up_with_sf(slowdown_factor,
						GLOBALJOBS[*it]->get_associated_wcet());
			if (!free_stream.empty()) {

//				streams[free_stream.front()].modify_all(GLOBALJOBS[*it]->get_associated_wcet());
//				update streams to vertex time if proceed per batch.
				if (streams[free_stream.front()].get_max_left_bound() < time)
					streams[free_stream.front()].extend_all(time);
				streams[free_stream.front()].modify_all(applied_sf_wcet);
				streams[free_stream.front()].set_scheduled_job_id(*it);
				free_stream.pop_front();
			} else
			{
				// create new stream
				//struct stream new_stream(++_count_stream, *it, time + GLOBALJOBS[*it]->get_associated_wcet());

				struct stream new_stream(++_count_stream, *it, time + applied_sf_wcet);
				streams.push_back(new_stream);
				//create_stream(*it);
			}

        	}

		// remove scheduled jobs from ready_jobs.

		// @TODO handle stream attribution here
		// work conserving we take in order stream that are free
		// 3 possible cases:
		// 	- no stream free for scheduled jobs (create new one for each jobs scheduled)
		// 	  and adjust their time using stream::apply_slowdown_factor_on_remaining_time().
		// 	- All scheduled jobs starting time are after the maximum finish time stream
		// 	  in this case reuse theses streams in priority and create new if we don't
		// 	  have enough streams to load the workload.
		// 	- Jobs starting time is after "some" streams finish time in this case we can
		// 	  reuse these stream but we have to adjust time on other stream using
		// 	  stream::apply_slowdown_factor_on_remaining_time().

	//	if (remaining_jobs == 0 && check_scheduled_jobs_deadlines()) std::bitset can be compared to int
		if (remaining_jobs.count() == 0 && check_scheduled_jobs_deadlines()) // need count method for boost
			schedulable = true;
	}


	std::vector<int> Vertex::fast_forward()
	{
#ifdef DEBUG_MODE
		DEBUGINFO("No job ready at the end of the maximum stream, fast forward to the next"
		          " minimal arrival time in remaining_jobs");
#endif
		int job_id;
		job_id = min_arrival_time_job();
#ifdef DEBUG_MODE
		DEBUGINFO("minimal arrival time job equal ", GLOBALJOBS[job_id]->get_arrival_time());
#endif

		// since remaining_jobs is sorted by arrival time
		// we can speed up by starting at the job id rather than 0.
		//for (std::size_t i = 0; i < remaining_jobs.size(); ++i) {
		for (std::size_t i = 0; i < remaining_jobs.size(); ++i) {
			// remaining_jobs is a bitstream we add a check if the jobs
			// is not already scheduled.
			// @TODO normally we can remove this check because we know
			// that we only fast-forward to job with an arrival time greater
			// than the current time of the vertex.
			if (remaining_jobs[i] != 0) {
				if (GLOBALJOBS[i]->get_arrival_time() <= GLOBALJOBS[job_id]->get_arrival_time() && (std::find(ready_jobs.begin(), ready_jobs.end(), i) == ready_jobs.end()))
					ready_jobs.push_back(i);
		//		else // since remaining_jobs is sorted we can instant break if we encounter
		//		     // one job with a higher arrival time than the one
		//		     // we fast forward to.
		//			break;
			}

		}
		for (struct stream &str: streams) {
			str.update_stream_to(GLOBALJOBS[job_id]->get_arrival_time());
		}

		time = GLOBALJOBS[job_id]->get_arrival_time(); // sneaky bug here care about time
							       // rather than update_stream_to
//		delta_dominance.clear(); 
		return ready_jobs;
	}

	std::vector<int> Vertex::fast_forward_to(int t)
	{

		for (std::size_t i = 0; i < remaining_jobs.size(); ++i) {
			// remaining_jobs is a bitstream we add a check if the jobs
			// is not already scheduled.
			// @TODO normally we can remove this check because we know
			// that we only fast-forward to job with an arrival time greater
			// than the current time of the vertex.
			if (remaining_jobs[i] != 0) {
				if (GLOBALJOBS[i]->get_arrival_time() <= t && (std::find(ready_jobs.begin(), ready_jobs.end(), i) == ready_jobs.end()))
					ready_jobs.push_back(i);

			}
		}
		return ready_jobs;
	}

	int Vertex::get_vertex_id() const
	{
		return vertex_id;
	}

	int Vertex::get_origin_vertex_id() const
	{
		return origin_vertex_id;
	}

	int Vertex::get_count_stream() const
	{
		return _count_stream;
	}

	bool Vertex::is_schedulable() const
	{
		return schedulable;
	}

	bool Vertex::get_invalid_vertex_flag() const
	{
		return is_invalid_vertex;
	}

	std::vector<int> Vertex::get_curr_jobs_to_schedule() const
	{
		return curr_jobs_to_schedule;
	}

	boost::dynamic_bitset<> Vertex::get_remaining_jobs() const
	{
		return remaining_jobs;
	}

	const std::set<int>& Vertex::get_delta_dominance() const
	{
		return delta_dominance;
	}

	void Vertex::update_delta_dominance(const std::vector<int>& v){
		for (auto q: v)
			delta_dominance.erase(q);
	}
	void Vertex::update_delta_dominance(int v){
		delta_dominance.erase(v);
	}

	const std::vector<struct stream>& Vertex::get_streams() const
	{
		return streams;
	}

	const std::vector<int>& Vertex::get_scheduled_jobs() const
	{
		return all_scheduled_jobs;
	}

	const std::vector<int>& Vertex::get_rdy_jobs() const
	{
		return ready_jobs;
	}

	std::vector<int> Vertex::get_copy_rdy_jobs() const
	{
		return ready_jobs;
	}

	const std::vector<int>& Vertex::get_past_vertex() const
	{
		return past_vertex;
	}

	double Vertex::get_mean_thread_usage() const
	{
		return mean_thread_usage;
	}

	double Vertex::get_mean_reg_usage() const
	{
		return mean_regs_usage;
	}

	int Vertex::get_time() const
	{
		return time;
	}
	void Vertex::set_rdy_jobs(std::vector<int> rdy_jobs)
	{
		ready_jobs = rdy_jobs;
	}
        int Vertex::min_arrival_time_job() const
	{
		// impossible job_id to start;
		// to avoid a return 0 (which exists in GLOBALJOBS) if no job is found in loop.
		int job_id = GLOBALJOBS.size() + 1;
		int minimal_arrival_time = std::numeric_limits<int>::max();
		// @TODO since we use a sorted list of jobs by arrival time
		// we can use the dynamic_bitset method find_first;
		//job_id = remaining_jobs.find_first();
		for (std::size_t i = 0; i < remaining_jobs.size(); ++i) {
			// remaining_jobs is a bitstream we add a check if the jobs
			// is not already scheduled.
			if (remaining_jobs[i] != 0) {
				int curr_arrival_time = GLOBALJOBS[i]->get_arrival_time();
				// check also if curr_arrival_time is greater than time
				// remove it if bug
				if (curr_arrival_time <= minimal_arrival_time && curr_arrival_time >= time){
					minimal_arrival_time = curr_arrival_time;
					job_id = i;
                        	}
			}
                }
#ifdef DEBUG_MODE
		if (static_cast<long unsigned int>(job_id) == GLOBALJOBS.size() + 1)
			DEBUGINFO("No job with minimal arrival time found");
#endif
		return job_id;
	}


	bool Vertex::check_occupancy_actual_state()
	{
		for (int job_id: curr_jobs_to_schedule)
			if (not vertex_occupancy.allocate_resource_kernel(job_id))
				return false;

		return true;
	}
	bool Vertex::check_occupancy_actual_state(const std::vector<int>& permutation)
	{
		bool _allocation_flag = true;
		for (int const& job_id: permutation) {
#ifdef DEBUG_MODE
			DEBUGINFO("Try to allocate GPU resources with its actual state for job[",
					job_id,"]");
#endif
			if (not vertex_occupancy.allocate_resource_kernel(job_id))
				_allocation_flag = false;
		}
		// restore previous state before check.
		//free_permutation_resources_check(permutation);
		// restore it in explore_space
		return _allocation_flag;
	}

	bool Vertex::check_occupancy_after_free_streams(const std::vector<int>& permutation)
	{
		// free resources of currently assigned jobs in streams.
		// Put all streams to the left bound of latest finish stream
		// and check the occupancy.
		int max_stream_index = get_higher_stream_index();
		//bool _allocation_flag = true;

		for (stream &str: streams) {
			if (str.get_scheduled_job_id() != -1) {
				vertex_occupancy.free_resources(str.get_scheduled_job_id());
			// @WARNING
			// was hard to detect
			str.scheduled_job_id = -1; // reset scheduled_job_id at -1
						   // or weird behavior when we'll want
						   // to check for deadline
			str._free = true;
			// fix bug when stream is push multiple time on the free_stream
			if ((std::find(free_stream.begin(), free_stream.end(), str.get_stream_id()) == free_stream.end()))
                        	free_stream.push_back(str.get_stream_id());
			}
		}

		// GPU is occupancy is totally free
		// now check the occupancy of job present in curr_jobs_to_schedule
		return vertex_occupancy.allocate_resource_batch_kernel(permutation);
		//for (int const& job_id: permutation) {
		//	if (not vertex_occupancy.allocate_resource_kernel(job_id))
		//		_allocation_flag = false;
		//}
		//std::cout << std::endl;
		//return _allocation_flag;
	}


	int Vertex::get_lower_stream_index() const
	{
		int min_stream_index = 0;

		for (long unsigned int  i = 0; i < streams.size() ; i ++)
			if (streams[i] < streams[min_stream_index])
				min_stream_index = i;

		return min_stream_index;
	}

	int Vertex::get_higher_stream_index() const
	{
		int max_stream_index = 0;
		for (long unsigned int  i = 0; i < streams.size(); i++){
			if (streams[max_stream_index] < streams[i])
				max_stream_index = i;
		}

		return max_stream_index;
	}

	int Vertex::get_depth() const
	{
		return depth;
	}


	void Vertex::dispatch_job_conserving()
	{
		if (check_occupancy_actual_state()) {
			return;
		}
        }

	bool Vertex::apply_dominance(std::vector<Vertex>& others) const
	{
		//for (Vertex& vertex: others) {
		//	if (check_dominance(vertex)){
		//		others.erase(std::find(others.begin(), others.end(), vertex));
		//	}
		//}
		return false;

	}
	error::error_t Vertex::check_dominance(const Vertex& other) const
	{
		// check if other.scheduled_jobs is a subset of all_scheduled_jobs.
		if ( not utils::is_subset<int>(other.all_scheduled_jobs,
					all_scheduled_jobs))
			return false;

		// check if other sm finish later than current vertex
		// if so we have current vertex domine other.
		// With work-conserving is if we found a SM that finish earlier in current
		// vertex.
		int curr_minimal_stream_index = get_lower_stream_index();
		int other_minimal_stream_index = other.get_lower_stream_index();

		if (streams[curr_minimal_stream_index].get_max_right_bound() <
		    other.streams[other_minimal_stream_index].get_max_right_bound())
			return true;

		// @TODO check occupancy difference too.
		return false;
	}


	bool Vertex::check_rdy_jobs()
	{
		std::set<int> related_tasks_id;

		for(auto job_id: ready_jobs){
			auto inserted = related_tasks_id.insert(GLOBALJOBS[job_id]->get_associated_task_id());
			if (!inserted.second){
				return false;
			}
		}
		return true;
	}

	bool Vertex::check_scheduled_jobs_deadlines()
	{
		//int stream_index = get_lower_stream_index();
		// get the max right bound (WCET)
		//int starting_time = streams[stream_index].get_max_right_bound();
		//if (GLOBALJOBS[job_id]->check_deadline(starting_time))
		//	return true; /// jobs can be executed on lowest stream

		//is_invalid_vertex = true;

		// check in rdy_jobs if any jobs missed deadline;
		for (int job_id: ready_jobs)
			if (GLOBALJOBS[job_id]->get_associated_wcet() + streams[get_higher_stream_index()].get_max_left_bound() > GLOBALJOBS[job_id]->get_relative_deadline()){
	//			std::cout << job_id << " THIS JOB MISSED ITS DEADLINE BEING IN READY_JOBS" << std::endl;
				return false;
			}

		// check for jobs in stream if they missed deadline
		for (auto stream: streams) {
#ifdef DEBUG_MODE
			std::cout << "stream scheduled_job: " << stream.scheduled_job_id << std::endl;
#endif
			// check for all stream if valid scheduled job
			// did not missed its deadline
			if (stream.scheduled_job_id != -1)
				if (GLOBALJOBS[stream.scheduled_job_id]->get_relative_deadline() < stream.get_max_left_bound()){
				//std::cout << "BUG IS HERE job id from a scheduled job rather than current workload job" << std::endl;
				//std::cout << "JOB MISSED DEADLINE: " << stream.scheduled_job_id
				//<< std::endl;
				//std::cout << stream.scheduled_job_id << " THIS JOB MISSED ITS DEADLINE" << std::endl;
				return false;

			}
		}
		return true;
	}

	std::vector<int> Vertex::fetch_ready_jobs(utils::schedule_type st)
	{
		//std::cout << to_string();
		// work conserving we take the last minimal moment of the
		// the current vertex to fetch new ready jobs
		int stream_index;
		int _stream_finish_time;
		switch (st)
		{
			case utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM:
				stream_index = get_higher_stream_index();
				_stream_finish_time = streams[stream_index].get_max_right_bound();
				break;
			case utils::schedule_type::WORK_CONSERVING_LOWER_STREAM:
				stream_index = get_lower_stream_index();
				_stream_finish_time = streams[stream_index].get_max_right_bound();
			default:
				stream_index = get_lower_stream_index();
				_stream_finish_time = streams[stream_index].get_max_right_bound();

		}
		// we use time to see impact

		//std::cout << "Stream finish time" << _stream_finish_time << std::endl;
		// @TODO
		// save the last time we found a jobs ready
		// so we don't restart from 0 everytime we have to fetch
		for (std::size_t i = 0; i < GLOBALJOBS.size(); i++){
			// Check if jobs is not already scheduled by checking remaining_jobs.
			if (remaining_jobs[i] != 0
			    && (GLOBALJOBS[i]->get_arrival_time() <= time)
			    && (std::find(ready_jobs.begin(), ready_jobs.end(), i) == ready_jobs.end())) {
				ready_jobs.push_back(i);
			}
		}
		//free all streams that finish before or after
		// the lower_stream_index so we can reuse it for
		// the new batch of jobs we just fetch.

		return ready_jobs;
	}

	void Vertex::set_free_stream_after_fetch(int stream_finish_time)
	{
		for (stream &str: streams)
			if (str.get_max_right_bound() <= stream_finish_time)
				str.set_free();
	}

	void Vertex::set_free_used_streams(){
		int max_stream_id = get_higher_stream_index();

		for (stream &str: streams) {
			int scheduled_job_id = str.get_scheduled_job_id();
			int stream_id = str.get_stream_id();
			if(scheduled_job_id != -1){
				vertex_occupancy.free_resources(scheduled_job_id);
			}
			str.scheduled_job_id = -1;
			str._free = true;

			if((std::find(free_stream.begin(), free_stream.end(), stream_id) == free_stream.end())) {
				free_stream.push_back(stream_id);
			}

		}
	}
	int Vertex::get_free_and_lower_stream() const
	{
		int min_right_bound = std::numeric_limits<int>::max();

		for (std::vector<struct stream>::const_iterator
				it = streams.begin();
				it != streams.end();
				++it) {
			int curr_right_bound = (*it).get_max_right_bound();
			if (curr_right_bound < min_right_bound && (*it).is_free())
				min_right_bound = curr_right_bound;
		}

		return min_right_bound;
	}

	void Vertex::free_permutation_resources_check(const std::vector<int>& permutation)
	{
		for (std::vector<int>::const_iterator it = permutation.begin();
			it != permutation.end();
			++it)
			vertex_occupancy.free_resources(*it);
	}

	void Vertex::create_stream(int job_id)
	{
#ifdef DEBUG_MODE
          DEBUGINFO("Create new stream for Vertex[", vertex_id, "]");
#endif
		stream new_stream;//@TODO modify here to use specific constructor
		new_stream.scheduled_job_id = job_id;

		int task_id = GLOBALJOBS[job_id]->get_associated_task_id();
		int lower_stream_index = get_lower_stream_index();

		// max between the maximum right and arrival time, this assure that a job
		// not always start a its arrival_time
		int starting_time = std::max(GLOBALJOBS[job_id]->get_arrival_time(),
				streams[lower_stream_index].get_max_right_bound());
		// If deadline is missed we don't generate stream and set
		// is_invalid_vertex to true.
		if (GLOBALJOBS[job_id]->check_deadline(starting_time)) {
			is_invalid_vertex = true;
			return;
		}

		new_stream.update_stream_to(starting_time + GLOBALTASKS[task_id]->get_task_wcet());
		new_stream.set_scheduled_job_id(job_id);
		streams.push_back(new_stream);
        }

	void Vertex::reuse_stream(int job_id)
	{
		int lower_stream_id = get_lower_stream_index();
		// move this check on a upper level function handle_stream_dispatch after
		//if (streams[lower_stream_id].get_max_right_bound() > GLOBALJOBS[job_id])
		//	create_stream(job_id);
	}

	void look_up(const Vertex& v)
	{
		Vertex tmp = v;
		std::cout << tmp.to_string() << std::endl;
		while (tmp.get_vertex_id() != 1) {
			tmp = tmp.get_origin_vertex();
			std::cout << tmp.to_string() << std::endl;
		}

		path_jobs_order(v);

		std::cout << std::endl;
	}

	void path_jobs_order(const Vertex& v)
	{

		Vertex tmp = v;
		std::vector<std::vector<int> > path_order_jobs;
		path_order_jobs.push_back(v.get_curr_jobs_to_schedule());
		while (tmp.get_vertex_id() != 1) {
			tmp = tmp.get_origin_vertex();
			path_order_jobs.push_back(tmp.get_curr_jobs_to_schedule());
		}
		int cmpt = 0;
		std::cout << "JOBS ORDER" << std::endl;
		for (auto it = std::prev(path_order_jobs.end()); it != path_order_jobs.begin();
				it--) {
			if ((*it).size() >1) {
				if (cmpt != 0)
					std::cout << " --> ";
				std::cout << "[";
				for (auto itt = (*it).begin(); itt != std::prev((*it).end()); ++itt)
					std::cout << (*itt)<< "|";
				std::cout << *(std::prev((*it).end())) << "]";
				cmpt++;
			} else if ((*it).size() == 1) {
				if (cmpt != 0)
					std::cout << " --> ";
				std::cout << (*it)[0];
				cmpt++;
			}
		}

		auto it = path_order_jobs.begin();
		if ( (*it).size() > 1) {
			std::cout << " --> ";
			std::cout << "[";
			for (auto itt = (*it).begin(); itt != std::prev((*it).end());  ++itt)
				std::cout << (*itt) << "|";
			std::cout << *(std::prev((*it).end())) << "]";
		}
		else {
			std::cout << " --> ";
			std::cout << (*it)[0];
		}
		std::cout << std::endl;
	}

	int Vertex::get_next_job_edf(std::vector<int> jobs_id){

		int min_deadline = std::numeric_limits<int>::max();
		int min_deadline_job_id = -1;
		for(auto job_id: jobs_id){
			int rdl = GLOBALJOBS[job_id]->get_relative_deadline();
			if (min_deadline > rdl) {
				min_deadline = rdl;
				min_deadline_job_id = job_id;
			}
		}
		return min_deadline_job_id;
	}


	int Vertex::get_next_job_edf(){

		int min_deadline = std::numeric_limits<int>::max();
		int min_deadline_job_id = -1;
		for(auto i= 0; i < remaining_jobs.size(); i++){
			if( remaining_jobs[i] != 0) {
			int rdl = GLOBALJOBS[i]->get_relative_deadline();
			if (min_deadline > rdl) {
				min_deadline = rdl;
				min_deadline_job_id = i;
			}
			}
		}
		if(min_deadline_job_id != -1)
			time = GLOBALJOBS[min_deadline_job_id]->get_arrival_time();
		fetch_ready_jobs(schedule_t);
		return min_deadline_job_id;
	}

	int Vertex::get_next_job_edf2(){

		int min_deadline = std::numeric_limits<int>::max();
		int min_deadline_job_id = -1;
		for(auto i= 0; i < remaining_jobs.size(); i++){
			if( remaining_jobs[i] != 0 && std::find(ready_jobs.begin(), ready_jobs.end(),i) != ready_jobs.end() && std::find(curr_jobs_to_schedule.begin(), curr_jobs_to_schedule.end(), i) == curr_jobs_to_schedule.end()) {
			int rdl = GLOBALJOBS[i]->get_relative_deadline();
			if (min_deadline > rdl) {
				min_deadline = rdl;
				min_deadline_job_id = i;
			}
			}
		}
		if(min_deadline_job_id != -1){
			time = GLOBALJOBS[min_deadline_job_id]->get_arrival_time();
			curr_jobs_to_schedule.push_back(min_deadline_job_id);
		}
		return min_deadline_job_id;
	}
} // namespace gpusched
