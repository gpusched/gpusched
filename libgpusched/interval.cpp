/// @file interval.cpp
#include <libgpusched/interval.hpp>

namespace gpusched
{

	int Interval::get_left_bound() const
	{
		return left_bound;
	}

	int Interval::get_right_bound() const
	{
		return right_bound;
	}

	bool Interval::in(const Interval& other) const
	{
		return (left_bound <= other.left_bound && other.right_bound <= right_bound);
        }

        bool Interval::in(int time) const
	{
		return (left_bound <= time && time <= right_bound);
	}

        bool Interval::in_right_exclusive(int time) const
	{
		return (left_bound < time && time <= right_bound);
	}

	bool Interval::is_greater(int t) const{
		return right_bound < t;
	}
	bool Interval::intersect(const Interval& other) const
	{
		return not disjoint(other);
	}

	bool Interval::disjoint(const Interval& other) const
	{
		return (right_bound < other.left_bound || other.right_bound < left_bound);
	}

	bool Interval::finish_later_than(const Interval& other) const
	{
		return (other.right_bound < right_bound);
	}

	bool Interval::finish_later_than(int time) const
	{
		return (time < right_bound);
	}


	void Interval::extend_to(int bound)
	{
		right_bound = std::max(bound, right_bound);
		left_bound  = std::max(bound, left_bound);
	}

	void Interval::extend_rbound_to(int rbound)
	{
		right_bound = std::max(rbound, right_bound);
	}

	void Interval::extend_lbound_to(int lbound)
	{
		// check if the new lbound is not greater
		// than the current lbound if so we discard the change
		if (lbound > right_bound)
			return;

		left_bound = std::max(lbound, left_bound);
	}


	void Interval::update_with_offset(int offset)
	{

		right_bound += offset;
		left_bound += offset;
	}

	void Interval::update_rbound_with_offset(int offset)
	{
		right_bound += offset;
	}

	void Interval::update_lbound_with_offset(int offset)
	{
		left_bound += offset;
	}

	Interval Interval::operator+(int offset) const
	{
		Interval new_interval;
		new_interval.left_bound = this->left_bound + offset;
		new_interval.right_bound = this->right_bound + offset;
		return new_interval;
	}
}
