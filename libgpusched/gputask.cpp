/// @file gputask.cpp
#include <libgpusched/gputask.hpp>

namespace gpusched
{

	GpuTask::~GpuTask()
	{
		cuda_params.reset();
	};


    	std::string GpuTask::to_string() {
    	    std::stringstream ss;
    	    char separator = ' ';
    	    int width = 25;

    	    ss << "\n\e[1mGPU Kernel parameters: \e[0m \n"
    	       << std::left << std::setw(width) << std::setfill(separator) << "Block"
    	       << std::left << std::setw(width) << std::setfill(separator) << "Thread per Block"
    	       << std::left << std::setw(width) << std::setfill(separator) << "Registers"
    	       << std::left << std::setw(width) << std::setfill(separator) << "smem"
    	       << "\n";

    	    ss << std::left << std::setw(width) << std::setfill(separator)
	       << cuda_params->block
    	       << std::left << std::setw(width) << std::setfill(separator)
	       << cuda_params->thread_per_block
    	       << std::left << std::setw(width) << std::setfill(separator)
	       << cuda_params->regs
    	       << std::left << std::setw(width) << std::setfill(separator)
	       << cuda_params->smem
    	       << "\n";
    	    return Task::to_string() + ss.str();
    	}

	cuda_kernel_parameters* GpuTask::get_kernel_params() const
	{
		return cuda_params.get();
	}

	int GpuTask::get_block() const
	{
		return cuda_params-> block;
	}

	int GpuTask::get_thread_per_block() const
	{
		return cuda_params-> thread_per_block;
	}

	int GpuTask::get_smem() const
	{
		return cuda_params-> smem;
	}

	int GpuTask::get_reg() const
	{
		return cuda_params-> regs;
	}

} // namespace gpusched
