#include <libgpusched/vertexBatch.hpp>

namespace gpusched
{

	void VertexBatch::handle_scheduled_jobs()
	{

#ifdef DEBUG_MODE
          	if (curr_jobs_to_schedule.size() == 0)
		{
          		DEBUGFATAL("Empty curr_jobs vector in Vertex[", vertex_id,"]");

		}
                else
			DEBUGINFO("Add jobs in curr_jobs");
#endif

		double slowdown_factor = -1.0;
		slowdown_factor = utils::get_sf_from_set(curr_jobs_to_schedule);

#ifdef DEBUG_MODE
		std::cout << "SF = " << slowdown_factor << std::endl;
#endif


        	for (std::vector<int>::const_iterator it = curr_jobs_to_schedule.begin();
				it != curr_jobs_to_schedule.end();
				++it) {
			all_scheduled_jobs.push_back(*it);
			// vector remaining_jobs.erase(...); O(n) from end
			// std::bitset flipping
			//remaining_jobs &= ~(1UL << *it);

			//boost flipping
			remaining_jobs[*it] = false;

			// erase-remove
			std::vector<int>::iterator position = std::find(ready_jobs.begin(), ready_jobs.end(), *it);
			ready_jobs.erase(position);

			int associated_task_id = GLOBALJOBS[*it]->get_associated_task_id();


			std::shared_ptr<GpuTask> curr_gpu_task;
			if ((curr_gpu_task = std::dynamic_pointer_cast<GpuTask>(
				     GLOBALTASKS[associated_task_id])) == nullptr)
			{
#ifdef DEBUG_MODE
				DEBUGFATAL("task with id: ", associated_task_id, " is not a gpu task");
#endif
				return; 
			}

			int applied_sf_wcet = utils::round_up_with_sf(slowdown_factor,
						GLOBALJOBS[*it]->get_associated_wcet());

			mean_thread_usage += (curr_gpu_task->get_block()
					      *curr_gpu_task->get_thread_per_block()
					      *applied_sf_wcet)
					      /(THREADS_PER_SM * NSM);

			mean_regs_usage += (curr_gpu_task->get_block()
					      *curr_gpu_task->get_thread_per_block()
					      *applied_sf_wcet
					      *curr_gpu_task->get_reg())
					      /(THREADS_PER_SM * NSM);

			if (!free_stream.empty()) {

//				streams[free_stream.front()].modify_all(GLOBALJOBS[*it]->get_associated_wcet());
//				update streams to vertex time if proceed per batch.
				if (streams[free_stream.front()].get_max_left_bound() < time)
					streams[free_stream.front()].extend_all(time);
				streams[free_stream.front()].modify_all(applied_sf_wcet);
				streams[free_stream.front()].set_scheduled_job_id(*it);
				free_stream.pop_front();
			} else
			{
				// create new stream
				struct stream new_stream(++_count_stream, *it, time + applied_sf_wcet);
				new_stream.set_scheduled_job_id(*it);
				streams.push_back(new_stream);
				//create_stream(*it);
			}

        	}

		// remove scheduled jobs from ready_jobs.

		// @TODO handle stream attribution here
		// work conserving we take in order stream that are free
		// 3 possible cases:
		// 	- no stream free for scheduled jobs (create new one for each jobs scheduled)
		// 	  and adjust their time using stream::apply_slowdown_factor_on_remaining_time().
		// 	- All scheduled jobs starting time are after the maximum finish time stream
		// 	  in this case reuse theses streams in priority and create new if we don't
		// 	  have enough streams to load the workload.
		// 	- Jobs starting time is after "some" streams finish time in this case we can
		// 	  reuse these stream but we have to adjust time on other stream using
		// 	  stream::apply_slowdown_factor_on_remaining_time().

		if (remaining_jobs.count() == 0 && check_scheduled_jobs_deadlines())
			schedulable = true;

	}


	bool VertexBatch::apply_dominance(std::vector<VertexBatch>& others) const
	{

		// check if other.scheduled_jobs is a subset of all_scheduled_jobs.
		bool is_dominated = false;

	  	graph_viz::GraphGen* _graph = graph_viz::GraphGen::getInstance();

		//others.erase(std::remove_if(others.begin(), others.end(),
		//			   [&](const Vertex& v){
		//				return check_dominance(v) == error::EDOMINE;
		//			   }), others.end());
		for (std::vector<VertexBatch>::const_iterator it = others.begin();
				it != others.end(); ) {
			if (not utils::is_subset<int>((*it).get_scheduled_jobs(),
						all_scheduled_jobs)){
				it++;
				continue; // no change
			}

			error::error_t dominance = check_dominance((*it));
			if (dominance == error::EDOMINATED){
				is_dominated = true;
				_graph->add_domination_relation(*this, (*it));
				break;
			}
			else if (dominance == error::EDOMINE) {

				_graph->add_domination_relation((*it), *this);
				// remove all children from the vertex just erased
				for (std::vector<VertexBatch>::const_iterator itt= others.begin();
						itt != others.end(); )
				{
					// compare only previous vertex
					// changed to compare with all predecessors vertex
					//if ((*itt).get_origin_vertex_id() == (*it).get_vertex_id()){
					auto past_vertex_of_itt = (*itt).get_past_vertex();
					//std::cout << past_vertex_of_itt.size() << std::endl;
					if (std::find(past_vertex_of_itt.begin(),
						      past_vertex_of_itt.end(),
						      (*it).get_vertex_id()) != past_vertex_of_itt.end()) {
                                                _graph->add_domination_relation((*itt), *this);
						//std::cout << "ERASE A VERTEX BECAUSE ONE OF ITS PREVIOUS VERTEX GOING DELETED" << std::endl;
						itt = others.erase(itt);
                                        }
					else
						++itt;
				}

				it = others.erase(it);
				break;
			} else
				++it;
		}

		if (!is_dominated){
			others.push_back((*this));
			return true;
		}

		return false;
	}

	error::error_t VertexBatch::check_dominance(const Vertex& other) const
	{

		if ( not utils::is_subset<int>(other.get_scheduled_jobs(),
					all_scheduled_jobs))
			return error::ENOTSUBSET;

		// check if other sm finish later than current vertex
		// if so we have current vertex domine other.
		// With work-conserving is if we found a SM that finish earlier in current
		// vertex.
		int curr_maximal_stream_index = get_higher_stream_index();
		int other_maximal_stream_index = other.get_higher_stream_index();

		const std::vector<struct stream> other_streams = other.get_streams();

		//for (auto job: all_scheduled_jobs) // compare finish time between all jobs.

		// upper bound check
		// J1 = other scheduled jobs, t1 is the time at which the last jobs of J1 finished
		// J2 = vertex scheduled jobs, t2 is the time at which the last jobs of J2 finished
		//  D = J1\J2, jobs in J1 that are not yet scheduled by other vertex
		// t2' = t2  + (sum_[C(D)] / #max_resources)
		// if t2' > t1 then other vertex dominate the current vertex


		// J1\J2
		std::vector<int> diff = utils::difference_set(other.get_scheduled_jobs(),
					all_scheduled_jobs);

	//	if (diff.size() == 0)
	//		if (streams[curr_maximal_stream_index].get_max_right_bound() <
	//	    		other_streams[other_maximal_stream_index].get_max_right_bound())
	//			return error::EDOMINE;
			//else if(streams[curr_maximal_stream_index].get_max_right_bound() >
			//		other_streams[other_maximal_stream_index].get_max_right_bound())
			//	return error::EDOMINATED;

		// t1
		int other_vertex_max_time = other_streams[other_maximal_stream_index].get_max_right_bound();
		// t2
		int curr_vertex_max_time  = streams[curr_maximal_stream_index].get_max_right_bound();

		int accumulate_ci = 0;

		//utils::display_combinaison(all_scheduled_jobs);
		//utils::display_combinaison(other.get_scheduled_jobs());
		//utils::display_combinaison(diff);
		for (auto job_id: diff) {
			// first acculumate_ci without ai first solution for dominance relation
			//accumulate_ci += GLOBALJOBS[job_id]->get_associated_wcet();


			int offset = 0;
			// second one with arrival time of job
			// If job is not yet arrived at vertex time, we have to take into account
			// the time at which it'll arrive
		//	offset = curr_vertex_max_time - GLOBALJOBS[job_id]->get_arrival_time();
			offset = GLOBALJOBS[job_id]->get_arrival_time() - curr_vertex_max_time;
			if (offset > 0)
				accumulate_ci += GLOBALJOBS[job_id]->get_associated_wcet() + offset;
			else
				accumulate_ci += GLOBALJOBS[job_id]->get_associated_wcet();


		}

		// third one by comparing also ready_jobs.
		// quickly generate the list from here without fast_forward to not break
		// the code
		//
		//
		// We want to compare the ready_list for jobs arriving after v2
		// and the list arriving after v1 + all accumulate ci from v2 that
		// v1 doesn't scheduled yet
		// if ready_list v1 is not contained in ready_list v2
		// it's not subset and v1 is not dominated
		// see counter example 1
		std::vector<int> curr_ready_after_accumulate; // v1' -> 5
		std::vector<int> other_ready; // v1 -> 4
		for (auto i = 0; i < GLOBALJOBS.size(); i++){

			// here we take only jobs in interval [other_finished, curr_finished] in the case where curr_finished finish after other_finished
			// [4,5] -> [j3, j4]
			if ((GLOBALJOBS[i]->get_arrival_time() <= (curr_vertex_max_time + accumulate_ci)) && (GLOBALJOBS[i]->get_arrival_time() > other_vertex_max_time))
				curr_ready_after_accumulate.push_back(GLOBALJOBS[i]->get_job_id());
			// here we just take all jobs ready at the end of the other vertex to compare
			else if (GLOBALJOBS[i]->get_arrival_time() <= (other_vertex_max_time))
				other_ready.push_back(GLOBALJOBS[i]->get_job_id());
		}

		// here with the example we'll have
		// other_ready = [4]
		// curr_ready = [4,5]
		// since curr_ready is not a subset of other_ready we can't say that other vertex dominate
		// the current vertex
		if (not utils::is_subset<int>(other_ready,
					curr_ready_after_accumulate))
			return error::ENOTSUBSET;
		//std::cout << accumulate_ci << std::endl;
		int future_time = curr_vertex_max_time + accumulate_ci;
		//std::cout << "future time: " << future_time << " other_vertex_max_time:" << other_vertex_max_time << std::endl;
		//std::cout << "CURR_VERTEX: " << curr_vertex_max_time
		//	  << " ACCUMULATE_CI: " << accumulate_ci
		//	  << " FUTURE TIME = "
		//	  << future_time
		//	  << " OTHER VERTEX TIME = "
		//	  << other_vertex_max_time
		//	  << std::endl;
		if (future_time >= other_vertex_max_time)
			return error::EDOMINATED;
                return error::EDOMINE;
	}
} // namespace
