/// @file occupancy.cpp
#include <libgpusched/occupancy.hpp>

namespace gpusched
{
	Occupancy::Occupancy()
	{
		// start by even sm;
		current_sm = 0;

		// all ressources are available at start
		std::fill_n(smem_occupancy.begin(), NSM, SMEM_PER_BLOCK);
		std::fill_n(reg_occupancy.begin(), NSM, REGS_PER_BLOCK);
		std::fill_n(thread_occupancy.begin(), NSM, THREADS_PER_SM);
	}

	bool Occupancy::allocate_resource_batch_kernel(const std::vector<int>& jobs_id)
	{
		int save_sm = current_sm;
		for (std::vector<int>::const_iterator it = jobs_id.begin();
				it != jobs_id.end();
				++it){

			if (not allocate_resource_kernel(*it))
			{
				current_sm = save_sm; // restore sm before batch checking.
				return false; // permutation can't be allocated.
			}
		}
		return true;
	}
	bool Occupancy::allocate_resource_kernel(int job_id)
	{
		int associated_task_id = GLOBALJOBS[job_id]->get_associated_task_id();
		// check if the task is gputask object
		std::shared_ptr<GpuTask> curr_gpu_task;
		if ((curr_gpu_task = std::dynamic_pointer_cast<GpuTask>(
			     GLOBALTASKS[associated_task_id])) == nullptr)
		{
#ifdef DEBUG_MODE
			DEBUGFATAL("task with id: ", associated_task_id, " is not a gpu task");
#endif
			return false;
		}

		std::unique_ptr<job_allocation_cache> cache =
			std::make_unique<job_allocation_cache>();
		cache->job_id = job_id;

		int save_curr_sm = current_sm;
		for (int i = 0; i < curr_gpu_task->get_block(); i++) {
			bool allocated = allocate_resource_one_block(job_id,
					associated_task_id,
					curr_gpu_task->get_reg(),
					curr_gpu_task->get_thread_per_block(),
					curr_gpu_task->get_smem(),
					current_sm);
			// @TODO not well implemented
			// If the next sm can't allocate we have to check next one until we
			// checked all the loop of sm.
			// If after the loop there is still no sm available we can say that
			// we ain't be able to allocate the block.

			while(!allocated) {
				if (not allocated && cache->failed_allocation_sm.size() == NSM) {
					// if one of the block composing the kernel associated to
					// job_id we free resources if any block has been allocated.
					//free_resources(job_id);
					current_sm = save_curr_sm;//restore sm;
					//std::cout << "FAILED ON ALL SM" << std::endl;
					return false;
				} else if (not allocated) {
					// store the sm on which the allocation failed in cache
					// and set current_sm to the next_sm where we may allocate
					// resources.
					cache->failed_allocation_sm.push_back(current_sm);
					current_sm = compute_next_sm(current_sm);
					//std::cout << "Try block on" << current_sm << std::endl;
					// @TODO deal with the get_next_sm function to reduce the number of
					// loop; current_sm = cache->get_next_sm(); // for the next loop
				}
			}
		}

		return true;
        }

	bool Occupancy::allocate_resource_one_block(int job_id,
			int task_id,
			int regs,
		       	int thread_per_block,
			int smem,
			int sm) // sm parameter not necessary if we use only curent_sm
	{
		// Check for gpuTask done on allocateResourceKernel

		// Check if we can allocate on the specified sm
		// have to check for multiple  sm if not possible here
		if (can_allocate_resources(regs * thread_per_block, thread_per_block, smem, sm)) {
#ifdef DEBUG_MODE
			DEBUGINFO("Allocate ressource on sm[", sm,"]  job [", job_id,
		        			"], of task[", task_id,"]");
#endif
			// TODO fix batch allocation (((regs * threadPerBlock)/256)+1) * 256
			// use granularity allocation
			// same for smem
			reg_occupancy[sm] -= regs * thread_per_block; // regs are allocated
								    // per thread
								    // and by batch.
			thread_occupancy[sm] -= thread_per_block;
			smem_occupancy[sm] -= smem; // same by batch
			add_block_allocated_to_sm(job_id, sm);

		  	// TODO change here if we only use curr_sm
			current_sm = compute_next_sm(sm);

			return true;
		}
		return false;
	}


	void Occupancy::free_resources(const int job_id)
	{

#ifdef DEBUG_MODE
		DEBUGINFO("Free resources occupied by job[", job_id,"]");
#endif
		int associated_task_id = GLOBALJOBS[job_id]->get_associated_task_id();
		std::shared_ptr<GpuTask> curr_gpu_task;
		if ((curr_gpu_task = std::dynamic_pointer_cast<GpuTask>
					(GLOBALTASKS[associated_task_id])) == nullptr) {
#ifdef DEBUG_MODE
			DEBUGFATAL("Task with id: ", associated_task_id, " is not a gpu task");
#endif
			return;
		}

		std::vector<std::pair<int, int> >& mapping_curr_job_resources
			= allocated_blocks[job_id];

		for (std::vector<std::pair<int, int> >::const_iterator it =
				mapping_curr_job_resources.begin();
				it != mapping_curr_job_resources.end(); ++it) {
			reg_occupancy[(*it).second]+=  (*it).first * (curr_gpu_task->get_reg()
					               * curr_gpu_task->get_thread_per_block());
			thread_occupancy[(*it).second]+= (*it).first
						         * curr_gpu_task->get_thread_per_block();
			smem_occupancy[(*it).second]+= (*it).first * curr_gpu_task->get_smem();
		}

		// remove job entry from allocated_block.
		allocated_blocks.erase(job_id);
	}

	void Occupancy::add_block_allocated_to_sm(int job_id, int sm)
	{
#ifdef DEBUG_MODE
		DEBUGINFO("Enter add_block_allocated_to_sm");
#endif
		// check if task has already allocated a block on specified sm
		// if so increment number of block allocated
		// else create an entry in allocated_blocks
		if (allocated_blocks.find(job_id) != allocated_blocks.end()) {
			//get element in vector allocated_blocks vector
			//that correspond to the specifed sm
			auto entry = std::find_if( allocated_blocks[job_id].begin(),
					allocated_blocks[job_id].end(),
					[&sm](const std::pair<int, int>& element)
					{
					return element.second == sm;
					});

			if (entry != allocated_blocks[job_id].end())
				(*entry).first++; // increment if found
			else {
				// otherwise create a new entry
				// initialize at 1 block and sm
				allocated_blocks[job_id].push_back(std::make_pair(1, sm));
			}
		} else {
#ifdef DEBUG_MODE
			DEBUGINFO("Create entry in allocated_blocks");
#endif
			// if job_id is not found in allocated_blocks
			// create an entry with job_id as key and add a pair to it
			allocated_blocks[job_id].push_back(std::make_pair(1, sm));
		}
	}

	std::vector<std::pair<int, int> >
		Occupancy::get_entry_blocks_allocated(int job_id)
	{
		return allocated_blocks[job_id];
	}

	int Occupancy::get_current_sm() const
	{
		return current_sm;
	}

	std::array<int, NSM> Occupancy::get_smem_occupancy() const
	{
		return smem_occupancy;
	}

	std::array<int, NSM> Occupancy::get_thread_occupancy() const
	{
		return thread_occupancy;
	}

	std::array<int, NSM> Occupancy::get_reg_occupancy() const
	{
		return reg_occupancy;
	}

	void Occupancy::reset() {
		std::fill_n(smem_occupancy.begin(), NSM, SMEM_PER_BLOCK);
		std::fill_n(reg_occupancy.begin(), NSM, REGS_PER_BLOCK);
		std::fill_n(thread_occupancy.begin(), NSM, THREADS_PER_SM);

		current_sm = 0;
		allocated_blocks.clear();
	}
	const std::string Occupancy::to_string() const
	{
        	std::stringstream ss;
        	char separator = ' ';
        	int width = 25;

        	ss << "=================\e[1m Occupancy: " << "\e[0m=================\n\n"
        	   << std::left << std::setw(width) << std::setfill(separator) << ""
        	   << std::left << std::setw(width) << std::setfill(separator) << "Registers"
        	   << std::left << std::setw(width) << std::setfill(separator) << "Threads"
        	   << std::left << std::setw(width) << std::setfill(separator) << "Shared memory"
        	   << "\n";

		std::string sm_i;
		for (int i = 0; i < NSM; i++) {
			sm_i =  "sm[" + std::to_string(i) +"]";
        		ss << std::left << std::setw(width) << std::setfill(separator)
		   	   << sm_i;
			ss << std::left << std::setw(width) << std::setfill(separator)
		   	   << reg_occupancy[i]
			   << std::left << std::setw(width) << std::setfill(separator)
			   << thread_occupancy[i]
			   << std::left << std::setw(width) << std::setfill(separator)
			   << smem_occupancy[i]
        	   	   << "\n";
		}

		return ss.str();
	}
} // namespace gpusched
