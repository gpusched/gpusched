#ifndef __COMMON__H
#define __COMMON__H

namespace gpusched {

// Following define are used to display log in std::cerr.
#define RED "\033[1;31m"
#define GREEN "\033[1;32m"
#define YELLOW "\033[1;33m"
#define RESET "\033[0m"

    const unsigned int nb_task = 3;

    /*
     * Neither copyable or movable class.
     *
     * This helper class should be used to declare classes that are not
     * movable nor copyable.
     *
     * @code
     * class Example : private gpusched::utils::NeitherCopyOrMove
     * {
     *   (code)
     * }
     */
    class NeitherCopyOrMove {
    public:
        NeitherCopyOrMove() = default;
        NeitherCopyOrMove(const NeitherCopyOrMove &) = delete;
        NeitherCopyOrMove(NeitherCopyOrMove &&) = delete;
        NeitherCopyOrMove &operator=(const NeitherCopyOrMove &) = delete;
        NeitherCopyOrMove &operator=(NeitherCopyOrMove &&) = delete;
        ~NeitherCopyOrMove() = default;
    };
} // namespace gpusched

#endif
