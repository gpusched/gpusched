/// @file job.hpp
#ifndef __JOB__HPP
#define __JOB__HPP

#include <libgpusched/logger.hpp>
#include <libgpusched/task.hpp>

// std include
#include <map>
#include <limits>
namespace gpusched
{

	enum class STATE : int {RUNNING, WAITING};

	//typedef struct
	//{
	//	bool hasBeenDispatched;
	//	std::map<int, int> sm; // gpu job
	//} dispatched;

	/**
	 * @brief
	 * Class job represents a job of an associated task.
	 */
	class Job final
	{
		/// @brief unique identifier
		int job_id;

		/// @brief associated task unique identifier
		int associated_task_id;

		/// @brief time at which job is ready to be executed
		int arrival_time;

		/// @brief time before job must finish its execution
		int relative_deadline;

		STATE state;

		/// @brief flag to know if the current job is associated with a GPU Task
		bool is_gpu_job;

		public:

		/**
		 * @brief
		 * Constructor
		 *
		 * @param jid 			a job unique identifier
		 * @param tid			a task unique identifier
		 * @param arrival_time 		time at which the job is ready to be executed
		 * @param deadline		time before the job must finish its execution
		 *
		 */
		Job(int jid,
		    int tid,
		    int arrival_time,
		    int deadline): job_id(jid),
					    associated_task_id(tid),
					    arrival_time(arrival_time),
					    relative_deadline(deadline),
					    state(STATE::WAITING)
		{
			if(GLOBALTASKS.find(tid) != GLOBALTASKS.end())
				is_gpu_job = GLOBALTASKS[tid] -> is_gpu_task();
		}

		~Job(){};

		/**
		 * @brief
		 * String format description of the current job.
		 *
		 * @return std::string 		The description in string format.
		 */
		const std::string to_string() const;

                /**
                 * @brief
                 * Getter for the current job unique identifier.
                 *
                 * @return 	the current job unique identifier.
                 */
                int get_job_id() const;

		/**
		 * @brief
		 * Getter for the current job associated task unique identifier.
		 *
		 * @return 	the associated task unique identifier.
		 */
		int get_associated_task_id() const;

		/**
		 * @brief
		 * Getter for the current job arrival time.
		 *
		 * @return 	the current job arrival time.
		 */
		int get_arrival_time() const;

		/**
		 * @brief
		 * Getter for the current job relative deadline.
		 *
		 * @return 	the current job relative deadline.
		 */
		int get_relative_deadline() const;

		int get_associated_wcet() const;

		/**
		 * @brief
		 * Check if the job deadline is respected form a given starting time.
		 *
		 * @param starting_time		the given time.
		 */
		bool check_deadline(const int starting_time) const;

	};


	Job* get_job_by_id(int jobId);


	//static std::unordered_map<int, Job*> &get_jobs_instance()
	//{
	//	static std::unordered_map<int, Job *> JOBS;
	//	return JOBS;
	//}

	// use unused attribute to avoid warning for unused function in file that indirectly
	// include the job.hpp file.
	__attribute__((unused)) static std::unordered_map<int, std::shared_ptr<Job> > &get_jobs_instance()
	{
		static std::unordered_map<int, std::shared_ptr<Job>> JOBS;
		return JOBS;
	}

	/// @brief Global map GLOBALJOBS contains all applications jobs.
	//extern std::unordered_map<int, Job*> GLOBALJOBS;

	extern std::unordered_map<int, std::shared_ptr<Job> > GLOBALJOBS;

	const int globaljobs_size();

	/**
	 * @brief
	 * Give the job with the minimal arrival time from a given subset of GLOBALJOBS
	 *
	 * @param remaining_jobs	subset of job id present in GLOBALJOBS.
	 *
	 * @return int		job id of job with the minimal arrival time present in
	 * 				GLOBALJOBS.
	 */
//	int min_arrival_time_job(const std::vector<int> remaining_jobs);

	/// @brief Free resources allocated for Global map GLOBALJOBS.
	//void free_GLOBALJOBS();
}
#endif
