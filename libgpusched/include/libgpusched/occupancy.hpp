/// @file occupancy.hpp
#ifndef __OCCUPANCY__HPP
#define __OCCUPANCY__HPP
#include <libgpusched/gpu_conf.hpp>
#include <libgpusched/gputask.hpp>
#include <libgpusched/job.hpp>
#include <libgpusched/logger.hpp>
#include <libgpusched/occupancy.hpp>
#include <libgpusched/utils.hpp>

// std include
#include <array>
#include <iomanip>
#include <map>
#include <string>
#include <sstream>
#include <vector>

namespace gpusched
{

	enum class resourceType : int { THREAD, SMEM, REGS};

	/**
	 * @brief
	 * Structure that works as a cache to store information
	 * about sm on which the scheduler failed to allocate a block
	 * for a job.
	 */
	struct job_allocation_cache
	{
		int job_id;
		std::vector<int> failed_allocation_sm;
		int next_sm; /// @brief next sm on which scheduler should try to allocate.
	};

	/*
	 *  @brief
	 *  Occupancy class
	 *  This class represents the state of the GPU occupancy dynamically.
	 */
	class Occupancy final
	{

		/*
		 *  @brief
		 *  allocated_blocks contains a mapping between a job id
		 *  and resources allocated for the job.
		 *
		 *  An entry are represented as follows:
		 *  key: job_id
		 *  value: vector of pair p such that p =  (number of block, sm)
		 *  Each pair represent the number of block allocated by job
		 *  with job_id on the sm.
		 */
		std::map<int, std::vector<std::pair<int, int> > >
			allocated_blocks;

		double gpu_occupancy;

		// changed to int to avoid int overflow.
		std::array<int, NSM> reg_occupancy;

		std::array<int, NSM> smem_occupancy;

		std::array<int, NSM> thread_occupancy;

		int current_sm;

		// can only be call from allocateResourceKernel
		bool allocate_resource_one_block(int job_id,
				int task_id,
				int block,
				int threadsPerBlock,
			       	int smem,
				int sm);

		public:
		Occupancy();

		~Occupancy(){};

		friend bool operator==(const Occupancy& lhs, const Occupancy& rhs);

		/**
		 * @brief
		 * Allocate resources for a given batch of job associated to GPU task.
		 *
		 * @param jobs_id 	Batch of jobs identifier.
		 *
		 * @return bool 	True if allocation succeed for all jobs in the batch.
		 * 			False otherwise.
		 */
		bool allocate_resource_batch_kernel(const std::vector<int>& jobs_id);

                /**
		 * @brief
		 * Allocate resources for the kernel associated to a GpuTask.
		 *
		 * @param job_id 	The job identifier from which allocation occurs.
		 *
		 * @return bool		true if the allocation succeed, false otherwise.
		 */
		bool allocate_resource_kernel(int job_id);

		/*
		 * @brief
		 * Deallocate resources for the kernel associated to a GpuTask.
		 *
		 * @param job_id 	The job identifier from which deallocation occurs.
		 */
		void free_resources(const int job_id);

		/*
		 * @brief
		 * When resources are allocated, add an entry in allocated_block or increment
		 * if already exist.
		 *
		 * @param job_id	The job identifier.
		 * @param sm		SM on which resources have been allocated.
		 */
		void add_block_allocated_to_sm(int job_id, int sm);

		/*
		 * @brief
		 * Check thread resource before allocation.
		 *
		 * @param thread_per_block	number of thread per block of a given kernel.
		 * @param sm			SM on which we want to allocate thread resource.
		 *
		 * @return bool			true if we can allocate, false otherwise.
		 */
		inline __attribute__((always_inline)) bool can_offload_threads(
				int thread_per_block,
			       	int sm)
		{
			if(thread_per_block > MAX_THREAD_PER_BLOCK){
#ifdef DEBUG_MODE
				DEBUGFATAL("Thread per block GPU limit");
#endif
				return false;
			}

			return (thread_per_block <= thread_occupancy[sm]);
		}

		/*
		 * @brief
		 * Check shared memory resource before allocation.
		 *
		 * @param smem			shared memory per block of a given kernel.
		 * @param sm			SM on which we want to allocate shared memory
		 * 				resource.
		 *
		 * @return bool			true if we can allocate, false otherwise.
		 */
		inline __attribute__((always_inline)) bool can_allocate_smem(
				int smem,
				int sm)
		{
			return smem < smem_occupancy[sm];
		}

		/*
		 * @brief
		 * Check register resource before allocation.
		 *
		 * @param regs			number of register per thread of a given kernel.
		 * @param sm			SM on which we want to allocate register resource.
		 *
		 * @return bool			true if we can allocate, false otherwise.
		 */
		inline __attribute__((always_inline)) bool can_allocate_regs(
				int reg,
				int sm)
		{
			return (reg < reg_occupancy[sm] && (reg_occupancy[sm] - reg) >= 0);
		}

		/*
		 * @brief
		 * Check allocation for thread, shared memory and register resources.
		 *
		 * @param regs			number of register per thread of a given kernel.
		 * @param thred_per_block 	number of thread per block of a given kernel.
		 * @param smem			shared memory per block of a given kernel.
		 * @param sm			SM on which we want to allocate thread resource.
		 *
		 * @return bool			true if we can allocate, false otherwise.
		 */
		inline __attribute__((always_inline)) bool can_allocate_resources(int regs,
			        int thread_per_block,
				int smem,
				int sm)
		{
			return (can_offload_threads(thread_per_block, sm) && can_allocate_smem(smem, sm)
					&& can_allocate_regs(regs, sm));
		}

		const std::string to_string() const;

		/*
		 * @brief
		 * Getter for reg_occupancy.
		 *
		 * @return std::array<int, NSM> 	the reg_occupancy array.
		 */
		std::array<int, NSM> get_reg_occupancy() const;

		/*
		 * @brief
		 * Getter for thread_occupancy.
		 *
		 * @return std::array<int, NSM> 	the thread_occupancy array.
		 */
		std::array<int, NSM> get_thread_occupancy() const;

		/*
		 * @brief
		 * Getter for smem_occupancy.
		 *
		 * @return std::array<int, NSM> 	the smem_occupancy array.
		 */
		std::array<int, NSM> get_smem_occupancy() const;

		std::vector<std::pair<int, int> >
			get_entry_blocks_allocated(int job_id);

		int get_current_sm() const;

		__attribute__((always_inline)) inline int compute_next_sm(int sm)
			const
		{
			sm = sm + 2;
			if (sm == NSM)
				sm+=1;
			else if (sm == (NSM+1))
				sm-=1;
			sm %= (NSM);

			return sm;
		}
		__attribute__((always_inline)) inline void compute_next_sm()
		{
			current_sm += 2;
			if (current_sm == NSM)
				current_sm++;
			else if (current_sm == (NSM+1))
				current_sm--;
			current_sm %= (NSM);
		}


		void reset();
        };// class Occupancy
}// namespace gpusched
#endif
