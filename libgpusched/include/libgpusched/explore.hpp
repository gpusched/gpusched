#ifndef __EXPLORE__HPP
#define __EXPLORE__HPP

#include <libgpusched/gputask.hpp>
#include <libgpusched/graph_gen.hpp>
#include <libgpusched/logger.hpp>
#include <libgpusched/task.hpp>
#include <libgpusched/vertex.hpp>
#include <libgpusched/interval.hpp>

#include <deque>
#include <iostream>
#include <vector>
#include <functional>
namespace gpusched
{

	template <typename T>
	bool explore(bool all_nodes, bool details)
	{

	  graph_viz::GraphGen* _graph = graph_viz::GraphGen::getInstance();
          std::vector<T> schedulable_vertex; // vertex which have the flag
                                                  // schedulable set to true.
          std::vector<T> dominance_vertex; // vertex that are dominant at a given time t.
          std::deque<T> list_vertex;

	  std::vector<size_t> invalid_permutation; // if we start iteration with fresh state of gpu
						   // we can compute it statically before the
						   // execution of gpusched.
						   // Otherwise, we can track invalid permutation
						   // by saving the state for which the permutation
						   // is invalid in the hash stored.

	  std::vector<size_t> valid_permutations;
          T base;

	  list_vertex.push_back(base);
          while (not list_vertex.empty()) {
		  T curr_vertex = list_vertex.front();
                  list_vertex.pop_front();
		  if (!curr_vertex.check_rdy_jobs()){
			continue;
		  }
		  if (curr_vertex.get_dominated_to_remove())
			  continue;
          	  //std::cout << curr_vertex.to_string() << std::endl;

		  std::vector<int> rdy_jobs = curr_vertex.fetch_ready_jobs(
				  	      utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
//		  utils::display_combinaison(rdy_jobs);
		  // debug_v contains all tasks id from jobs in ready_jobs
		  // if we have two times the same task id we stop exploration from this
		  // vertex
		  std::vector<int> debug_v;
		  for (auto q: rdy_jobs)
		  {
			  debug_v.push_back(GLOBALJOBS[q]->get_associated_task_id());
		  }
		  std::set<int> debug_set(debug_v.begin(), debug_v.end());
		  // if two jobs of the same task are ready to be executed at the same time
		  // we already know that one of them missed deadline
                  if (debug_set.size() != debug_v.size()) {
                    T children(
                        curr_vertex, {},
                        utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
                    _graph->add_edge(children, error::ERDYJOBSDEADLINE);
                    continue;
                  }
                  //  have to forward all stream to the next minimal arrival time jobs.

		  auto deltas = curr_vertex.get_delta_dominance();
		  //std::cout << "before delta delete" << deltas.size() << std::endl;
		  std::vector<int> delta_to_delete;
		  for(std::set<int>::iterator it = deltas.end(); it!= deltas.begin(); it--){
		        bool create_children = false;
		  	//construct_delta_vertex(curr_vertex);
			int saved_delta = *it;
		        int min_arrival_time_job_id = curr_vertex.min_arrival_time_job();
			auto copy_rdy_jobs = curr_vertex.get_copy_rdy_jobs();
		        if (min_arrival_time_job_id == GLOBALJOBS.size() + 1)
		        	continue;
		        // create an interval of current_time and current + delta

		        Interval interval(curr_vertex.get_time(), curr_vertex.get_time() + *it);
		        // we must exclude the smallest border
		        int count_jobs_added = 0;
		        while(interval.in_right_exclusive(GLOBALJOBS[min_arrival_time_job_id]->get_arrival_time())) {
		        	count_jobs_added++;
				copy_rdy_jobs.push_back(min_arrival_time_job_id);
		        	min_arrival_time_job_id = utils::get_next_different_arrival_job(min_arrival_time_job_id);
		        	if (min_arrival_time_job_id == -1)
		        		break;
		        	create_children = true;
				delta_to_delete.push_back(*it);
		  	}
			if (count_jobs_added == 0)
				break;
		        if (create_children){
		        	// since it forwards when created we don't have to
		        	// set time nor rdy_jobs
		  		if (gpusched::utils::check_created_rdy_jobs(copy_rdy_jobs, curr_vertex.get_time() + saved_delta)){
		        		T split_children(curr_vertex, curr_vertex.get_time() + saved_delta,
		        			utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
		        		list_vertex.push_back(split_children);

		  		}
		        }
		  }

		  curr_vertex.update_delta_dominance(delta_to_delete);
	  	  delta_to_delete.clear();
		  if ((curr_vertex.get_remaining_jobs().count() != 0) && rdy_jobs.size() == 0 && (!curr_vertex.is_schedulable())) {
			  rdy_jobs = curr_vertex.fast_forward();
#ifdef DEBUG_MODE
			  // if rdy_jobs too big or twice the
			  // task set it's not ok
			  // if rdy_jobs contains twice a job of the same task break;
			  for (auto job_ : rdy_jobs)
				  std::cout << job_ << " ";
			  std::cout << std::endl;
#endif
		  }

		  std::set<std::vector<int> > permutations;
/*==================================== Generate Combinaition ====================================*/
		  std::set<std::vector<int> > allocated_permutations;
 		  std::set<std::vector<int> > tmp_allocated;
			utils::get_all_combinaisons(permutations, rdy_jobs);

		  	// remove all non viable permutations by checking here if they can't be
		  	// allocated with the curent state of the current vertex.

		  	for (std::set<std::vector<int> >::iterator perm = permutations.begin();
					  perm != permutations.end();
					  ++perm) {

				//  we verify if we didn't already check similar permutation
				std::size_t curr_perm_hash = utils::PermutationHash{}(*perm);
				if (std::find(invalid_permutation.begin(),
					      invalid_permutation.end(),
					      curr_perm_hash) != invalid_permutation.end())
				{
                                  	continue;
				}

				curr_perm_hash = utils::ValidPermutationHash{}(*perm);
				T children(curr_vertex, *perm,
						utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
				// else we check if the permutation fit the GPU state
				error::error_t node_type;
				// need to check a new permutation that know
				// where index are from rather than invalid permutations
				bool already_checked_perm = std::find(valid_permutations.begin(),
                                                                      valid_permutations.end(),
                                                                      curr_perm_hash) != valid_permutations.end();

				// We have to free streams if already_checked_perm is ok
				if (already_checked_perm)
					children.set_free_used_streams();
				if (not children.check_occupancy_after_free_streams(*perm)) {

					node_type = error::EALLOCB;
					_graph->add_edge(children, error::EALLOCB);
					invalid_permutation.push_back(curr_perm_hash);//utils::PermutationHash{}(*perm));
					utils::PermutationHash::all_invalid_permutations.push_back(std::make_pair(curr_perm_hash,*perm));//utils::PermutationHash{}(*perm));

#ifdef DEBUG_MODE
					DEBUGWARN("Start- Not enough space for the given permutation");
					DEBUGWARN("End- Not enough space for the given permutation");
#endif
				} else {
					valid_permutations.push_back(curr_perm_hash);
					utils::ValidPermutationHash::all_valid_permutations.push_back(std::make_pair(curr_perm_hash,*perm));//utils::PermutationHash{}(*perm));
#ifdef DEBUG_MODE
					std::cout << "Permutation IS ALLOCATED size: " <<
						(*perm).size() << std::endl;
					for (std::vector<int>::const_iterator it = (*perm).begin();
						it != (*perm).end();
						++it)
						std::cout << *it << " ";
#endif

					children.handle_scheduled_jobs();

					if (children.check_scheduled_jobs_deadlines()){
						// generate all nodes without domination
						if (all_nodes)
						{
							if (children.is_schedulable()) {
								node_type = error::ESCHEDULABLE;
								schedulable_vertex.push_back(children);
							}
							else{
								node_type = error::EDOMINE;
								list_vertex.push_back(children);
							}
						}else // generates node but with domination rules
						{
							if (children.apply_dominance(dominance_vertex, list_vertex)){
								if (children.is_schedulable()) {
									node_type = error::ESCHEDULABLE;
									schedulable_vertex.push_back(children);
									break;
								} else {
									node_type = error::EDOMINE;
									list_vertex.push_back(children);
								}
                                                        }
							else {
								node_type = error::EDOMINATED;
							}
						}
					} else {
#ifdef DEBUG_MODE
						std::cout << "JOBS DON'T RESPECT DEADLINE" << std::endl;
						std::cout << "DISPLAY JOB AND DEADLINE" << std::endl;

#endif
						node_type = error::EDEADLINE;
					}

#ifdef DEBUG_MODE
					std::cout << "AFTER COMPUTATION NEW VERTEX CHILDREN" << std::endl;
					std::cout << children.to_string() << std::endl;
#endif
					//restore the curr_vertex to its initial state.
					//children.free_permutation_resources_check(*perm);
					tmp_allocated.insert(*perm);
				}
			if (details)
				_graph->add_edge_details(children, node_type);
			else
				_graph->add_edge(children, node_type);

			}

			for (auto q: tmp_allocated)
			{
				allocated_permutations.insert(q);
			}
			tmp_allocated.clear();
			permutations.clear();
		  //}
#ifdef DEBUG_MODE
			std::cout << "permutations size:" <<  permutations.size();
			std::cout << " allocated_permutations size: " << allocated_permutations.size() << std::endl;
			for (auto q: allocated_permutations)
				utils::display_combinaison(q);
#endif
		}
#ifdef DEBUG_MODE
		if (schedulable_vertex.size() != 0){
			DEBUGINFO("FIND PATH");

		}
#endif
		for (auto v: schedulable_vertex){
			look_up(v);
		}

		if (schedulable_vertex.size() == 0)
		{
			return false;
                }
          return true;
	}


	template<typename T>
	void construct_delta_vertex(const T& curr_vertex)
	{
		bool delta_match;
		delta_match = true;
		int min_arrival_time_job = curr_vertex.min_arrival_time_job();
		if (min_arrival_time_job == std::numeric_limits<int>::max())
			return;
		auto delta_dominance = curr_vertex.get_delta_dominance();
		// use an interval of index to remove all delay that have the same ready jobs
		// and forward to the jobs arrival time that they all share.
		while ( delta_match ){
			int min_delta = GLOBALJOBS[min_arrival_time_job]->get_arrival_time() - curr_vertex.get_time();

			auto it = std::upper_bound(delta_dominance.begin(),
					delta_dominance.end(), min_delta,
					[](int _delta, int curr_delta) {
						return _delta <= curr_delta;
					});
			delta_match = it != delta_dominance.end() ? true: false;

			if (delta_match) {
				min_arrival_time_job = utils::get_next_different_arrival_job(min_arrival_time_job);
			}
		}
	}



	template <typename T>
	bool parallel_explore(bool all_nodes)
	{

	  graph_viz::GraphGen* _graph = graph_viz::GraphGen::getInstance();
          std::vector<T> schedulable_vertex; // vertex which have the flag
                                                  // schedulable set to true.
          std::vector<T> dominance_vertex; // vertex that are dominant at a given time t.
          std::deque<T> list_vertex;

	  std::vector<size_t> invalid_permutation; // if we start iteration with fresh state of gpu
						   // we can compute it statically before the execution
						   // of gpusched.
						   // Otherwise, we can track invalid permutation
						   // by saving the state for which the permutation
						   // is invalid in the hash stored.

          T base;

	  list_vertex.push_back(base);
          while (not list_vertex.empty()) {
		  T curr_vertex = list_vertex.front();
		  list_vertex.pop_front();
          	  //std:: << curr_vertex.to_string() << std::endl;

		  std::vector<int> rdy_jobs = curr_vertex.fetch_ready_jobs(
				  	      utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);

		  // debug_v contains all tasks id from jobs in ready_jobs
		  // if we have two times the same task id we stop exploration from this
		  // vertex
		  std::vector<int> debug_v;
		  for (auto q: rdy_jobs)
		  {
			  debug_v.push_back(GLOBALJOBS[q]->get_associated_task_id());
		  }
		  std::set<int> debug_set(debug_v.begin(), debug_v.end());
		  // if two jobs of the same task are ready to be executed at the same time
		  // we already know that one of them missed deadline
		  if (debug_set.size() != debug_v.size()){
			  T children(curr_vertex, {}, utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
			  _graph->add_edge(children, error::ERDYJOBSDEADLINE);
			  continue;
		  }
		  //  have to forward all stream to the next minimal arrival time jobs.
		  if (rdy_jobs.size() == 0 && (!curr_vertex.is_schedulable())) {
			  rdy_jobs = curr_vertex.fast_forward();
#ifdef DEBUG_MODE
			  // if rdy_jobs too big or twice the
			  // task set it's not ok
			  // if rdy_jobs contains twice a job of the same task break;
			  for (auto job_ : rdy_jobs)
				  std::cout << job_ << " ";
			  std::cout << std::endl;
#endif
		  }
//	       	  std::cout << "RDY_JOBS.size() = " << rdy_jobs.size() << std::endl;


		  std::set<std::vector<int> > permutations;
/*==================================== Generate Combinaition ====================================*/
		  std::set<std::vector<int> > allocated_permutations;
 		  std::set<std::vector<int> > tmp_allocated;
		  // this loop is removed because we generate all combinaisons
		  //for (long unsigned int i = 0; i != rdy_jobs.size(); i++)
		  //{
			// start by higher k combinaisons to lowest
			// as we use work-conserving this allow us to take the permutation
			// that use the most the GPU
			// example:
			// 	1 2 3 is a valid combinaison
			// 	1 2   combinaison will be discard
                  	//utils::get_k_combinaisons(rdy_jobs.size()-i, permutations, rdy_jobs);
			utils::get_all_combinaisons(permutations, rdy_jobs);

		  	// remove all non viable permutations by checking here if they can't be
		  	// allocated with the curent state of the current vertex.

			//@TODO problem here when there is 0 permutations available due to occupancy
			// we have multiple options
			//
			// 1 - There is no job in rdy_jobs
			// 	- forward until next job arrival (free resources allocated on stream
			// 	finishing before next job arrival time)
			//
			// 2 - There is some jobs in rdy_jobs BUT there is not enough resources in
			//     Occupancy to allocate.
			//     In this case we have to forward stream one by one until there is
			//     enough resources in occupancy for at least one combinaison
			//
			// How to know if the combinaisons already been checked for the current vertex?
			// We can use flag seeing if we already tried to allocate in the current state


			const unsigned long length = permutations.size();

			const unsigned long min_per_thread = 1;

			const unsigned long max_threads = (length + min_per_thread-1)/min_per_thread;

			const unsigned long num_threads = std::min(utils::hw_threads!=0?utils::hw_threads:2, max_threads);

			const unsigned long block_size = length/num_threads;


			//std::cout << "rdy_jobs: " << rdy_jobs.size() << std::endl;
			//std::cout << "permutation size: " << length << std::endl;
			//std::cout << "block_size: " << block_size << std::endl;
			std::vector<std::size_t> results_hash(permutations.size());
			//std::cout << "results_hash: " << results_hash.size() << std::endl;
			//std::cout << "num_threads: " << num_threads << std::endl;

			std::vector<std::thread> threads(num_threads-1); // we have the main thread too

			auto block_start = permutations.begin();
			auto block_results_start = results_hash.begin();

			for (unsigned int i = 0; i < (num_threads-1); ++i)
			{
					auto block_end = block_start;
					auto block_results_end = block_results_start;
					std::advance(block_end, block_size);
					std::advance(block_results_end, block_size);
					threads[i] = std::thread(
							utils::PermutationHashParallel(),
							block_start,
							block_end,
							block_results_start,
							std::ref(results_hash),
							block_size);
					block_start = block_end;
					block_results_start = block_results_end;
			}

			std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));

			auto block_results_end = block_results_start;
			auto block_perm_end = block_start;

		       	std::advance(block_perm_end, std::distance(block_perm_end, permutations.end()));
		       	std::advance(block_results_end , std::distance(block_results_end, results_hash.end()));

			utils::PermutationHashParallel{}(block_start, block_perm_end,  block_results_start,  results_hash, block_size);

			//for (auto q= results_hash.begin(); q!=results_hash.end(); ++q){
			//	int index = std::distance(results_hash.begin(), q);
			//	std::cout << *q << " " << index << " ";
			//	auto it = permutations.begin();
			//	std::advance(it, index);
			//	utils::display_combinaison(*it);
			//}
			//std::cout << std::endl;
		  	for (std::set<std::vector<int> >::iterator perm = permutations.begin();
					  perm != permutations.end();
					  ++perm) {


//======= REMOVE COMMENT IF WE ONLY WANT TO EXPLORE FROM BIGGEST WORKLOAD AVAILABLE =======
				// As we consider work-conserving we first check if the
				// current combinaison is not a subset of a previous one
				// with more jobs running
				//bool is_subset_of_previous_permutation = false;
				//for (auto v_perm : allocated_permutations) {
				//	if (utils::is_subset(v_perm, *perm))
				//	{
				//		//std::cout << "BREAK" << std::endl;
				//		//utils::display_combinaison(*perm);
				//		is_subset_of_previous_permutation = true;
				//	}
				//}
				//// if it is a subset of a previous combinaison
				//// we stop break the loop for this permutation
				//if (is_subset_of_previous_permutation) {
				//	break;
				//}
//======= REMOVE COMMENT IF WE ONLY WANT TO EXPLORE FROM BIGGEST WORKLOAD AVAILABLE =======

				//  we verify if we didn't already check similar permutation
				//  @TODO use thread to compute on multiple permutations
				std::size_t curr_perm_hash = results_hash[std::distance(permutations.begin(), perm)];
				//std::size_t test = utils::PermutationHash{}(*perm);
				//std::cout << "curr_perm_hash: " << curr_perm_hash << " test: " << test;
				//std::cout << std::endl;
				if (std::find(invalid_permutation.begin(),
					      invalid_permutation.end(),
					      results_hash[std::distance(permutations.begin(), perm)]) != invalid_permutation.end())
				{
                                  	continue;
				}

				T children(curr_vertex, *perm,
						utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
				// else we check if the permutation fit the GPU state
				error::error_t node_type;

				if (not children.check_occupancy_after_free_streams(*perm)) {

					// for task_set_3_1 job_set_3_3
				//	if ((*perm).size() == 3)
				//		if ((*perm)[0] == 2 && (*perm)[1] == 5 && (*perm)[2] == 6)
				//			std::cout << children.to_string() << std::endl;
					//children.free_permutation_resources_check(*perm);
					node_type = error::EALLOCB;
					//_graph->add_edge(children, error::EALLOCB);
					// we can use only one invalid_permutation vector of pair<size_t, vector>
					// and remove the one static in permutationHash
					invalid_permutation.push_back(curr_perm_hash);//utils::PermutationHash{}(*perm));
					utils::PermutationHash::all_invalid_permutations.push_back(std::make_pair(curr_perm_hash,*perm));//utils::PermutationHash{}(*perm));

#ifdef DEBUG_MODE
					DEBUGWARN("Start- Not enough space for the given permutation");
					DEBUGWARN("End- Not enough space for the given permutation");
#endif
				} else {

					// for task_set_3_1 job_set_3_3
				//	if ((*perm).size() == 3)
				//		if ((*perm)[0] == 5 && (*perm)[1] == 2 && (*perm)[2] == 6)
				//			std::cout << children.to_string() << std::endl;
#ifdef DEBUG_MODE
					std::cout << "Permutation IS ALLOCATED size: " <<
						(*perm).size() << std::endl;
					for (std::vector<int>::const_iterator it = (*perm).begin();
						it != (*perm).end();
						++it)
						std::cout << *it << " ";
#endif

					children.handle_scheduled_jobs();

					if (children.check_scheduled_jobs_deadlines()){
						// generate all nodes without domination
						if (all_nodes)
						{
							if (children.is_schedulable()) {
								//_graph->add_edge(children, error::ESCHEDULABLE);
								node_type = error::ESCHEDULABLE;
								schedulable_vertex.push_back(children);
							}
							else{
								//_graph->add_edge(children, error::EDOMINE);
								node_type = error::EDOMINE;
								list_vertex.push_back(children);
							}
						}else // generates node but with domination rules
						{
							if (children.apply_dominance(dominance_vertex, list_vertex)){
								if (children.is_schedulable()) {
									//_graph->add_edge(children, error::ESCHEDULABLE);
									node_type = error::ESCHEDULABLE;
									schedulable_vertex.push_back(children);
								} else {
									//_graph->add_edge(children, error::EDOMINE);
									node_type = error::EDOMINE;
									list_vertex.push_back(children);
								}
                                                        }
							else {
								node_type = error::EDOMINATED;
								//_graph->add_edge(children, error::EDOMINATED);
							}
						}
						//_graph->add_edge(children, error::EDOMINE);
					} else {
#ifdef DEBUG_MODE
						std::cout << "JOBS DON'T RESPECT DEADLINE" << std::endl;
#endif
						node_type = error::EDEADLINE;
						//_graph->add_edge(children, error::EDEADLINE);
					}

#ifdef DEBUG_MODE
					std::cout << "AFTER COMPUTATION NEW VERTEX CHILDREN" << std::endl;
					std::cout << children.to_string() << std::endl;
#endif
					//restore the curr_vertex to its initial state.
					//children.free_permutation_resources_check(*perm);
					tmp_allocated.insert(*perm);
				}
			_graph->add_edge(children, node_type);
			}

			for (auto q: tmp_allocated)
			{
				allocated_permutations.insert(q);
			}
			tmp_allocated.clear();
			permutations.clear();
		  //}
#ifdef DEBUG_MODE
			std::cout << "permutations size:" <<  permutations.size();
			std::cout << " allocated_permutations size: " << allocated_permutations.size() << std::endl;
			for (auto q: allocated_permutations)
				utils::display_combinaison(q);
#endif
		}

#ifdef DEBUG_MODE
		if (schedulable_vertex.size() != 0){
			DEBUGINFO("FIND PATH");

		}
		for (auto v: schedulable_vertex){
			look_up(v);
		}
#endif

		if (schedulable_vertex.size() != 0)
		{
			// write in file path and schedulable to true for
			// graph
			int i;
                }
          return true;
	}

	template <typename T>
	bool exploreEDFMAX(bool all_nodes, bool details)
	{

	  graph_viz::GraphGen* _graph = graph_viz::GraphGen::getInstance();
          std::vector<T> schedulable_vertex; // vertex which have the flag
                                                  // schedulable set to true.

	  std::vector<size_t> invalid_permutation; // if we start iteration with fresh state of gpu
						   // we can compute it statically before the
						   // execution of gpusched.
						   // Otherwise, we can track invalid permutation
						   // by saving the state for which the permutation
						   // is invalid in the hash stored.
          std::deque<T> list_vertex;

          T base;

	  list_vertex.push_back(base);
          while (not list_vertex.empty()) {
	//	  std::cout <<"List vertex size: " <<  list_vertex.size() << std::endl;
		  T curr_vertex = list_vertex.front();
                  list_vertex.pop_front();
		  if (!curr_vertex.check_rdy_jobs()){
			continue;
		  }

		  //if(curr_vertex.get_dominated_to_remove()){
		  //        //std::cout << "salut" << std::endl;
		  //        continue;
		  //}
		  std::vector<int> rdy_jobs = curr_vertex.fetch_ready_jobs(
				  	      utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);

		  // debug_v contains all tasks id from jobs in ready_jobs
		  // if we have two times the same task id we stop exploration from this
		  // vertex
		  std::vector<int> debug_v;
		  for (auto q: rdy_jobs)
		  {
			  debug_v.push_back(GLOBALJOBS[q]->get_associated_task_id());
		  }
		  std::set<int> debug_set(debug_v.begin(), debug_v.end());
		  // if two jobs of the same task are ready to be executed at the same time
		  // we already know that one of them missed deadline
                  if (debug_set.size() != debug_v.size()) {
                    T children(
                        curr_vertex, {},
                        utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
                    _graph->add_edge(children, error::ERDYJOBSDEADLINE);
                    continue;
                  }
                  //  have to forward all stream to the next minimal arrival time jobs.
		  if (rdy_jobs.size() == 0 && (!curr_vertex.is_schedulable())) {
			  rdy_jobs = curr_vertex.fast_forward();
#ifdef DEBUG_MODE
			  // if rdy_jobs too big or twice the
			  // task set it's not ok
			  // if rdy_jobs contains twice a job of the same task break;
			  for (auto job_ : rdy_jobs)
				  std::cout << job_ << " ";
			  std::cout << std::endl;
#endif
		  }


		  std::set<std::vector<int> > permutations;
/*==================================== Generate Combinaition ====================================*/
		  std::set<std::vector<int> > allocated_permutations;
 		  std::set<std::vector<int> > tmp_allocated;



		  std::vector<int> EDFPermutation;
		  // change here to use EDF for not yet arrived jobs
		  auto rdy_cpy = curr_vertex.get_copy_rdy_jobs();
		  // auto next_job_edf = curr_vertex.get_next_job_edf2(); // same thing that get_next_job_edf
		  // but don't update the time of the vertex
		  // if( std::find(rdy_cpy.begin(), rdy_cpy.end(), next_job_edf) == rdy_cpy.end())
		  // 		curr_vertex.get_next_job_edf(); // update time vertex
		  auto next_job_edf = curr_vertex.get_next_job_edf(rdy_cpy);
		  EDFPermutation.push_back(next_job_edf);

		  std::vector<int> EDFPermutationTry;
		  EDFPermutationTry.push_back(next_job_edf);

		  rdy_cpy.erase(std::find(rdy_cpy.begin(), rdy_cpy.end(), next_job_edf));

		  while (rdy_cpy.size() != 0)
		  {
		  	next_job_edf = curr_vertex.get_next_job_edf(rdy_cpy);
			EDFPermutationTry.push_back(next_job_edf);
			rdy_cpy.erase(std::find(rdy_cpy.begin(), rdy_cpy.end(), next_job_edf));
			if(curr_vertex.check_occupancy_after_free_streams(EDFPermutationTry)){
				EDFPermutation.push_back(next_job_edf);
			}else
				break;
		  }
		  rdy_cpy.clear();

	   	  T children(curr_vertex, EDFPermutation,
				utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
		error::error_t node_type;
//		children.set_free_used_streams(); // fix stream problem in edf max
		if(children.check_occupancy_after_free_streams(EDFPermutation)){
			children.handle_scheduled_jobs();
			if (children.check_scheduled_jobs_deadlines()){
					  // generate all nodes without domination
				 if (children.is_schedulable()) {
					node_type = error::ESCHEDULABLE;
					schedulable_vertex.push_back(children);
					_graph->add_edge(children, node_type);
				 }
				 else{
					node_type = error::EDOMINE;
					list_vertex.push_back(children);
					_graph->add_edge(children, node_type);
			 	 }
			}else{
				std::cout << "NOT SCHEDULABLE" << std::endl;
				node_type = error::EDEADLINE;
				_graph->add_edge(children, node_type);
			}
		} else{
			node_type == error::EALLOCB;
			_graph->add_edge(children, node_type);
		}
	}

#ifdef DEBUG_MODE
	if (schedulable_vertex.size() != 0){
		DEBUGINFO("FIND PATH");

	}
	for (auto v: schedulable_vertex){
		look_up(v);
	}
#endif
	if (schedulable_vertex.size() == 0)
	{
		return false;
        }
        return true;
	}

	template <typename T>
	bool exploreEDFIsolation(bool all_nodes, bool details)
	{

	  graph_viz::GraphGen* _graph = graph_viz::GraphGen::getInstance();
          std::vector<T> schedulable_vertex; // vertex which have the flag
                                                  // schedulable set to true.

	  std::vector<size_t> invalid_permutation; // if we start iteration with fresh state of gpu
						   // we can compute it statically before the
						   // execution of gpusched.
						   // Otherwise, we can track invalid permutation
						   // by saving the state for which the permutation
						   // is invalid in the hash stored.
          std::deque<T> list_vertex;

          T base;

	  list_vertex.push_back(base);
          while (not list_vertex.empty()) {
	//	  std::cout <<"List vertex size: " <<  list_vertex.size() << std::endl;
		  T curr_vertex = list_vertex.front();
                  list_vertex.pop_front();
		  if (!curr_vertex.check_rdy_jobs()){
			continue;
		  }
		  std::vector<int> rdy_jobs = curr_vertex.fetch_ready_jobs(
				  	      utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);

//		  std::cout << curr_vertex.to_string() << std::endl;
		  // debug_v contains all tasks id from jobs in ready_jobs
		  // if we have two times the same task id we stop exploration from this
		  // vertex
		  std::vector<int> debug_v;
		  for (auto q: rdy_jobs)
		  {
			  debug_v.push_back(GLOBALJOBS[q]->get_associated_task_id());
		  }
		  std::set<int> debug_set(debug_v.begin(), debug_v.end());
		  // if two jobs of the same task are ready to be executed at the same time
		  // we already know that one of them missed deadline
                  if (debug_set.size() != debug_v.size()) {
                    T children(
                        curr_vertex, {},
                        utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
                    //_graph->add_edge(children, error::ERDYJOBSDEADLINE);
                    continue;
                  }
                  //  have to forward all stream to the next minimal arrival time jobs.
		  if (rdy_jobs.size() == 0 && (!curr_vertex.is_schedulable())) {
			  rdy_jobs = curr_vertex.fast_forward();
#ifdef DEBUG_MODE
			  // if rdy_jobs too big or twice the
			  // task set it's not ok
			  // if rdy_jobs contains twice a job of the same task break;
			  for (auto job_ : rdy_jobs)
				  std::cout << job_ << " ";
			  std::cout << std::endl;
#endif
		  }


		  std::set<std::vector<int> > permutations;
/*==================================== Generate Combinaition ====================================*/
		  std::set<std::vector<int> > allocated_permutations;
 		  std::set<std::vector<int> > tmp_allocated;


		  //utils::display_combinaison(curr_vertex.get_rdy_jobs());

		  std::vector<int> EDFPermutation;
		  auto next_job_edf = curr_vertex.get_next_job_edf();
		  auto rdy_cpy = curr_vertex.get_copy_rdy_jobs();
		  //utils::display_combinaison(rdy_cpy);
		//std::cout << next_job_edf << std::endl;
		  EDFPermutation.push_back(next_job_edf);

		  rdy_cpy.erase(std::find(rdy_cpy.begin(), rdy_cpy.end(), next_job_edf));

	   	  T children(curr_vertex, EDFPermutation,
				utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
		error::error_t node_type;
		//children.set_free_used_streams(); // fix stream problem in edf max
		//std::cout << children.to_string() << std::endl;

		if(children.check_occupancy_after_free_streams(EDFPermutation)){
			children.handle_scheduled_jobs();
		} else{
			node_type == error::EALLOCB;
			_graph->add_edge(children, node_type);
			break;
		}

		//if (children.check_occupancy_after_free_streams(EDFPermutation)){
		if (children.check_scheduled_jobs_deadlines()){
				  // generate all nodes without domination
			 if (children.is_schedulable()) {
				node_type = error::ESCHEDULABLE;
				schedulable_vertex.push_back(children);
				_graph->add_edge(children, node_type);
			 }
			 else{
				node_type = error::EDOMINE;
				list_vertex.push_back(children);
				_graph->add_edge(children, node_type);
		 	 }
		}else{
			//std::cout << "NOT SCHEDULABLE" << std::endl;
			node_type = error::EDEADLINE;
			_graph->add_edge(children, node_type);
		}
		//}
	}

#ifdef DEBUG_MODE
	if (schedulable_vertex.size() != 0){
		DEBUGINFO("FIND PATH");

	}
	for (auto v: schedulable_vertex){
		look_up(v);
	}
#endif
	if (schedulable_vertex.size() == 0)
	{
		return false;
        }
        return true;
	}
}

#endif
