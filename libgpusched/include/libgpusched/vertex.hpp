/// @file vertex.hpp
#ifndef __VERTEX__HPP
#define __VERTEX__HPP

#include <libgpusched/error_t.hpp>
#include <libgpusched/gpu_conf.hpp>
#include <libgpusched/gputask.hpp>
#include <libgpusched/interval.hpp>
//#include <libgpusched/jobs_conf.hpp>
#include <libgpusched/occupancy.hpp>
#include <libgpusched/task.hpp>
#include <libgpusched/utils.hpp>

#include <../../../boost/dynamic_bitset.hpp>
#include <atomic>
#include <bitset>
#include <cmath>
#include <deque>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

namespace gpusched
{

	/**
	 * @brief
	 * Abstraction of cuda stream.
	 *
	 * A stream contains the job id of the job dispatched on the stream,
	 * an array of interval with an entry for each sm present in GPU.
	 */
	struct stream
	{

		int stream_id; /// @brief The current stream unique identifier.
		int scheduled_job_id; /// @brief Job id scheduled on the current stream.

		bool _free; /// @ brief flag to know if the stream is actually free.

		/**
		 * @brief sm_interval contains an interval for each SM.
		 * Each interval of time denote the duration on which a SM is occupied
		 * by a Job associated to a GPU task.
		 */
		std::array<Interval, NSM> sm_interval; // maybe it's better to use a map so
							 // we store only sm occupied under this
						         // stream
		//std::map<int, Interval> sm_interval; // require to reset the map at start
							      // of vertex.


                public:
		/**
		 * @brief
		 * Copy constructor from other stream.
		 *
		 */
		stream(const stream& other):
			stream_id(other.stream_id),
			scheduled_job_id(other.scheduled_job_id),
			_free(other._free),
			sm_interval(other.sm_interval){};

		/**
		 * @brief
		 * stream constructor, create an empty interval for each SM.
		 *
		 * By default a stream is free and we assign a non possible job id.
		 */
		stream():
			stream_id(0),
			scheduled_job_id(-1),
			_free(true)
		{
			std::fill_n(sm_interval.begin(), NSM, Interval());
		}

		stream(int _id, int _job_id, int _offset):
			stream_id(_id),
			scheduled_job_id(_job_id),
			_free(false)
		{
			std::fill_n(sm_interval.begin(), NSM, Interval(_offset, _offset));
		}

		/**
		 * @brief
		 * Getter for the scheduled_job_id.
		 */
		int get_scheduled_job_id() const
		{
			return scheduled_job_id;
		}

		/**
		 * @brief
		 * Getter for the stream id.
		 */
		int get_stream_id() const
		{
			return stream_id;
		}

		bool is_free() const
		{
			return _free;
		}

		void set_free()
		{
			_free = true;
			//scheduled_job_id = -1;
		}

		/**
		 * @brief
		 * Display the stream.
		 */
		const std::string to_string() const
		{
        		std::stringstream ss;
			ss << "Stream id: " << stream_id << "\n";
			for(std::size_t i = 0; i < NSM; i++)
                        	ss << "sm[" << i << "] -> "
				   << "[" << sm_interval[i].get_left_bound() << ","
				   << sm_interval[i].get_right_bound() << "]\n";
			//for (std::map<int, Interval>::const_iterator
			//	it = sm_interval.begin();
			//	it != sm_interval.end();
			//	++it)
                        //        ss << "sm[" << (*it).first << "] -> "
			//	   << "[" << (*it).second.get_left_bound() << ","
			//	   << (*it).second.get_right_bound() << "]\n";
			//}
			return ss.str();
                }

		/**
		 * @brief
		 * Modify an SM interval by adding an offset.
		 *
		 * @param sm		index to access sm in sm_interval.
		 * @param offset	the offset to add to the sm_interval[sm] left and right
		 * 			bounds.
		 */
		void modify_sm_interval(int sm, int offset)
		{
			sm_interval[sm].update_with_offset(offset);
		}

		/**
		 * @brief
		 * Modify all Interval in stream
		 * This method is used in conjunction with Vertex::fast_forward().
		 *
		 * @param offset	offset to which all sm are updated.
		 */
		void modify_all(int offset)
		{
			for(std::array<Interval, NSM>::iterator it = sm_interval.begin();
					it != sm_interval.end();
					++it)
					(*it).update_with_offset(offset);
		//	for (std::map<int, Interval>::iterator
		//			it = sm_interval.begin();
		//			it != sm_interval.end();
		//			++it) {
		//		(*it).second.update_with_offset(offset);
		//	}

			//scheduled_job_id = GLOBALJOBS.size() + 1; // set to impossible jobs
		}

		void extend_all(int bound)
		{
			for (std::array<Interval, NSM>::iterator it = sm_interval.begin();
					it != sm_interval.end();
					++it)
					(*it).extend_to(bound);
		}
		/**
		 * @brief
		 * Getter for the maximum left bound present in sm_interval.
		 *
		 * @return 	the maximum left bound present in sm_interval.
		 *
		 * @warning 	As a stream can only load a kernel at a time
		 * 		all is abstract sm are occupied the same amount of time
		 * 		so there is no maximum left_bound()
		 */
		int get_max_left_bound() const
		{
			int max_left_bound = 0;

			for(std::array<Interval, NSM>::const_iterator it = sm_interval.begin();
					it != sm_interval.end();
					++it)
			{
				int curr_left_bound = (*it).get_left_bound();
				if(max_left_bound < curr_left_bound)
					max_left_bound = curr_left_bound;
			}
		//	for (std::map<int, Interval>::const_iterator
		//			it = sm_interval.begin();
		//			it != sm_interval.end();
		//			++it) {
		//		int curr_left_bound = (*it).second.get_left_bound();
		//		if(max_left_bound < curr_left_bound)
		//			max_left_bound = curr_left_bound;
		//	}

			return max_left_bound;
		}

		/**
		 * @brief
		 * Getter for the maximum right bound present in sm_interval.
		 *
		 * @return 	the maximum right bound present in sm_interval.
		 * @warning 	As a stream can only load a kernel at a time
		 * 		all is abstract sm are occupied the same amount of time
		 * 		so there is no maximum right_bound()
		 */
		int get_max_right_bound() const
		{
			int max_right_bound = 0;

			for(std::array<Interval, NSM>::const_iterator it = sm_interval.begin();
					it != sm_interval.end();
					++it)

			{
				int curr_right_bound = (*it).get_right_bound();
				if(max_right_bound < curr_right_bound)
					max_right_bound = curr_right_bound;
			}
		//	for (std::map<int, Interval>::const_iterator
		//			it = sm_interval.begin();
		//			it != sm_interval.end();
		//			++it) {
		//		int curr_right_bound = (*it).second.get_right_bound();
		//		if(max_right_bound < curr_right_bound)
		//			max_right_bound = curr_right_bound;
		//	}

			return max_right_bound;
		}

		/**
		 * @brief
		 * Variant of modify_all();
		 *
		 * @param offset	offset to add.
		 */
                void update_stream_to(int offset)
	       	{
			// Wait for all jobs to finish
			// All interval become [offset, offset];
	       		for(std::array<Interval, NSM>::iterator it = sm_interval.begin();
					it != sm_interval.end();
					++it)
			{
				// first rbound then lbound
				// to avoid bug lbound > rbound before rbound updated.
				(*it).extend_rbound_to(offset);
				(*it).extend_lbound_to(offset);
			}
		//	for(std::map<int, Interval>::iterator
		//			it = sm_interval.begin();
		//			it != sm_interval.end();
		//			++it) {
		//		(*it).second.extend_lbound_to(offset);
		//		(*it).second.extend_rbound_to(offset);
		//	}

		// 	scheduled_job_id = GLOBALJOBS.size() + 1;// set scheduled_job_id to
								 // impossible value.
		}

		/**
		 * @brief
		 * Setter for scheduled_job_id.
		 *
		 * @param job_id	the new job id.
		 */
		void set_scheduled_job_id(int job_id)
		{
			scheduled_job_id = job_id;
			_free = false;
		}

		/**
		 * @brief
		 * Conserving we check for the minimal starting time
		 */
                friend bool operator<(const stream& lhs, const stream& rhs)
		{
			return lhs.get_max_right_bound() < rhs.get_max_right_bound();
		}


		/**
		 * @brief
		 * Apply slowdown factor of the task associated to the given job identifier
		 * to the remaining time of the current task running on current stream.
		 *
		 * @param  sms		contains all SMs on which the slow down factor must be
		 * 			applied.
		 * @param  job_id	the identifier of the job.
		 *
		 * Use cases:
		 * Internal stream state is as following:
		 * [5,5] ... [5,5] on each SM and schedule job J1
		 * Jobs J2 arrival time is 3, here slow-down apply only from [3,5] interval so 2
		 * unit of time.
		 */
		void apply_slowdown_factor_on_remaining_time(
				const std::vector<int>& sms,
				int job_id)
		{

#ifdef DEBUG_MODE
// check if the arrival time is earlier than
			DEBUGINFO("Slow-down factor is applied to remaining time in stream ",
					stream_id);
#endif
			int other_task_id = GLOBALJOBS[job_id]->get_associated_task_id();
			int task_id = GLOBALJOBS[scheduled_job_id]->get_associated_task_id();
			int other_arrival_time = GLOBALJOBS[job_id]->get_arrival_time();
			double sf = TASKSSF[task_id][other_task_id];

			for (std::vector<int>::const_iterator it = sms.begin();
					it != sms.end();
					++it) {
				// we start with left_bound == right_bound (BCET = WCET)
				// but already differentiate case.
				// left_bound <= right_bound so begin the if with the
				// left_bound check.
				if ( other_arrival_time < sm_interval[*it].get_left_bound()) {
					int remaining_left =
								sm_interval[*it].get_left_bound()
							       	- other_arrival_time;
			   		int lbound_offset =
								std::ceil( remaining_left * sf);

			       		sm_interval[*it].update_lbound_with_offset(lbound_offset);

					int remaining_right =
								sm_interval[*it].get_right_bound()
							        - other_arrival_time;
					int rbound_offset =
								std::ceil(remaining_right * sf);

					sm_interval[*it].update_rbound_with_offset(rbound_offset);
				}
				else if (other_arrival_time < sm_interval[*it].get_right_bound()) {

					int remaining_right =
								sm_interval[*it].get_right_bound()
							        - other_arrival_time;
					int rbound_offset =
								std::ceil(remaining_right * sf);

					sm_interval[*it].update_rbound_with_offset(rbound_offset);
				}
			}
		}
	}; // stream structure

	/**
	 * @brief
	 * Vertex represent a vertex in the dag we create.
	 *
	 */
	class Vertex
	{
		protected:
		int vertex_id;
		int origin_vertex_id; /// @brief parent unique identifier.
		std::shared_ptr<Vertex> origin;
                Occupancy vertex_occupancy; /// @brief GPU occupancy.
		//std::array<struct stream, 7> streams_arr; // 7 arbitrary number will have to generate
							  // .h file with those information before
							  // launching gpusched.
							  //
		/// @brief Abstract cuda stream
		std::vector<struct stream> streams; // want to pre-configure n stream with n number
						    // of applications jobs.
						    //
		/// @brief jobs that have been scheduled
		std::vector<int> all_scheduled_jobs;

		//std::vector<int> remaining_jobs; Maybe it's better to use bitset

		/// @brief jobs ready to be executed from which permutations are computed.
		std::vector<int> ready_jobs;

		//std::bitset<NB_JOBS> remaining_jobs; // change it to dynamic bitset of size of GLOBALJOBS

		boost::dynamic_bitset<> remaining_jobs;

		std::vector<int> curr_jobs_to_schedule; // Each vertex has only one
								 // combinaison of jobs assigned
								 // during the space exploration.
								 // maybe we can replace it to be
								 // a unordered_map so we can
								 // assign for each job the stream
								 // we used to dispatch it. But we
								 // already store this information
								 // in stream structure.


		/// @brief number of jobs scheduled.
		int depth;

		/**
		 * @brief
		 * Flag to know if the vertex is schedulable. (i.e all jobs present in GLOBALJOBS
		 * have been scheduled within their timing constraints.)
		 */
		bool schedulable;

		/**
		 * @brief
		 * Flag to know if any scheduled jobs missed its deadline.
		 *
		 * When a vertex is marked as invalid, we stop the exploration for this vertex.
		 */
		bool is_invalid_vertex;
		//std::set<std::vector<int> > permutations; // will be move outside class
								   // and be computed during the
								   // space exploration.

		int time;
		std::deque<int> free_stream;
		int _count_stream;
		utils::schedule_type schedule_t;
		int slack_time;

		double mean_thread_usage;
		double mean_regs_usage;
		double mean_register_usage;
		std::set<int> delta_dominance;

		bool created_by_dominance;
		protected:
		bool dominated_to_remove; 
		static std::atomic<int> _id;
		std::vector<int> past_vertex;
		public:



		/**
		 * @brief
		 * Copy constructor from an origin Vertex object.
		 *
		 * @param origin 	The vertex from which the current vertex will be created.
		 * @param workload	The jobs to be executed during this vertex.
		 */
		Vertex(const Vertex& origin, std::vector<int> workload, utils::schedule_type st):
			vertex_id(++_id),
			origin_vertex_id{origin.vertex_id},
			origin{std::make_shared<Vertex>(origin)},
			vertex_occupancy{origin.vertex_occupancy},
			streams{origin.streams},
			all_scheduled_jobs{origin.all_scheduled_jobs},
			remaining_jobs{origin.remaining_jobs},
			curr_jobs_to_schedule{workload},
		       	depth{0},
			schedulable{false},
			is_invalid_vertex{false},
			free_stream{origin.free_stream},
			_count_stream{origin._count_stream},
			schedule_t{st},
			time{origin.time},
			past_vertex{origin.past_vertex},
			delta_dominance{origin.delta_dominance},
			dominated_to_remove{origin.dominated_to_remove}
			{
				int stream_index;
				created_by_dominance = false;
				switch (st)
				{
					case utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM:
						stream_index = get_higher_stream_index();
						//time = streams[stream_index].get_max_right_bound();
						vertex_occupancy.reset();
					for (auto stream: streams) {
						if (stream.get_scheduled_job_id() != -1) {
							//vertex_occupancy.free_resources(stream.get_scheduled_job_id());
							stream.update_stream_to(time);
							stream._free = true;
							stream.scheduled_job_id = -1;
							//free_stream.push_back(stream.get_stream_id());
						}
					}
					fetch_ready_jobs(st);
						break;
					case utils::schedule_type::WORK_CONSERVING_LOWER_STREAM:
						stream_index = get_lower_stream_index();
					default:
						stream_index = get_higher_stream_index();
						break;
				}

				//time = streams[stream_index].get_max_right_bound();
				past_vertex.push_back(origin_vertex_id);
				dominated_to_remove = false;
			};
		Vertex(const Vertex& origin,int time,utils::schedule_type st):
			vertex_id(++_id),
			origin_vertex_id{origin.vertex_id},
			origin{std::make_shared<Vertex>(origin)},
			vertex_occupancy{origin.vertex_occupancy},
			streams{origin.streams},
			all_scheduled_jobs{origin.all_scheduled_jobs},
			remaining_jobs{origin.remaining_jobs},
		       	depth{0},
			schedulable{false},
			is_invalid_vertex{false},
			free_stream{origin.free_stream},
			_count_stream{origin._count_stream},
			schedule_t{st},
			past_vertex{origin.past_vertex},
			time{time},
			dominated_to_remove{origin.dominated_to_remove}
			{
				created_by_dominance = true;
				int stream_index;
				switch (st)
				{
					case utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM:
						stream_index = get_higher_stream_index();
						vertex_occupancy.reset();
					for (auto stream: streams) {
						if (stream.get_scheduled_job_id() != -1) {
							//vertex_occupancy.free_resources(stream.get_scheduled_job_id());
							//stream.update_stream_to(streams[stream_index].get_max_right_bound());
							stream.update_stream_to(time);
							stream._free = true;
							stream.scheduled_job_id = -1;
							//free_stream.push_back(stream.get_stream_id());
						}
					}
					fetch_ready_jobs(st);
						break;
					case utils::schedule_type::WORK_CONSERVING_LOWER_STREAM:
						stream_index = get_lower_stream_index();
					default:
						stream_index = get_higher_stream_index();
						break;
				}
				past_vertex.push_back(origin_vertex_id);
				dominated_to_remove = false;
			};

		/**
		 * @brief
		 * Constructor initiate the initial vertex.
		 */
		Vertex(): vertex_id(++_id),
			  origin_vertex_id{0},
			  depth{0},
			  schedulable{false},
			  is_invalid_vertex{false},
			  time{0},
			  _count_stream{0}
		{
			
			remaining_jobs.resize(GLOBALJOBS.size());
			remaining_jobs.set();
			//remaining_jobs.set();
			struct stream original_stream;
			streams.push_back(original_stream);
			dominated_to_remove = false;
			//free_stream.push_back(original_stream.get_stream_id());
		}

/*===== Getter =====*/
		/**
		 * @brief
		 * Getter for the vertex_id.
		 *
		 * @return 	the current vertex_id.
		 */
		int get_vertex_id() const;

		int get_origin_vertex_id() const;

		int get_count_stream() const;

		int get_depth() const;

		/**
		 * @brief
		 * A vertex is schedulable if remaining_jobs vector is empty.
		 */
		bool is_schedulable() const;

		/**
		 * @brief
		 * Get the is_invalid_vertex flag.
		 */
		bool get_invalid_vertex_flag() const;

		Vertex get_origin_vertex() {
			return *origin.get();
		}
		/**
		 * @brief
		 * Getter for the index corresponding to the stream with the minimal
		 * interval present in streams vector.
		 *
		 * Work-conserving, we always choose the stream with the lowest finish time.
		 * @return 	index to access the stream with the minimal interval.
		 */
		int get_lower_stream_index() const;

		/**
		 * @brief
		 * Getter for the index associated to the stream with the maximal
		 * right bound present in streams vector.
		 *
		 * Non work-conserving, we wait the execution end of the longest job.
		 * @return 	index to acces the stream with the maximal right bound.
		 */
		int get_higher_stream_index() const;

		boost::dynamic_bitset<> get_remaining_jobs() const;

		double get_mean_thread_usage() const;
		double get_mean_reg_usage() const;

		int get_time() const;
/*===== end Getter =====*/

		/**
		 * @brief
		 * Jobs in curr_jobs_to_schedule are schedulable if and only if they respect their
		 * deadline after being scheduled.
		 *
		 * @return 	True if for all jobs, fi <= di,
		 * 		False, otherwise.
		 */
		bool curr_jobs_is_schedulable() const;

                /**
                 * @brief
                 * For each value in curr_jobs add an entry into all_scheduled_jobs and remove it
		 * from the remaining_jobs.
		 *
                 */
                void handle_scheduled_jobs();

		/**
		 * @brief
		 * When there is no job ready to be executed at the start of the vertex (i.e when
		 * ready_jobs vector is empty) this function will forward streams interval to the
		 * next job minimal arrival time for his children Vertex.
		 *
		 * Update all interval present in current stream to the minimal arrival time of
		 * jobs present in remaining_jobs.
		 */
		std::vector<int> fast_forward();
		std::vector<int> fast_forward_to(int t);

		/**
		 * @brief
		 * Give the job with the minimal arrival time from a given subset of GLOBALJOBS
		 *
		 * @param remaining_jobs	subset of job id present in GLOBALJOBS.
		 *
		 * @return int		job id of job with the minimal arrival time
		 * 				present in GLOBALJOBS.
		 */
		int min_arrival_time_job() const;

		/**
		 * @brief
		 * For each job in  curr_jobs check if the job GPU workload can be dispatched on
		 * the current state of the GPU.
		 *
		 * If there is not enough resources available, we recheck with the method
		 * check_occupancy_after_free_streams() the combinaison occupancy after the
		 * execution of all currently running jobs.
		 *
		 *
		 * Example:
		 * Current vertex v2 inherit streams s1 and s2 from a vertex v1
		 * s1 : [5,5] [5,5] [5,5] [5,5] <- jobs J1
		 * s2 : [0,0] [0,0] [0,0] [0,0]
		 * At vertex v2 we want to dispatch J2 ready at time 0, but J1 occupancy is too
		 * high to let J2 run on s2.
		 *
		 * In this case checkOccupancy will tell the program to wait until [5,5] and
		 * dispatch J2 on s1.
		 *
		 * @return bool		True if assigned workload can be executed GPU with the
		 * 			actual state of the gpu.
		 * 			False otherwise.
		 *
		 */
		bool check_occupancy_actual_state(); // note we work on the current occupancy
						     // maybe it's better to use a copy so we don't
						     // have to care about free allocation.


		bool check_occupancy_actual_state(const std::vector<int>& permutation);

		/**
		 * @brief
		 * For each job in curr_jobs_to_schedule check if the job GPU workload can be
		 * dispatched on the GPU after all resources have been freed.
		 *
		 * @return  		True if assigned workload can be executed on GPU after all
		 * 			resources have been freed.
		 * 			False otherwise.
		 */
		bool check_occupancy_after_free_streams(const std::vector<int>& permutation);

		std::vector<int> get_curr_jobs_to_schedule() const;

		/**
		 * @brief
		 * Give the stream with the minimal ending time (right bound is minimal) and free.
		 *
		 * @return 	Streeam unique identifier.
		 */
		 int get_free_and_lower_stream() const;


		 /**
		  * @brief
		  * Return current vertex streams vector
		  *
		  * @return 	streams
		  */
		 const std::vector<struct stream>& get_streams() const;


		 /**
		  * @brief
		  * Return current vertex scheduled jobs
		  *
		  * @return scheduled_jobs
		  */
		 const std::vector<int>& get_scheduled_jobs() const;

		 const std::vector<int>& get_rdy_jobs() const;
		 std::vector<int> get_copy_rdy_jobs() const;
		 const std::vector<int>& get_past_vertex() const;

		 const std::set<int>& get_delta_dominance() const ;
		 void update_delta_dominance(const std::vector<int>& v);
		 void update_delta_dominance(int v);
		/**
		 * @brief
		 * Check the dominance relation between the current vertex and a list of vertex.
		 * Modify the given list of dominants vertex in consequence.
		 *
		 * @param others 	list of dominant vertex.
		 */
		 bool apply_dominance(std::vector<Vertex>& others) const;

		/**
		 * @brief
		 * Check the dominance relation between the current vertex and a given vertex.
		 *
		 *
		 * @return 	True if the current vertex domine the given one.
		 * 		False otherwise.
		 */
		 error::error_t check_dominance(const Vertex& other) const;


		const std::string to_string() const;


		/**
		 * @brief
		 * Function that check current scheduled jobs deadlines
		 */
		bool check_scheduled_jobs_deadlines();

		bool check_rdy_jobs();
		/**
		 * @brief
		 * Dispatch the jobs presents in the attributed permutation to streams.
		 */
		void dispatch_job_conserving();

		void free_permutation_resources_check(const std::vector<int>& permutation);

		/**
		 * @brief
		 * Fetch all ready jobs ready to be executed at the end of the
		 * minimal stream
		 *
		 * This method is used to generate the permutation to generate new vertex.
		 *
		 * @return 	a vector containing all jobs unique identifier from which
		 * 		generate permutations.
		 */
		std::vector<int> fetch_ready_jobs(utils::schedule_type st);


/*===== Streams related methods =====*/
		/**
		 * @brief
		 * Add a new stream entity in the streams vector.
		 *
		 * When a permutations is correct if the length of the permutations is bigger
		 * than the current available streams, this function will add new streams.
		 */
		void add_new_stream();

		/**
		 * @brief
		 * Set all stream that finish their execution
		 */
		void set_free_stream_after_fetch(int stream_finish_time);
		void set_free_used_streams();
		void set_rdy_jobs(std::vector<int> rdy_jobs);
		void set_time(int time){
			time = time;
		}
		void create_stream(int job_id);

		void reuse_stream(int job_id);
		// add function that take the maximum occupancy for a given set of job_id
		// EDF + un autre job.
		// check work-conserving vs non-work converving
		//
		// J1-> J2|J3
		// on tente J1|J2 puis J3 work-conserving


		int get_next_job_edf(std::vector<int> jobs_id);
		int get_next_job_edf();
		int get_next_job_edf2();

		void clear_dominance(){
			delta_dominance.clear();
		}
		/*
		 * this method is used to generate new vertex from a given time
		 * generated by dominance
		 * must update rdy jobs
		 * scheduled jobs
		 * remaining jobs
		 */
		void handle_in_time(std::vector<int> job_ids)
		{
			for(auto q: job_ids){
				remaining_jobs[q] = false;
				all_scheduled_jobs.push_back(q);
			 	auto pos = std::find(ready_jobs.begin(), ready_jobs.end(), q);
				if (pos != ready_jobs.end())
					ready_jobs.erase(pos);
			}

		}
		const bool get_dominated_to_remove() const{return dominated_to_remove;};
		void set_dominated_to_remove(){dominated_to_remove = true;}; 
	}; // Vertex class

/*===== Look up function =====*/
	void look_up(const Vertex& v);
	void path_jobs_order(const Vertex& v);
}// namespace gpusched


#endif
