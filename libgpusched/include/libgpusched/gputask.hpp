/// @file gputask.hpp
#ifndef __GPUTASK__HPP
#define __GPUTASK__HPP
#include <libgpusched/logger.hpp>
#include <libgpusched/task.hpp>

namespace gpusched {

	typedef struct {
		int block;            // number of block composing the kernel
		int thread_per_block; // number of thread composing a block
		int regs;             // register allocated per thread
		int smem;             // shared memory allocated by block
	} cuda_kernel_parameters;

// struct must be associated to a job not a gpuTask
// TODO TO MOVE to job.hpp
// typedef struct
//{
//	// @brief boolean to know if kernel has been allocated
//	bool hasBeenAllocated;
//	// @brief save on which sm blocks have been dispatched
//	std::map<int, int> sm;
//} alloc;

	class GpuTask : public Task {
 		std::unique_ptr<cuda_kernel_parameters> cuda_params;

		public:

		GpuTask(int task_id, std::unique_ptr<cuda_kernel_parameters> kernel_parameters,
          		std::unique_ptr<real_time_parameters> rt_parameters)
      			: Task(task_id, std::move(rt_parameters), true),
        	  	  cuda_params(std::move(kernel_parameters)){ };

  		~GpuTask();

  		cuda_kernel_parameters *get_kernel_params() const;

  		int get_block() const;

  		int get_thread_per_block() const;

  		int get_reg() const;

  		int get_smem() const;

  		std::string to_string() override;
	};

} // namespace gpusched

#endif
