#ifndef __GRAPH_GEN_HPP
#define __GRAPH_GEN_HPP

#include <libgpusched/vertex.hpp>

#include <string>
namespace gpusched
{
	namespace graph_viz
	{

		class GraphGen
		{
			private:
			std::ofstream out;
			std::ofstream node;
			std::ofstream graph;
			static GraphGen* m_instance;

			int generated_node;
                        public:
			GraphGen(const std::string filename, bool details ): out(filename), node("./node.tmp"), graph("./graph.tmp"), generated_node(1) {
				out << "digraph {" << std::endl;
				if (!details)
					node << "{\n node [margin=0 fontcolor=black shape=circle style=filled]\n" << std::endl;
				else
					node << "{\nnode [shape=record style=filled]\n" << std::endl;

				m_instance = this;
			}

			void add_edge(const Vertex& vertex, gpusched::error::error_t state) {
				std::string color;
				std::string vertex_name("v" + std::to_string(vertex.get_vertex_id()));
				switch(state) {
					case gpusched::error::EALLOCB:
						color = "red2";
						node << vertex_name << " [fillcolor=" << color << "]" << std::endl;
						break;
					case gpusched::error::EDOMINATED:
						color = "green2";
						node << vertex_name << " [fillcolor=" << color << "]" << std::endl;
						break;
					case gpusched::error::EDEADLINE:
						color= "yellow3";
						node << vertex_name << " [fillcolor=" << color << "]" << std::endl;
						break;
					case gpusched::error::ERDYJOBSDEADLINE:
						color= "blue";
						node << vertex_name << " [fillcolor=" << color << "]" << std::endl;
						break;
					case gpusched::error::EDOMINE:
						color= "black";
						node << vertex_name << " [fillcolor=white]" << std::endl;
						break;
					case gpusched::error::ESCHEDULABLE:
						color = "black";
						node << vertex_name << " [fillcolor=gold]" << std::endl;
						break;
					default:
						color= "black";
						node << vertex_name << " [fillcolor=white]" << std::endl;
				}


        			std::stringstream label;
				auto scheduled_jobs = vertex.get_curr_jobs_to_schedule();
				if (scheduled_jobs.size() >1) {
					for (auto job = scheduled_jobs.begin(); job != std::prev(scheduled_jobs.end()); ++job)
						label << (*job)<< "|";
					label << *(std::prev(scheduled_jobs.end()));
				} else if (scheduled_jobs.size() == 1) {
					label << scheduled_jobs[0];
				}

//				std::cout << "LABEL IS " << label.str() << std::endl;
				graph << "v"
				    << vertex.get_origin_vertex_id()
				    << " -> "
				    << "v"
				    << vertex.get_vertex_id()
				    << "[label=\""
				    << label.str()
				    << "\" "
				    << "color=\""
				    << color
				    << "\"];"
				    << std::endl;
				generated_node++;
			}

			void add_edge_details(const Vertex& vertex, gpusched::error::error_t state) {
				std::string color;
				std::string vertex_name("v" + std::to_string(vertex.get_vertex_id()));
				std::stringstream ss;

				ss << " label=\"{name | time | scheduled_jobs | rdy_jobs | U_thread} | {v" << vertex.get_vertex_id()
				   << " | "
				   << vertex.get_time()
				   << " | ";
				for (auto job_id: vertex.get_scheduled_jobs())
					ss << job_id << " ";
				ss << " | ";
				for (auto rdy_job_id: vertex.get_rdy_jobs())
					ss << rdy_job_id << " ";
				ss << " | "
				   << vertex.get_mean_thread_usage()/30000; // to change here 30000 is the hyperperiod of job_set
				ss << "}\"]"
				<< std::endl;

				switch(state) {
					// add vertex name
					// scheduled jobs
					// rdy_jobs
					// occupancy estimation
					case gpusched::error::EALLOCB:
						color = "red2";
						node << vertex_name << " [fillcolor=" << color;
						break;
					case gpusched::error::EDOMINATED:
						color = "green2";
						node << vertex_name << " [fillcolor=" << color;
						break;
					case gpusched::error::EDEADLINE:
						color= "yellow3";
						node << vertex_name << " [fillcolor=" << color;
						break;
					case gpusched::error::ERDYJOBSDEADLINE:
						color= "blue";
						break;
					case gpusched::error::EDOMINE:
						color= "black";
						node << vertex_name << " [";
						break;
					case gpusched::error::ESCHEDULABLE:
						color = "black";
						node << vertex_name << " [fillcolor=gold ";
						break;
					default:
						color= "black";
						node << vertex_name << " [fillcolor=white]" << std::endl;
				}

				node << ss.str();

        			std::stringstream label;
				auto scheduled_jobs = vertex.get_curr_jobs_to_schedule();
				if (scheduled_jobs.size() >1) {
					for (auto job = scheduled_jobs.begin(); job != std::prev(scheduled_jobs.end()); ++job)
						label << (*job)<< "|";
					label << *(std::prev(scheduled_jobs.end()));
				} else if (scheduled_jobs.size() == 1) {
					label << scheduled_jobs[0];
				}

//				std::cout << "LABEL IS " << label.str() << std::endl;
				graph << "v"
				    << vertex.get_origin_vertex_id()
				    << " -> "
				    << "v"
				    << vertex.get_vertex_id()
				    << "[label=\""
				    << label.str()
				    << "\" "
				    << "color=\""
				    << color
				    << "\"];"
				    << std::endl;
				generated_node++;
			}
			void add_domination_relation(const Vertex& dominated, const Vertex& domine)
			{

				graph << "v" << dominated.get_vertex_id()
					<< " -> "
					<< "v"
					<< domine.get_vertex_id()
					<< "[color=\"brown\"];"
					<< std::endl;
			}

			~GraphGen()
			{
				node << "}"<< std::endl;
				node.close();
				graph.close();

				std::ifstream node_description("./node.tmp");
				std::ifstream graph_description("./graph.tmp");

//				out << node_description << graph_description << std::endl;
				std::string str;
				while(std::getline(node_description,str))
					out << str << std::endl;

				while(std::getline(graph_description,str))
					out << str << std::endl;
				node_description.close();
				graph_description.close();
				remove("./node.tmp");
				remove("./graph.tmp");
				out << "}";
				out.close();

				std::ofstream execution_time("./execution_time", std::ios::app);
				execution_time << " Number of node generated: " << generated_node << std::endl;
				execution_time.close();

			}


			static GraphGen* getInstance()
			{
				if (m_instance == nullptr)
					m_instance = new GraphGen("./test.dot", false);
				return m_instance;
			}

			static void DestroyInstance()
			{
				delete m_instance;
			}
			void operator=(const GraphGen) = delete;

			GraphGen(GraphGen &) = delete;
		};


	}// namespace graph_viz
} // namespace gpusched
#endif
