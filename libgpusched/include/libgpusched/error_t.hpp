#ifndef __ERROR_T__HPP
#define __ERROR_T__HPP

#include <array>

#define ERROR	gpusched::error::error_t
namespace gpusched
{
	namespace error
	{
		typedef int error_t;

		constexpr error_t success 		= 0;
		constexpr error_t EIAV 			= 1;    // error : Invalid argument value
		constexpr error_t EALLOCB 		= 2; 	// error : allocation block
		constexpr error_t EDOMINATED 		= 3; 	// The vertex is dominated
		constexpr error_t EDOMINE 		= 4;    // The vertex domine
		constexpr error_t ENOTSUBSET 		= 5;
		constexpr error_t EDEADLINE 		= 6;
		constexpr error_t ESCHEDULABLE 		= 7;
		constexpr error_t ERDYJOBSDEADLINE 	= 8;
		constexpr error_t ESAMEVERTEX		= 9;

		constexpr std::array errorMessage = {
			"success",
			"Invalid Argument Value",
			"Block allocation failed",
			"Vertex is dominated",
			"Vertex dominate",
			"Vertex scheduled jobs are not a subset of other vertex",
			"Jobs deadline missed",
			"Vertex is schedulable",
			"Jobs in ready jobs already missed its deadline"
		};


		constexpr const char * getErrorMessage(error_t error) {
			if (static_cast<long unsigned int>(error) < errorMessage.size())
				return errorMessage[error];
			return "out of bounds";
		}
	} // namespace error
} // namespace gpusched

#endif
