/// @file interval.hpp
#ifndef __INTERVAL__HPP
#define __INTERVAL__HPP
#include <libgpusched/logger.hpp>
//#include <libgpusched/error_t.hpp>
#include <algorithm>

namespace gpusched
{

	/**
	 * @brief
	 * Represent an interval with left and right bound.
	 */
	class Interval final
	{
		/// @brief left_bound of interval @warning left_bound <= right_bound
		int left_bound;
		/// @brief right_bound of interval @warning right_bound <= right_bound
		int right_bound;
		public:

		/**
		 * @brief
		 * Interval class constructor
		 * Construct an interval object from given left and right bound.
		 *
		 * @param lbound 	left bound
		 * @param rbound 	right bound
		 */
		Interval(int lbound, int rbound) :
			left_bound(lbound),
			right_bound(rbound){};
		/**
		 * @brief
		 * Interval class constructor
		 * Construct an interval from a given interval.
		 *
		 * @param origin	an interval object.
		 */
		Interval(const Interval& origin) :
			left_bound(origin.left_bound),
			right_bound(origin.right_bound){};

		/**
		 * @brief
		 * Interval class constructor
		 * Construct an interval initialized at [0,0]
		 */
		Interval(): left_bound(0), right_bound(0){};

		~Interval(){};

		/**
		 * @brief
		 * Getter for the left bound
		 *
		 * @return 	the left bound.
		 */
		int get_left_bound() const;

		/**
		 * @brief
		 * Getter for the right bound
		 *
		 * @return	the right bound.
		 */
		int get_right_bound() const;

		/**
		 * @brief
		 * Function checking if an interval is containing in the current interval.
		 *
		 * @return 		True if the given interval is containing
		 * 			in the current interval.
		 * 			False, otherwise.
		 */
		bool in(const Interval& other) const; // \TODO rename it to contains
		/**
		 * @brief
		 * Function checking if a given time is containing in the current interval.
		 *
		 * @return 		True if the given time is containing
		 * 			in the current interval.
		 * 			False, otherwise.
		 */
		bool in(int time) const;
		bool in_right_exclusive(int time) const;

		bool is_greater(int t) const;

		/**
		 * @brief
		 * Function that check if the given interval and the current interval intersect.
		 *
		 * @param other	 	given interval.
		 *
		 * @return 		True if they intersect. False, otherwise.
		 */
		bool intersect(const Interval& other) const;

		/**
		 * @brief
		 * Function that check if the given interval and the current interval are disjoint.
		 *
		 * @param other	 	given interval.
		 *
		 * @return 		True if they are disjoint. False, otherwise.
		 */
		bool disjoint(const Interval& other) const;

		/**
		 * @brief
		 * Check if the given interval finish later than a given interval.
		 *
		 * @param other		given interval.
		 *
		 * @return		True if the current interval finish later
		 * 			than other interval.
		 * 			False, otherwise.
		 */
		bool finish_later_than(const Interval& other) const;

		bool finish_later_than(int time) const;


		/**
		 * @brief
		 * Extend right and left bound of the current interval to the given value.
		 *
		 * @param bound		given value to which extend the right and left bound
		 * 			attribut.
		 */
		void extend_to(int bound);

		/**
		 * @brief
		 * Extend the right bound of the current interval to the given value.
		 *
		 * @param rbound	given value to which extend the right_bound attribut.
		 */
		void extend_rbound_to(int rbound);

		/**
		 * @brief
		 * Extend the left bound of the current interval to the given value.
		 *
		 * @param lbound 	given value to which extend the left_bound attribut.
		 */
		void extend_lbound_to(int lbound);

		/**
		 * @brief
		 * Update the current interval with the given offset.
		 *
		 * @param offset	given offset to add.
		 */
		void update_with_offset(int offset);


		/**
		 * @brief
		 * Update the current right bound with the given offset.
		 *
		 * @param offset 	given offset to add to right bound.
		 */
		void update_rbound_with_offset(int offset);

		/**
		 * @brief
		 * Update the current left bound with the given offset.
		 *
		 * @param offset 	given offset to add to the left bound.
		 */
		void update_lbound_with_offset(int offset);

		/**
		 * @brief
		 * Overloading the operator+ for addition between an Interval and a given offset.
		 *
		 * @return 		an Interval on which we add the offset to right_bound and
		 * 			left_bound.
		 */
		Interval operator+(int offset) const;
	}; // class Interval
} // namespace gpusched
#endif
