#ifndef __GPU_CONF__HPP
#define __GPU_CONF__HPP


namespace
{
	const int NSM = 8;
	const int THREADS_PER_SM = 2048;
	const int _THREADS = THREADS_PER_SM * NSM;
	const int MAX_BLOCK_PER_SM = 32;

	const int SMEM_PER_BLOCK = 49152;
	const int REGS_PER_BLOCK = 65536;

	const int SMEM_ALLOC_UNIT_SIZE = 128;
	const int REGS_ALLOC_UNIT_SIZE = 256;

	// alloc register 32 thread per 32 thread (warp)
	const int REG_ALLOC_GRANULARITY = 32;
	const int WARP_ALLOC_GRANULARITY = 4;

	const int MAX_REGS_PER_THREAD = 255;

	const int MAX_THREAD_PER_BLOCK = 1024;


}
#endif
