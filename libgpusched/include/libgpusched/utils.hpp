/// @file utils.hpp
#ifndef __UTILS__HPP
#define __UTILS__HPP
#include <libgpusched/interval.hpp>
#include <libgpusched/task.hpp>
#include <libgpusched/gputask.hpp>
#include <libgpusched/job.hpp>

#include <cmath>
#include <numeric>
#include <set>
#include <thread>
#include <string>
#include <vector>

namespace gpusched
{
	namespace utils
	{
		/// @brief const FLAG to determine if a task is a GpuTask.
		const int GPUTASK = 1;
		// @brief const FLAG to determine if a task is a CpuTask.
		const int CPUTASK = 0;

		const unsigned long hw_threads = std::thread::hardware_concurrency();
		/**
		 * @brief
		 * enumeration for scheduler type.
		 */
		enum class schedule_type: int
		{
			WORK_CONSERVING_HIGHER_STREAM, // wait the end of all task
			WORK_CONSERVING_LOWER_STREAM,  // fetch as soon as a stream is free
			NON_WORK_CONSERVING_HIGHER_STREAM,
			NON_WORK_CONSERVING_LOWER_STREAM
		};


		/**
		 * @brief
		 * Function that will compute the new wcet of a task with the given
		 * slowdown factor.
		 *
		 * @param sf 	Slowdown factor to apply.
		 * @param wcet	Worst-case execution time of a task executing in isolation.
		 */
		int round_up_with_sf(double sf, int wcet);

		int get_next_different_arrival_job(int time);
// ======================= Fetching function. =======================
		/**
		 * @brief
		 * Function that will fetch all application tasks from a given filename.
		 * This function will fill GLOBALTASKS with tasks and TASKSSF with slowdown factors.
		 *
		 * @param filename	the filename where tasks are defined.
		 */
		void fetchTasks(const std::string &filename);

		/**
		 * @brief
		 * Function that will fetch all application jobs from a given filename.
		 * This function will fill GLOBALJOBS with jobs.
		 *

		 * @param filename	the filename where jobs are defined.
		 */
		void fetchJobs(const std::string &filename);

// ======================= Function associated to permutation generation. =======================
		/**
		 * @brief
		 * Give all permutation of a set of jobs.
		 *
		 * @param jobs		vector containing jobs id.
		 *
		 * @return 		A set which contains all permutations possible.
		 */
		std::set<std::vector<int> > get_permutations(
				std::vector<int> jobs);

		/**
		 * Give all combinaison of a vector of ready jobs.
		 *
		 * @param permutations		a reference to a set of permutations to fill.
		 * @param ready_jobs		vector of jobs ready to be executed.
		 */
		void get_all_combinaisons(std::set<std::vector<int> >& permutations,
				std::vector<int> ready_jobs);

		/**
		 * Give r combinaison of a vector of ready jobs.
		 *
		 * @param k 			size of the combinaison
		 * @param permutations		a reference to a set of permutations to fill.
		 * @param ready_jobs 		vector of jobs ready to be executed.
		 */
		void get_k_combinaisons(long unsigned int k,
				std::set<std::vector<int> >& permutations,
				std::vector<int> ready_jobs);


		void display_combinaison(const std::vector<int>& comb);

// ======================= Function associated to permutation generation. =======================
// ======================= Sub-set functions. =======================

		/**
		 * @brief
		 * Check if a vector vec_b is a subset of a vector vec_a.
		 *
		 * We use a copy not a reference to not remove the schedule order in vertex.
		 * @param vec_a 	vector to be compared to
		 * @param vec_b 	vector for which we check if it is a subset of vec_b.
		 *
		 * @return		True if vec_a is a subset of vec_b,
		 * 			False otherwise.
		 */
		template<typename T>
		bool is_subset(std::vector<T> vec_a, std::vector<T> vec_b)
		{
			std::sort(vec_a.begin(), vec_a.end());
		        std::sort(vec_b.begin(), vec_b.end());
			return std::includes(vec_a.begin(), vec_a.end(),
					     vec_b.begin(), vec_b.end());
		}

		/*
		 * without sort for hash
		 */
		template<typename T>
		bool is_subset_hash(std::vector<T> vec_a, std::vector<T> vec_b)
		{
			return std::includes(vec_a.begin(), vec_a.end(),
					     vec_b.begin(), vec_b.end());
		}
		std::vector<int> difference_set(std::vector<int> vec_a, std::vector<int> vec_b);

		/**
		 * @brief
		 * Function that will compute the slowdown factor that will be applied to a subset
		 * of jobs running concurrently.
		 *
		 * @param jobs	A set of jobs id.
		 *
		 * @return 	The slowdown to apply at each jobs associated wcet.
		 */
		double get_sf_from_set(std::vector<int>& jobs);

		double other_get_sf_from_set(std::vector<int>& jobs);


		template<typename Iterator>
		double get_sf_from_set_it(Iterator begin, Iterator end){
			double sf = 1.0;
			for(auto it = begin; it != end; it++){
				for(auto itt = it+1; itt != end; ++itt){ //+1 avoid useless multiplication
					int task_id_a = GLOBALJOBS[*it]->get_associated_task_id();
					int task_id_b = GLOBALJOBS[*itt]->get_associated_task_id();
					sf *= TASKSSF[task_id_a][task_id_b];
				}
			}
			return sf;
		}
		template<typename Iterator>
		double other_get_sf_from_set_it(Iterator begin, Iterator end){
			double sf = 1.0;
			int task_id_a = GLOBALJOBS[*begin]->get_associated_task_id();
			for(auto itt = begin+1; itt != end; ++itt){ //+1 avoid useless multiplication
				int task_id_b = GLOBALJOBS[*itt]->get_associated_task_id();
				sf *= TASKSSF[task_id_a][task_id_b];
			}
			return sf;
		}
// ======================= Sub-set functions. =======================


// ======================= Hash functions. =======================
		/**
		 * @brief
		 * Hash function to represent a permutation.
		 */
		struct PermutationHash
		{

			static std::vector<std::pair<size_t,std::vector<int>> > all_invalid_permutations;

			std::size_t operator()( const std::vector<int>& v) const noexcept
			{
				// first check if the current permutation is not a super set of
				// a previous invalid combinaison if so we return the previous
				// invalid combinaisons hash to stop expleoration
				auto tmp = v;
				if (auto it = std::find_if(all_invalid_permutations.begin(),
						 all_invalid_permutations.end(),
						 [tmp](const std::pair<size_t,std::vector<int> >& invalid_perm)
						 {
						 if (tmp.size() < invalid_perm.second.size())
								 return false;
						 std::vector<int> task_v;
						 std::vector<int> task_invalid;
						 for (auto i = 0; i!= invalid_perm.second.size();i++)
						 {
						 	task_v.push_back(GLOBALJOBS[invalid_perm.second[i]]->get_associated_task_id());
						 	task_invalid.push_back(GLOBALJOBS[tmp[i]]->get_associated_task_id());
						 }

						 return (is_subset_hash(task_v, task_invalid) && (tmp.size() > invalid_perm.second.size()));
						 });
						it!= all_invalid_permutations.end())
						return (*it).first;
				//use new hash https://stackoverflow.com/questions/20511347/a-good-hash-function-for-a-vector
				std::size_t seed = v.size();
				//std::sort(v.begin(), v.end()); // can't sort
							       // due to gpu internal state
  				for(auto& i : v) {

   		 			seed ^= GLOBALJOBS[i]->get_associated_task_id() + 0x9e3779b9 + (seed << 6) + (seed >> 2);

			//		bool is_start = std::find(invalid_perm.begin(), invalid_perm.end(), seed) == invalid_perm.end();
			//		if (is_start)
			//				return seed;
   		 			//seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  				}

				//for(auto x : v) {
				//	auto tmp = x;
    				//	x = ((x >> 16) ^ x) * 0x45d9f3b;
    				//	x = ((x >> 16) ^ x) * 0x45d9f3b;
    				//	x = (x >> 16) ^ x;
    				//	seed ^= x + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  				//}
				//all_invalid_permutations.push_back(std::make_pair(seed,v));
				return seed;
			}
		};

		struct PermutationHashParallel
		{

			static std::vector<std::pair<size_t,std::vector<int>> > all_invalid_permutations;

			void operator() (std::set<std::vector<int> >::iterator start,
				       	 std::set<std::vector<int> >::iterator end,
					 std::vector<std::size_t>::iterator  results_start,
					 std::vector<std::size_t>& results,
					 int block_size
				       )const noexcept
			{
				int  i = 0;
				// first check if the current permutation is not a super set of
				// a previous invalid combinaison if so we return the previous
				// invalid combinaisons hash to stop expleoration

				for (auto perm = start; perm!=end; perm++){
					auto tmp = *perm;
					//int distance = std::distance(results.begin(), results_start);
					int dist = std::distance(results.begin(), results_start) + i;
					if (auto it = std::find_if(all_invalid_permutations.begin(),
							 all_invalid_permutations.end(),
							 [tmp](const std::pair<size_t,std::vector<int> >& invalid_perm)
							 {
							 if (tmp.size() < invalid_perm.second.size())
									 return false;
							 std::vector<int> task_v;
							 std::vector<int> task_invalid;
							 for (auto i = 0; i!= invalid_perm.second.size();i++)
							 {
							 	task_v.push_back(GLOBALJOBS[invalid_perm.second[i]]->get_associated_task_id());
							 	task_invalid.push_back(GLOBALJOBS[tmp[i]]->get_associated_task_id());
							 }

							 return (is_subset_hash(task_v, task_invalid) && (tmp.size() > invalid_perm.second.size()));
							 });
							it!= all_invalid_permutations.end()) {
							results[dist] = (*it).first;
					} else {
					//use new hash https://stackoverflow.com/questions/20511347/a-good-hash-function-for-a-vector
					std::size_t seed = (*perm).size();
					//std::sort(v.begin(), v.end()); // can't sort
								       // due to gpu internal state
  					for(auto& i : (*perm)) {

   		 				seed ^= GLOBALJOBS[i]->get_associated_task_id() + 0x9e3779b9 + (seed << 6) + (seed >> 2);

  					}
						results[dist] = seed;
					}
					i++;
				}
			}
		};

		/**
		 * @brief
		 * Hash function to represent a permutation.
		 */
		struct ValidPermutationHash
		{

			static std::vector<std::pair<size_t,std::vector<int>> > all_valid_permutations;

			std::size_t operator()( const std::vector<int>& v) const noexcept
			{
				auto tmp = v;
				if (auto it = std::find_if(all_valid_permutations.begin(),
							all_valid_permutations.end(),
							[tmp](const std::pair<size_t, std::vector<int> >& valid_perm){

						return tmp == valid_perm.second;
						}); it != all_valid_permutations.end()){
						return (*it).first;
				}

				//use new hash https://stackoverflow.com/questions/20511347/a-good-hash-function-for-a-vector
				std::size_t seed = v.size();
				//std::sort(v.begin(), v.end()); // can't sort
							       // due to gpu internal state
  				for(auto& i : v) {
					auto x = i;
					x = ((x >> 16) ^ x) * 0x45d9f3b;
   					x = ((x >> 16) ^ x) * 0x45d9f3b;
   					x = (x >> 16) ^ x;
   		 			seed ^= x + GLOBALJOBS[i]->get_associated_task_id() + 0x9e3779b9 + (seed << 6) + (seed >> 2)  ;

			//		bool is_start = std::find(invalid_perm.begin(), invalid_perm.end(), seed) == invalid_perm.end();
			//		if (is_start)
			//				return seed;
   		 			//seed ^= i + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  				}

				//for(auto x : v) {
				//	auto tmp = x;
    				//	x = ((x >> 16) ^ x) * 0x45d9f3b;
    				//	x = ((x >> 16) ^ x) * 0x45d9f3b;
    				//	x = (x >> 16) ^ x;
    				//	seed ^= x + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  				//}
				//all_invalid_permutations.push_back(std::make_pair(seed,v));
				return seed;
			}
		};
// ======================= Hash functions. =======================
	bool check_created_rdy_jobs(const std::vector<int>& rdy_jobs, int time);
	int compute_sf(std::vector<std::pair<double, Interval> > intervals, int wcet);
	}// namespace utils
}// namespace gpusched

#endif
