#ifndef __VERTEX_BATCH__HPP
#define __VERTEX_BATCH__HPP

#include <libgpusched/vertex.hpp>
#include <libgpusched/graph_gen.hpp>

namespace gpusched
{

	class VertexBatch : public Vertex
	{
		public:

		VertexBatch(): Vertex() {};

		VertexBatch(const Vertex& origin, std::vector<int> workload, utils::schedule_type st):
			Vertex(origin, workload, st)
		{}

		void handle_scheduled_jobs();

		bool apply_dominance(std::vector<VertexBatch>& others) const;

		error_t check_dominance(const Vertex& other) const;
	};
}
#endif
