#ifndef __VERTEX_BATCH2__HPP
#define __VERTEX_BATCH2__HPP

#include <set>

#include <libgpusched/vertexBatch2.hpp>
#include <libgpusched/graph_gen.hpp>

namespace gpusched
{
	class VertexBatch2 : public Vertex
	{
		int dominated;
		public:

		VertexBatch2(): Vertex() {};

		VertexBatch2(const VertexBatch2& origin, std::vector<int> workload, utils::schedule_type st):
			Vertex(origin, workload, st), dominated{-1}
		{

		}

		VertexBatch2(const VertexBatch2& origin, int time, utils::schedule_type st)
		:Vertex(origin, time, st), dominated{-1}
		{
			fast_forward_to(time);
			delta_dominance.clear();
		}

		void handle_scheduled_jobs();

		bool apply_dominance(std::vector<VertexBatch2>& others, std::deque<VertexBatch2>& list_expand);

		error_t check_dominance(const Vertex& other);
		error_t check_dominance2(Vertex& other, std::deque<VertexBatch2>& list_expand);
		error_t check_dominance_same_vertex(Vertex& other, std::deque<VertexBatch2>& list_expand);

	};
}
#endif
