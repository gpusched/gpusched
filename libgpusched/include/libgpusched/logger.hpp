#ifndef __LOGGER__H
#define __LOGGER__H
#include <libgpusched/common.hpp>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace gpusched {

    enum class _debug_level : int { FATAL, INFO, WARN, DEBUG, ALL, PRINT };

    class Logger {
        std::ostream &_output_stream;
        // enumeration of all logger level.
        std::vector<_debug_level> _debug_levels;
        bool debugAll;
        Logger() : _output_stream(std::cerr){};

    public:
        ~Logger();

        static Logger instance() {
            static Logger instance;
            return instance;
        }

        /**
	 * 	@brief Enable log for a specific debug level.
         *
         *	@param level _debug_level determines the debug level.
         */
        void enable_debug_level(_debug_level level);

        /**
	 * 	@brief Disable log for a specific debug level.
         *
         * 	@param level _debug_level determines the debug level.
         */
        void disable_debug_level(_debug_level level);

        /**	@brief Overload operator << to print on instance.
         *
         *	@param  output T msg to display.
         */
        template <typename... Args>
        Logger &operator<<(const Args &...output) {
            ((_output_stream << output), ...);
            return *this;
        }

        template <typename... Args>
        void handleLog(_debug_level level, const Args &...args) {
            switch (level) {
            case (_debug_level::INFO):
                (_output_stream << GREEN << "INFO" << RESET << ":" << ...
                                << args)
                    << "\n";
                break;
            case (_debug_level::FATAL):
                (_output_stream << RED << "FATAL" << RESET << ":" << __FILE__ << "(" << __LINE__ << ") " <<  ...
                                << args)
                    << "\n";
		std::abort();
                break;
            case (_debug_level::WARN):
                (_output_stream << YELLOW << "WARN" << RESET << ":" << ...
                                << args)
                    << "\n";
                break;
	    case (_debug_level::PRINT):
		(_output_stream << ... << args) << "\n";
		break;
            default:
                break;
            }
        }
    }; // Logger class

    /// @brief Instance of logger.
    static Logger LOG = Logger::instance();

#ifdef	DEBUG_MODE

    /// @brief Base function DEBUG.
    template <typename... Args>
    inline void DEBUG(_debug_level level, const Args &...output) {
        LOG.handleLog(level, output...);
    }

    /// @brief Wrapper function DEBUGINFO.
    template <typename... Args>
    inline void DEBUGINFO(const Args &...output) {
        DEBUG(_debug_level::INFO, output...);
    }

    /// @brief Wrapper function DEBUGWARN.
    template <typename... Args>
    inline void DEBUGWARN(const Args &...output) {
        DEBUG(_debug_level::WARN, output...);
    }

    template <typename... Args>
    inline void DEBUGFATAL(const Args &...output) {
        DEBUG(_debug_level::FATAL, output...);
    }
    template <typename... Args>
    inline void PRINT(const Args &...output) {
        DEBUG(_debug_level::PRINT, output...);
    }
#endif

#ifndef DEBUG_MODE

#endif
} // namespace gpusched
#endif
