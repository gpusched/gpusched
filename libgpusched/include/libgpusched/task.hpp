/// @file task.hpp
#ifndef __TASK__HPP
#define __TASK__HPP

#include <libgpusched/logger.hpp>

// std include
#include <iomanip>
#include <memory>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

namespace gpusched{

	/**
	 *  @brief
	 *  Struct containing real-time parameters required by our real-time task model.
	 */
	typedef struct
	{
		int priority;
		int wcet;
		int bcet;
		int period;
		int deadline;
	}real_time_parameters;

	/**
	 *  @brief
	 *  Class Task
	 */
	class Task {
		protected:
		/**
		 * @brief
		 * Unique identifier.
		 */
		const int task_id;

		/**
		 * @brief
		 * rtParams contains real-time parameters
		 */
		std::unique_ptr<real_time_parameters> rt_params;

		/**
		 * @brief
		 * forbiddenConcurrencyTasks contains tasks for which the current task
		 * cannot be executed at the same time.
		 */
		std::vector<int> forbidden_candidates;

		std::vector<int> concurrency_candidates;

		/**
		 * @brief
		 * slowdownFactors contains all tasks's slowdown factors that apply to the current
		 * task when executed concurrently.
		 *
		 * Tasks id are associated with their associated slowdown factor.
		 */
		std::unordered_map<int, double> slowdown_factors; // useless can be removed
									   // we use TASKSSF global
									   // array;

		/// @brief flag to know if the current task is a GpuTask.
		bool task_type;

		public:

		/**
		 * @brief
		 * Task class constructor
		 *
		 * @param id 		The task unique identifier.
		 * @param param 	A structure containing real-time parameters.
		 * @param type		True if the task is a GpuTask, false otherwise.
		*/
		Task(const int id, std::unique_ptr<real_time_parameters> param, bool type):
			task_id{id}, rt_params{std::move(param)}, task_type{type}{};

		/**
		 * @brief
		 * Virtual deconstructor
		 */
		virtual ~Task();


		/**
		 * @brief
		 * Check if another Task can be executed concurrently with the current task.
		 *
		 * @param
		 * const Task& other 	other task
		 *
		 * @return
		 * bool true/false
		 */
       		bool is_concurrency_candidate(const Task &other) const ;

		/**
		 * @brief
		 * Check if another Task can be executed concurrently with the current task.
		 *
		 * @param id		Other task unique identifier.
		 *
		 * @return bool 	true/false
		 */
       		bool is_concurrency_candidate(const int id) const;



		/*
		 *  @brief
		 *  Add a task id and it's associated slow down factors in
		 *
		 *  @param id	other task unique identifier
		 *  @param sd	slow-down factor.
		 *
		 *  Use TASKSSF for slow-down factors.
		 */
		//void add_slowdown_factor(const int id, const double sd);

		/**
		 *  @brief
		 *  Add a entry into the concurrencyCandidate vector.
		 *
		 *  @param task_id 	task unique identifier to be added.
		 */
		void add_concurrency_candidate(const int task_id);

		/**
		 * @brief
		 * Add an entry into the forbiddenCandidate vector
		 *
		 * @param task_id	task unique identifier to be added.
		 */
		void add_forbidden_candidate(const int task_id);

		/**
		 * @brief
		 * Getter for the current task id.
		 *
		 * @return 	Task id.
		 */
	 	int get_task_id() const;

		/**
		 * @brief
		 * Getter for the current task deadline
		 *
		 * @return 	task deadline
		 */
		int get_task_deadline() const;

		/**
		 * @brief
		 * Getter for the curretn task wcet
		 *
		 * @return task wcet.
		 */
		int get_task_wcet() const;

		/**
		 * @brief
		 * Check if the current task is a GpuTask.
		 *
		 * @return True if the current task is a GpuTask, False otherwise.
		 */
		bool is_gpu_task() const;

		virtual std::string to_string();
	}; // class Task

	/// @brief
	Task *get_task_by_id(const int taskId);

	//static std::unordered_map<int, Task*> &get_task_instance()
	//{
	//	static std::unordered_map<int, Task*> TASKS;

        //        return TASKS;
        //}

	// use unused attribute to avoid warning for unused function in file that indirectly
	// include the job.hpp file.
	__attribute__((unused)) static std::unordered_map<int, std::shared_ptr<Task>> &get_task_instance()
	{
		static std::unordered_map<int, std::shared_ptr<Task>> TASKS;

                return TASKS;
        }
	/// @brief Global map GLOBALTASKS contains all application tasks.
	//extern std::unordered_map<int, Task *> GLOBALTASKS;
	extern std::unordered_map<int, std::shared_ptr<Task> > GLOBALTASKS;

	const int globaltasks_size();

	/// @brief Global 2D-array TASKSSF contains all slowdown factors.
	extern double** TASKSSF;

	/// @brief Free resources allocated by application tasks. @warning useless use smart pointer
	//void free_GLOBALTASKS();
	/// @brief Free resources allocated for the slodown factor 2D-array.
	void free_TASKSSF();

} // namespace gpusched
#endif
