#include <libgpusched/vertexBatch2.hpp>

namespace gpusched
{

	std::vector<std::vector<int> > already_splitted; 
	void VertexBatch2::handle_scheduled_jobs()
	{

#ifdef DEBUG_MODE
          	if (curr_jobs_to_schedule.size() == 0)
		{
          		DEBUGFATAL("Empty curr_jobs vector in Vertex[", vertex_id,"]");

		}
                else
			DEBUGINFO("Add jobs in curr_jobs");
#endif
		if (curr_jobs_to_schedule.size() == 0)
			return;
		double slowdown_factor = -1.0;
		slowdown_factor = utils::get_sf_from_set(curr_jobs_to_schedule);
#ifdef DEBUG_MODE
		std::cout << "SF = " << slowdown_factor << std::endl;
#endif
		// modify here to adjust sf not applying on all task
		auto copy_workload = curr_jobs_to_schedule;
		std::sort(copy_workload.begin(), copy_workload.end(), [](auto left, auto right){

					return GLOBALJOBS[left]->get_associated_wcet() < GLOBALJOBS[right]->get_associated_wcet();

				});

		// COMPUTE SF
		// Use interval


		// We can optimize by storing the sf in the valid combinasons global array
		std::vector<std::pair<double, Interval> > sf_intervals;
		for(size_t i = 0; i < copy_workload.size(); i++)
		{
			std::pair<double,Interval> sf_related;
			sf_related.first = gpusched::utils::get_sf_from_set_it(copy_workload.begin()+i,copy_workload.end());
//			sf_related.first = gpusched::utils::other_get_sf_from_set_it(copy_workload.begin()+i,copy_workload.end());
				if(i == 0){
					sf_related.second = Interval{0,GLOBALJOBS[*(copy_workload.begin()+i)]->get_associated_wcet()};
				}else{
					sf_related.second=Interval{GLOBALJOBS[*std::prev(copy_workload.begin()+i)]->get_associated_wcet(),
							GLOBALJOBS[*(copy_workload.begin()+i)]->get_associated_wcet()};
				}
			sf_intervals.push_back(sf_related);
		}

        	for (std::vector<int>::const_iterator it = copy_workload.begin();
				it != copy_workload.end();
				++it) {
			all_scheduled_jobs.push_back(*it);
			// vector remaining_jobs.erase(...); O(n) from end
			// std::bitset flipping
			//remaining_jobs &= ~(1UL << *it);

			//boost flipping
			remaining_jobs[*it] = false;

			// erase-remove
			std::vector<int>::iterator position = std::find(ready_jobs.begin(), ready_jobs.end(), *it);
			//std::cout <<"time vertex:" << time << std::endl;
			//std::cout <<"time arrival jobs:" << GLOBALJOBS[*it]->get_arrival_time() << std::endl;
			//utils::display_combinaison(ready_jobs);
			//utils::display_combinaison(copy_workload);
			//std::cout << to_string() << std::endl;
			ready_jobs.erase(position);

			int associated_task_id = GLOBALJOBS[*it]->get_associated_task_id();


			std::shared_ptr<GpuTask> curr_gpu_task;
			if ((curr_gpu_task = std::dynamic_pointer_cast<GpuTask>(
				     GLOBALTASKS[associated_task_id])) == nullptr)
			{
#ifdef DEBUG_MODE
				DEBUGFATAL("task with id: ", associated_task_id, " is not a gpu task");
#endif
				return;
			}

			//int applied_sf_wcet = utils::round_up_with_sf(slowdown_factor,
			//			GLOBALJOBS[*it]->get_associated_wcet());
			int applied_sf_wcet = gpusched::utils::compute_sf(sf_intervals, GLOBALJOBS[*it]->get_associated_wcet());
			//std::cout <<"NEW WCET FOR : " << associated_task_id << " " << applied_sf_wcet << std::endl;

			mean_thread_usage += (curr_gpu_task->get_block()
					      *curr_gpu_task->get_thread_per_block()
					      *applied_sf_wcet)
					      /(THREADS_PER_SM * NSM);

			mean_regs_usage += (curr_gpu_task->get_block()
					      *curr_gpu_task->get_thread_per_block()
					      *applied_sf_wcet
					      *curr_gpu_task->get_reg())
					      /(THREADS_PER_SM * NSM);

			if (!free_stream.empty()) {

//				streams[free_stream.front()].modify_all(GLOBALJOBS[*it]->get_associated_wcet());
//				update streams to vertex time if proceed per batch.
				if (streams[free_stream.front()].get_max_left_bound() < time)
					streams[free_stream.front()].extend_all(time);
				streams[free_stream.front()].modify_all(applied_sf_wcet);
				streams[free_stream.front()].set_scheduled_job_id(*it);
				free_stream.pop_front();
			} else
			{
//				std::cout << "I create new stream" << std::endl;

				// create new stream
				struct stream new_stream(++_count_stream, *it, time + applied_sf_wcet);
				new_stream.set_scheduled_job_id(*it);
				streams.push_back(new_stream);
				//create_stream(*it);
			}

        	}
#
		// remove scheduled jobs from ready_jobs.

		// @TODO handle stream attribution here
		// work conserving we take in order stream that are free
		// 3 possible cases:
		// 	- no stream free for scheduled jobs (create new one for each jobs scheduled)
		// 	  and adjust their time using stream::apply_slowdown_factor_on_remaining_time().
		// 	- All scheduled jobs starting time are after the maximum finish time stream
		// 	  in this case reuse theses streams in priority and create new if we don't
		// 	  have enough streams to load the workload.
		// 	- Jobs starting time is after "some" streams finish time in this case we can
		// 	  reuse these stream but we have to adjust time on other stream using
		// 	  stream::apply_slowdown_factor_on_remaining_time().

		if (remaining_jobs.count() == 0 && check_scheduled_jobs_deadlines())
			schedulable = true;

	}


	bool VertexBatch2::apply_dominance(std::vector<VertexBatch2>& others, std::deque<VertexBatch2>& list_expand_path)
	{

		// check if other.scheduled_jobs is a subset of all_scheduled_jobs.
		bool is_dominated = false;

	  	graph_viz::GraphGen* _graph = graph_viz::GraphGen::getInstance();

		//others.erase(std::remove_if(others.begin(), others.end(),
		//			   [&](const Vertex& v){
		//				return check_dominance(v) == error::EDOMINE;
		//			   }), others.end()


		// REPUT CONST ITERATOR FOR CHECK_DOMINANCE2
		// PUT NON CONST FOR CHECK_SAME_VERTEX
//		for (std::vector<VertexBatch2>::const_iterator it = others.begin();
//				it != others.end(); ) {
		for (std::vector<VertexBatch2>::iterator it = others.begin();
				it != others.end(); ) {
			if (not utils::is_subset<int>((*it).get_scheduled_jobs(),
						all_scheduled_jobs)){
				it++;
				continue; // no change
			}

//			error::error_t dominance = check_dominance((*it)); // comment below and uncomment here
			error::error_t dominance = check_dominance2((*it), list_expand_path);

//			error::error_t dominance = check_dominance_same_vertex((*it), list_expand_path);
			if (dominance == error::EDOMINATED){

				is_dominated = true;
				_graph->add_domination_relation(*this, (*it));
				// ADDITION START HERE If bug
				// We have to propagate the delta time between vertex being dominated
				// and all successors, we also have to check if successor can split
				// if so we split and add the new vertex to dominant_list
				for (std::vector<VertexBatch2>::iterator itt= others.begin();
						itt != others.end(); itt++ )
				{
					// compare only previous vertex
					// changed to compare with all predecessors vertex
					//if ((*itt).get_origin_vertex_id() == (*it).get_vertex_id()){
					auto past_vertex_of_itt = (*itt).get_past_vertex();
					//std::cout << past_vertex_of_itt.size() << std::endl;
					if (std::find(past_vertex_of_itt.begin(),
						      past_vertex_of_itt.end(),
						      (*it).get_vertex_id()) != past_vertex_of_itt.end()) {
							// check delta + time
							// if arr delta + time != arr time
							// 	split
							// else
							// store delta in sucessor
						//std::cout << "salut" << std::endl;
							if ((*this).dominated != -1){
								int delta = (*this).dominated;
                                                                //std::cout << delta << std::endl;
								int other_next_min_arrival_time = (*itt).min_arrival_time_job();
								if (other_next_min_arrival_time == GLOBALJOBS.size() +1) // CHECK HERE Because we can have false job in return of min_arrival_time_job
									continue;
								Interval interval{(*itt).get_time(), (*itt).get_time() + delta};
								//std::cout << "Interval: [" << (*itt).get_time() << "," << (*itt).get_time() +delta << "]\n";
								//std::cout << GLOBALJOBS[other_next_min_arrival_time]->get_arrival_time() << std::endl;

		        					if (interval.in_right_exclusive(GLOBALJOBS[other_next_min_arrival_time]->get_arrival_time())){
								    //&& std::find(already_splitted.begin(), already_splitted.end(), (*itt).get_time() + delta) == already_splitted.end()){
									//std::cout << "I generate new child split" << std::endl; 
									//std::cout << "I generate new child split" << std::endl; 
									//std::cout << (*itt).get_time() + delta << std::endl; 
									VertexBatch2 v_split(*itt,(*itt).get_time() + delta,utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
									auto rdy = v_split.get_copy_rdy_jobs();
									if(std::find(already_splitted.begin(), already_splitted.end(), rdy) == already_splitted.end()){
										list_expand_path.push_back(v_split);
										already_splitted.push_back(rdy); 
									}
									//others.push_back(v_split);
								}
								else{
									(*itt).delta_dominance.insert(delta);
								}
							}
                                	}
				}
				break;
			}
			else if (dominance == error::EDOMINE) {

				_graph->add_domination_relation((*it), *this);
				// remove all children from the vertex just erased
				for (std::vector<VertexBatch2>::const_iterator itt= others.begin();
						itt != others.end(); )
				{
					// compare only previous vertex
					// changed to compare with all predecessors vertex
					//if ((*itt).get_origin_vertex_id() == (*it).get_vertex_id()){
					auto past_vertex_of_itt = (*itt).get_past_vertex();
					//std::cout << past_vertex_of_itt.size() << std::endl;
					if (std::find(past_vertex_of_itt.begin(),
						      past_vertex_of_itt.end(),
						      (*it).get_vertex_id()) != past_vertex_of_itt.end()) {
                                                _graph->add_domination_relation((*itt), *this);
						//std::cout << "ERASE A VERTEX BECAUSE ONE OF ITS PREVIOUS VERTEX GOING DELETED" << std::endl;
						itt = others.erase(itt);
                                        }
					else
						++itt;
				}

				it = others.erase(it);
				//others.push_back(*this);
				break;
			} else
				++it;
		}

		if (!is_dominated){
			others.push_back((*this));
			return true;
		}

		return false;
	}

	error::error_t VertexBatch2::check_dominance(const Vertex& other)
	{

		if ( not utils::is_subset<int>(other.get_scheduled_jobs(),
					all_scheduled_jobs))
			return error::ENOTSUBSET;

		// check if other sm finish later than current vertex
		// if so we have current vertex domine other.
		// With work-conserving is if we found a SM that finish earlier in current
		// vertex.
		int curr_maximal_stream_index = get_higher_stream_index();
		int other_maximal_stream_index = other.get_higher_stream_index();

		const std::vector<struct stream> other_streams = other.get_streams();

		//for (auto job: all_scheduled_jobs) // compare finish time between all jobs.

		// upper bound check
		// J1 = other scheduled jobs, t1 is the time at which the last jobs of J1 finished
		// J2 = vertex scheduled jobs, t2 is the time at which the last jobs of J2 finished
		//  D = J1\J2, jobs in J1 that are not yet scheduled by other vertex
		// t2' = t2  + (sum_[C(D)] / #max_resources)
		// if t2' > t1 then other vertex dominate the current vertex


		// J1\J2
		std::vector<int> diff = utils::difference_set(other.get_scheduled_jobs(),
					all_scheduled_jobs);

	//	if (diff.size() == 0)
	//		if (streams[curr_maximal_stream_index].get_max_right_bound() <
	//	    		other_streams[other_maximal_stream_index].get_max_right_bound())
	//			return error::EDOMINE;
			//else if(streams[curr_maximal_stream_index].get_max_right_bound() >
			//		other_streams[other_maximal_stream_index].get_max_right_bound())
			//	return error::EDOMINATED;

		// t1
		int other_vertex_max_time = other_streams[other_maximal_stream_index].get_max_right_bound();
		// t2
		int curr_vertex_max_time  = streams[curr_maximal_stream_index].get_max_right_bound();

		int accumulate_ci = 0;

		for (auto job_id: diff) {
			// first acculumate_ci without ai first solution for dominance relation
			//accumulate_ci += GLOBALJOBS[job_id]->get_associated_wcet();


			int offset = 0;
			// second one with arrival time of job
			// If job is not yet arrived at vertex time, we have to take into account
			// the time at which it'll arrive
		//	offset = curr_vertex_max_time - GLOBALJOBS[job_id]->get_arrival_time();
			offset = GLOBALJOBS[job_id]->get_arrival_time() - curr_vertex_max_time;
			if (offset > 0)
				accumulate_ci += GLOBALJOBS[job_id]->get_associated_wcet() + offset;
			else
				accumulate_ci += GLOBALJOBS[job_id]->get_associated_wcet();


		}

		// third one by comparing also ready_jobs.
		// quickly generate the list from here without fast_forward to not break
		// the code
		//
		//
		// We want to compare the ready_list for jobs arriving after v2
		// and the list arriving after v1 + all accumulate ci from v2 that
		// v1 doesn't scheduled yet
		// if ready_list v1 is not contained in ready_list v2
		// it's not subset and v1 is not dominated
		// see counter example 1
		std::vector<int> curr_ready_after_accumulate; // v1' -> 5
		std::vector<int> other_ready; // v1 -> 4
		for (auto i = 0; i < GLOBALJOBS.size(); i++){

			// here we take only jobs in interval [other_finished, curr_finished] in the case where curr_finished finish after other_finished
			// [4,5] -> [j3, j4]
			if ((GLOBALJOBS[i]->get_arrival_time() <= (curr_vertex_max_time + accumulate_ci)) && (GLOBALJOBS[i]->get_arrival_time() > other_vertex_max_time))
				curr_ready_after_accumulate.push_back(GLOBALJOBS[i]->get_job_id());
			// here we just take all jobs ready at the end of the other vertex to compare
			else if (GLOBALJOBS[i]->get_arrival_time() <= (other_vertex_max_time))
				other_ready.push_back(GLOBALJOBS[i]->get_job_id());
		}

		// here with the example we'll have
		// other_ready = [4]
		// curr_ready = [4,5]
		// since curr_ready is not a subset of other_ready we can't say that other vertex dominate
		// the current vertex
		if (not utils::is_subset<int>(other_ready,
					curr_ready_after_accumulate))
			return error::ENOTSUBSET;
		int future_time = curr_vertex_max_time + accumulate_ci;
		//std::cout << "future time: " << future_time << " other_vertex_max_time:" << other_vertex_max_time << std::endl;
		//std::cout << "CURR_VERTEX: " << curr_vertex_max_time
		//	  << " ACCUMULATE_CI: " << accumulate_ci
		//	  << " FUTURE TIME = "
		//	  << future_time
		//	  << " OTHER VERTEX TIME = "
		//	  << other_vertex_max_time
		//	  << std::endl;
		if (future_time >= other_vertex_max_time)
			return error::EDOMINATED;

		delta_dominance.insert(other_vertex_max_time - future_time);
		for (auto it = other.get_delta_dominance().begin();
				it != other.get_delta_dominance().end();
				it++)
		{
			delta_dominance.insert((*it) + (other_vertex_max_time - future_time));
		}
                return error::EDOMINE;
	}

	error::error_t VertexBatch2::check_dominance2(Vertex& other, std::deque<VertexBatch2>& list_expand)
	{

		if ( not utils::is_subset<int>(other.get_scheduled_jobs(),
					all_scheduled_jobs))
			return error::ENOTSUBSET;


		// check if other sm finish later than current vertex
		// if so we have current vertex domine other.
		// With work-conserving is if we found a SM that finish earlier in current
		// vertex.
		int curr_maximal_stream_index = get_higher_stream_index();
		int other_maximal_stream_index = other.get_higher_stream_index();

		const std::vector<struct stream> other_streams = other.get_streams();

		//for (auto job: all_scheduled_jobs) // compare finish time between all jobs.

		// upper bound check
		// J1 = other scheduled jobs, t1 is the time at which the last jobs of J1 finished
		// J2 = vertex scheduled jobs, t2 is the time at which the last jobs of J2 finished
		//  D = J1\J2, jobs in J1 that are not yet scheduled by other vertex
		// t2' = t2  + (sum_[C(D)] / #max_resources)
		// if t2' > t1 then other vertex dominate the current vertex


		// J1\J2
		std::vector<int> diff = utils::difference_set(other.get_scheduled_jobs(),
					all_scheduled_jobs);

		// t1
		int other_vertex_max_time = other_streams[other_maximal_stream_index].get_max_right_bound();
		////// t2
		int curr_vertex_max_time  = streams[curr_maximal_stream_index].get_max_right_bound();

		int accumulate_ci = 0;

                if (other.get_remaining_jobs().count() == remaining_jobs.count() &&
		    (curr_vertex_max_time == other_vertex_max_time) &&
		    (ready_jobs == other.get_rdy_jobs()))
		{
			(*this).dominated = 0; 
			set_dominated_to_remove();
			return error::EDOMINATED;
		}
		for (auto job_id: diff) {
			// first acculumate_ci without ai first solution for dominance relation
			//accumulate_ci += GLOBALJOBS[job_id]->get_associated_wcet();
			int offset = 0;
			// second one with arrival time of job
			// If job is not yet arrived at vertex time, we have to take into account
			// the time at which it'll arrive
		//	offset = curr_vertex_max_time - GLOBALJOBS[job_id]->get_arrival_time();
			offset = GLOBALJOBS[job_id]->get_arrival_time() - curr_vertex_max_time;
			if (offset > 0)
				accumulate_ci += GLOBALJOBS[job_id]->get_associated_wcet() + offset;
			else
				accumulate_ci += GLOBALJOBS[job_id]->get_associated_wcet();

			if(time + accumulate_ci > GLOBALJOBS[job_id]->get_relative_deadline())
				return error::ENOTSUBSET;
		}

		// third one by comparing also ready_jobs.
		// quickly generate the list from here without fast_forward to not break
		// the code
		//
		//
		// We want to compare the ready_list for jobs arriving after v2
		// and the list arriving after v1 + all accumulate ci from v2 that
		// v1 doesn't scheduled yet
		// if ready_list v1 is not contained in ready_list v2
		// it's not subset and v1 is not dominated
		// see counter example 1
		std::vector<int> curr_ready_after_accumulate = other.get_copy_rdy_jobs(); // v1' -> 5
		std::vector<int> other_ready; // v1 -> 4
		int future_time = curr_vertex_max_time + accumulate_ci;
		for (auto i = 0; i < GLOBALJOBS.size(); i++){

			// here we take only jobs in interval [other_finished, curr_finished] in the case where curr_finished finish after other_finished
			// [4,5] -> [j3, j4]
			if ((GLOBALJOBS[i]->get_arrival_time() <= (future_time)) && (GLOBALJOBS[i]->get_arrival_time() > other_vertex_max_time))
				curr_ready_after_accumulate.push_back(GLOBALJOBS[i]->get_job_id());
			// here we just take all jobs ready at the end of the other vertex to compare
		}

		// here with the example we'll have
		// other_ready = [4]
		// curr_ready = [4,5]
		// since curr_ready is not a subset of other_ready we can't say that other vertex dominate
		// the current vertex
		//if (not utils::is_subset<int>(other_ready,
		//			curr_ready_after_accumulate))
		if (other.get_rdy_jobs() != curr_ready_after_accumulate){
			return error::ENOTSUBSET;
		}
		//std::cout << "future time: " << future_time << " other_vertex_max_time:" << other_vertex_max_time << std::endl;
		//std::cout << "CURR_VERTEX: " << curr_vertex_max_time
		//	  << " ACCUMULATE_CI: " << accumulate_ci
		//	  << " FUTURE TIME = "
		//	  << future_time
		//	  << " OTHER VERTEX TIME = "
		//	  << other_vertex_max_time
		//	  << std::endl;
		if (future_time >= other_vertex_max_time){
			(*this).dominated = future_time - other.get_time();
			set_dominated_to_remove();
			return error::EDOMINATED;
		}

		VertexBatch2 new_vertex(*this, future_time, utils::schedule_type::WORK_CONSERVING_HIGHER_STREAM);
		new_vertex.handle_in_time(diff);
		list_expand.push_back(new_vertex);
		new_vertex.delta_dominance.insert(other_vertex_max_time - future_time);
		//for (auto it = other.get_delta_dominance().begin();
		//		it != other.get_delta_dominance().end();
		//		it++)
		//{
		//	delta_dominance.insert((*it) + (other_vertex_max_time - future_time));
		//}
                return error::EDOMINE;
	}



	error::error_t VertexBatch2::check_dominance_same_vertex(Vertex& other, std::deque<VertexBatch2>& list_expand)
	{

		int curr_maximal_stream_index = get_higher_stream_index();
		int other_maximal_stream_index = other.get_higher_stream_index();

		const std::vector<struct stream> other_streams = other.get_streams();
		int past_other_vertex_max_time = other_streams[other_maximal_stream_index].get_max_right_bound();
		////// t2
		int curr_vertex_max_time  = streams[curr_maximal_stream_index].get_max_right_bound();
		//if((other.get_remaining_jobs().count() == remaining_jobs.count()) && (curr_vertex_max_time < past_other_vertex_max_time) && (ready_jobs == other.get_rdy_jobs()))
		//{
		//	std::cout << std::endl;
		//	std::cout << std::endl;
		//	std::cout << std::endl;
		//	std::cout << other.to_string() << std::endl;
		//	std::cout << to_string() << std::endl;
		//	std::cout << std::endl;
		//	std::cout << std::endl;
		//	std::cout << std::endl;
		//	other.set_dominated_to_remove();
		//	delta_dominance.insert(past_other_vertex_max_time - curr_vertex_max_time);
		//	return error::EDOMINE;
		//}else if (other.get_remaining_jobs().count() == remaining_jobs.count() &&  curr_vertex_max_time >= past_other_vertex_max_time && ready_jobs == other.get_rdy_jobs())
		//{
		//	set_dominated_to_remove();

		//	other.update_delta_dominance(curr_vertex_max_time - past_other_vertex_max_time);
		//	return error::EDOMINATED;
		//}
                //std::cout << curr_vertex_max_time << " "
                //          << past_other_vertex_max_time << std::endl;
                if (other.get_remaining_jobs().count() == remaining_jobs.count() &&
		    (curr_vertex_max_time == past_other_vertex_max_time) &&
	//	    (get_curr_jobs_to_schedule() == other.get_curr_jobs_to_schedule()) &&
		    (ready_jobs == other.get_rdy_jobs()))
		{
			set_dominated_to_remove();
			return error::EDOMINATED;
		}
		return error::ENOTSUBSET;
	}
} // namespace
  //
