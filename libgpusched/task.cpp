/// @file task.cpp
#include <libgpusched/task.hpp>


namespace gpusched
{


	//std::unordered_map<int, Task *> GLOBALTASKS(get_task_instance());

	std::unordered_map<int, std::shared_ptr<Task> > GLOBALTASKS = get_task_instance();

	double ** TASKSSF = nullptr;

	// useless if we use shared_ptr
//	void free_GLOBALTASKS()
//	{
//#ifdef DEBUG_MODE
//		DEBUGINFO("Free all ressources allocated for tasks");
//#endif
//		for(std::pair<int, std::shared_ptr<Task>> task: GLOBALTASKS)
//			delete task.second;
//	}

	void free_TASKSSF()
	{
#ifdef DEBUG_MODE
		DEBUGINFO("Free slow down factor");
#endif

		// TASKSSF is [GLOBALTASKS.size()][GLOBALTASKS.size()] array
		for (std::size_t i = 0; i < GLOBALTASKS.size(); i++)
			delete[] TASKSSF[i];
		delete[] TASKSSF;
	}

        // ===	Task class implementation  ===
        Task::~Task()
	{
		rt_params.reset();
	}

	bool Task::is_concurrency_candidate(const Task &other) const
	{
		const int id = other.get_task_id();

		for (std::vector<int>::const_iterator it = concurrency_candidates.begin();
			       	it != concurrency_candidates.end(); ++it) {
			if (*it == id)
				return true;
		}

		return false;
	}

	bool Task::is_concurrency_candidate(const int id) const
	{
		for (std::vector<int>::const_iterator it = concurrency_candidates.begin();
				it != concurrency_candidates.end(); ++it) {
			if(*it == id)
				return true;
		}
		return false;
	}

// we use TASKSSF global array for slow-down factors.
//	void Task::add_slowdown_factor(const int id, const double sd)
//	{
//
//#ifdef DEBUG_MODE
//		DEBUGINFO("Add slow-down factor ", sd, " for task[", id,"]");
//#endif
//		slowdown_factors.insert(std::make_pair(id, sd));
//	}
//

	void Task::add_concurrency_candidate(const int id)
	{

#ifdef DEBUG_MODE
		DEBUGINFO("Add task with id[", id,"] in task[", task_id,"] concurrency candidates");
#endif
		concurrency_candidates.push_back(id);
	}

	void Task::add_forbidden_candidate(const int id)
	{
#ifdef DEBUG_MODE
		DEBUGINFO("Add task with id[", id,"] in task[", task_id,"] forbidden candidates");
#endif
		forbidden_candidates.push_back(id);
	}

	int Task::get_task_id() const
	{
		return task_id;
	}

	int Task::get_task_deadline() const
	{
		return rt_params->deadline;
	}

	int Task::get_task_wcet() const
	{
		return rt_params->wcet;
	}


	bool Task::is_gpu_task() const
	{
		return task_type;
	}

    	std::string Task::to_string() {
        	std::stringstream ss;
        	char separator = ' ';
        	int width = 25;

        	ss << "=================\e[1mTask id: " << task_id
        	   << "\e[0m=================\n\n"
        	   << "\e[1mReal-Time Parameters:\e[0m \n"
        	   << std::left << std::setw(width) << std::setfill(separator) << "bcet"
        	   << std::left << std::setw(width) << std::setfill(separator) << "wcet"
        	   << std::left << std::setw(width) << std::setfill(separator) << "period"
		   << std::left << std::setw(width) << std::setfill(separator) << "deadline"
        	   << "\n";

        	ss << std::left << std::setw(width) << std::setfill(separator)
		   << rt_params->bcet
        	   << std::left << std::setw(width) << std::setfill(separator)
		   << rt_params->wcet
		   << std::left << std::setw(width) << std::setfill(separator)
		   << rt_params->period
		   << std::left << std::setw(width) << std::setfill(separator)
		   << rt_params->deadline
        	   << "\n";

        	return ss.str();
    }


}
