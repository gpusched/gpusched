/// @file utils.cpp
#include <libgpusched/utils.hpp>
namespace gpusched
{
	namespace utils
	{
		std::vector<std::pair<size_t, std::vector<int> > > PermutationHash::all_invalid_permutations;
		std::vector<std::pair<size_t, std::vector<int> > > ValidPermutationHash::all_valid_permutations;
		std::vector<std::pair<size_t, std::vector<int> > > PermutationHashParallel::all_invalid_permutations;
		void fetchTasks(const std::string &filename)
		{
			std::string line;
			std::ifstream file(filename);
#ifdef DEBUG_MODE
			if(file.fail())
				DEBUGFATAL("File not found!");
#endif

			int task_type;
			int task_id;
#ifdef DEBUG_MODE
			DEBUGINFO("Fetching tasks from ", filename);
#endif

			while (file >> task_type && task_type != -1) {
#ifdef DEBUG_MODE
				if( task_type == CPUTASK)
					DEBUGWARN("CPU Task type[", task_type,"]");
				else if (task_type == GPUTASK)
					DEBUGWARN("GPU Task type[", task_type,"]");
				else
					DEBUGFATAL("Unknown task type");
#endif
				file >> task_id;
				std::unique_ptr<gpusched::real_time_parameters> rp=
					std::make_unique<gpusched::real_time_parameters>();
				file >> (rp->bcet);
				file >> (rp->wcet);
				file >> (rp->period);
				file >> (rp->deadline);
				file >> (rp->priority);

				if( task_type == GPUTASK) {
					std::unique_ptr<gpusched::cuda_kernel_parameters> kp =
						std::make_unique<gpusched::cuda_kernel_parameters>();
					file >> (kp->block);
					file >> (kp->thread_per_block);
					file >> (kp->regs);
					file >> (kp->smem);
#ifdef DEBUG_MODE
					DEBUGINFO("Create GPU Task with id: ", task_id);
#endif
					//GLOBALTASKS[task_id] = new GpuTask(task_id, std::move(kp),
					//		std::move(rp));
					GLOBALTASKS[task_id] = std::make_shared<GpuTask>(task_id,
						       	std::move(kp), std::move(rp));
				} else if (task_type == CPUTASK) {
#ifdef DEBUG_MODE
					DEBUGINFO("Create CPU Task with id: ", task_id);
#endif
					//GLOBALTASKS[task_id] = new Task(task_id, std::move(rp),
					//		false);
					GLOBALTASKS[task_id] = std::make_shared<Task>(task_id,
							std::move(rp), false);
				}
			}

			// read slow down factor
			std::size_t size = GLOBALTASKS.size();

			TASKSSF = new double*[size];
			for (std::size_t i = 0; i < size; i++)
				TASKSSF[i]= new double[size];

			int x,y;
			while(file >> x) {
				file >> y;
				file >> TASKSSF[x][y];
#ifdef DEBUG_MODE
				std::cout << TASKSSF[x][y] << std::endl;
#endif
			}
		}


		void fetchJobs(const std::string &filename)
		{
			std::string line;
			std::ifstream file(filename);
#ifdef DEBUG_MODE
			if(file.fail())
				DEBUGFATAL("File not found!");
#endif
			int job_id, associated_task_id, arrival_time, relative_deadline;

			while (file >> job_id) {
				file >> associated_task_id;
				file >> arrival_time;
				file >> relative_deadline;
#ifdef DEBUG_MODE
				DEBUGINFO("Create new JOB with id: ", job_id,
						" associated to task with id ", associated_task_id);
#endif
				GLOBALJOBS[job_id] = std::make_shared<Job>(job_id,
									    associated_task_id,
									    arrival_time,
									    relative_deadline);
			}

		}

		void get_all_combinaisons(std::set<std::vector<int> >& permutations,
				std::vector<int> ready_jobs)
		{
			if (ready_jobs.size() == 0)
				return;
			for (int i = 0; i <= ready_jobs.size()-1; i++) {
				do{
					std::vector<int> perm;

					for (std::vector<int>::const_iterator
							it = ready_jobs.begin();
							it != (ready_jobs.end() - i);
							++it)
						perm.push_back(*it);

					permutations.insert(perm);
				} while (std::next_permutation(ready_jobs.begin(),
						       	ready_jobs.end()));
			}
		}

		void get_k_combinaisons(long unsigned int k,
				std::set<std::vector<int> >& permutations,
				std::vector<int> ready_jobs
				)
		{

#ifdef DEBUG_MODE
			if (k > ready_jobs.size())
				DEBUGFATAL("Can't construct, ", k,"-combinaisons because ready_jobs"
						"size is )", ready_jobs.size());
#endif
			if ( k == 0)
				return;
			do{
				std::vector<int> perm;
				for (std::vector<int>::const_iterator
						it = ready_jobs.begin();
						it != (ready_jobs.begin() + k);
						++it)
						perm.push_back(*it);
				permutations.insert(perm);
			} while (std::next_permutation(ready_jobs.begin(), ready_jobs.end()));
		}

		void display_combinaison(const std::vector<int>& combinaison)
		{
			std::cout << "\nDisplay combinaison" << std::endl;
			for (std::vector<int>::const_iterator it = combinaison.begin();
			     		it != combinaison.end();
					++it)
				std::cout << *it << " ";
			std::cout << std::endl;
			std::cout << "End DISPLAY COMBINAISON\n" << std::endl;
		}

		std::vector<int> difference_set(std::vector<int> vec_a, std::vector<int> vec_b)
		{
			std::vector<int> difference;
			std::sort(vec_a.begin(), vec_a.end());
			std::sort(vec_b.begin(), vec_b.end());
			std::set_difference(std::begin(vec_a), std::end(vec_a),
					    std::begin(vec_b), std::end(vec_b),
					    std::back_inserter(difference));
			return difference;
		}

		int round_up_with_sf(double sf, int wcet)
		{
			return std::ceil(sf * wcet);
		}

		int get_next_different_arrival_job(int job_id)
		{
			int save_arrival_time = GLOBALJOBS[job_id]->get_arrival_time();
			while(GLOBALJOBS[job_id]->get_arrival_time() == save_arrival_time) {
				job_id++;
				if (job_id >= GLOBALJOBS.size())
					return -1;
			}

			return job_id;
		}
		double get_sf_from_set(std::vector<int>& jobs_id)
		{
			double sf = 1.0;
			int task_id_a = -1;
			int task_id_b = -1;
			for (std::size_t i = 0; i < jobs_id.size(); i++) {
				for (std::size_t j = i; j < jobs_id.size(); j++) {
					task_id_a = GLOBALJOBS[jobs_id[i]]->get_associated_task_id();
					task_id_b = GLOBALJOBS[jobs_id[j]]->get_associated_task_id();
					sf *= TASKSSF[task_id_a][task_id_b];
				}
			}
			return sf;
		}

		double ohter_get_sf_from_set(std::vector<int>& jobs_id)
		{
			double sf = 1.0;
			int task_id_a = -1;
			int task_id_b = -1;
			task_id_a = GLOBALJOBS[jobs_id[jobs_id[0]]]->get_associated_task_id();
			for (std::size_t j = 1; j < jobs_id.size(); j++) {
					task_id_b = GLOBALJOBS[jobs_id[j]]->get_associated_task_id();
					sf *= TASKSSF[task_id_a][task_id_b];
			}
			return sf;
		}

	bool check_created_rdy_jobs(const std::vector<int>& rdy_jobs, int vertex_time){
	//	std::set<int> related_tasks_id;

	//	for(auto job_id: rdy_jobs){
	//		auto inserted = related_tasks_id.insert(GLOBALJOBS[job_id]->get_associated_task_id());
	//		if (!inserted.second){
	//			return false;
	//		}
	//	}
		for(auto job_id: rdy_jobs){
			if(!GLOBALJOBS[job_id]->check_deadline(vertex_time))
				return false;
		}
		return true;
	}

	int compute_sf(std::vector<std::pair<double, Interval> > intervals, int wcet){

	int wcet_sf = 0;

	for(auto it = intervals.begin(); it != intervals.end(); it++){
		if((*it).second.get_left_bound() == wcet)
			continue;
		if((*it).second.get_right_bound() == wcet){
			//std::cout << std::ceil((wcet - (*it).second.get_left_bound()) * (*it).first) << std::endl;
			wcet_sf += std::ceil((wcet - (*it).second.get_left_bound()) * (*it).first);
		}
		else if((*it).second.is_greater(wcet)) {
			//std::cout << std::ceil(((*it).second.get_right_bound()-(*it).second.get_left_bound()) * (*it).first) << std::endl;
			wcet_sf+= std::ceil(((*it).second.get_right_bound()-(*it).second.get_left_bound()) * (*it).first);
		}
	}
	return wcet_sf;

	}

	}//namespace utils
}// namespace gpusched
